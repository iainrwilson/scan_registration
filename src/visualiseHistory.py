"""

Load a csv generated from onh_regs.py (it takes ages to run)

playing with visualising the data.

"""

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

df = pd.read_csv('history_points.csv')

# list of pids
pids = df.pid.unique()



fig, ax = plt.subplots(5, 3, sharex='col', sharey='row')
axs = ax.flatten()

for i in range(len(axs)):
    _df = df[(df.pid == pids[i]) & (df.ID == 4)]

    col = ['#e74c3c','#2ecc71']
    sns.scatterplot(x='scan_number',y='value',data=_df,hue='response',palette=sns.color_palette(col),ax=axs[i],legend=False)
    axs[i].set_title("PID: %d"%pids[i])