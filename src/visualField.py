"""

Looking at the normal control visual field MP data - eventual will add the octopus.

"""
import platform,os,glob
import EyeData


home = os.path.expanduser("~")
data_dir = os.path.join(home, "experiments/vision classification/scan_registration/data/")

if platform.system() == "Linux":
    mp_data_dir = "/data/backup/MAIA/MAIA_backup_20180626/"
    mp_fixation_dir = "/data/backup/MAIA/maia-4003_data"
else:
    mp_data_dir = "/Users/iain/Documents/backup_maia-4003_20180626"
    mp_fixation_dir = os.path.join(home,"experiments/microperimetry/fixation_data")



mp_scans = [

    {'id': 'TRT02', 'pid': 549, 'eid': 7619, 'grid': "star"},
    {'id': 'TRT02', 'pid': 549, 'eid': 7625, 'grid': "circle"},
    {'id': 'TRT02', 'pid': 549, 'eid': 7651, 'grid': "circle"},
    {'id': 'TRT02', 'pid': 549, 'eid': 7652, 'grid': "circle"},
    {'id': 'TRT02', 'pid': 549, 'eid': 7693, 'grid': "10-2"},

    {'id': 'TRT01', 'pid': 548, 'eid': 7613, 'grid': "circle"},
    {'id': 'TRT01', 'pid': 548, 'eid': 7620, 'grid': "star"},

    {'id': 'TRT03', 'pid': 551, 'eid': 7648, 'grid': "circle"},
    {'id': 'TRT03', 'pid': 551, 'eid': 7654, 'grid': "star"},

    # {'id': 'TRT04', 'pid': 548, 'eid':  , 'grid': "circle"},
    # {'id': 'TRT04', 'pid': 548, 'eid':  , 'grid': "star"},
    #
    # {'id': 'TRT05', 'pid': 548, 'eid':  , 'grid': "circle"},
    # {'id': 'TRT05', 'pid': 548, 'eid':  , 'grid': "star"},
]

oct_scans = {
    "TRT01":"OCT_TRT01_FDAB9A80_OD",
    "TRT02":"OCT_TRT02_37301230_OD",
    "TRT03":"OCT_TRT03_5471E390_OD"
}


data = EyeData.Importer2(mp_data_folder=mp_data_dir,mp_fixation_dir=mp_fixation_dir)

print ("__ Loading MP Scans __ ")
for mp in mp_scans:
    data.loadMP("TRT02",mp['pid'],mp['eid'])

print ("__ Loading OCT Scans __ ")
# find oct scans
find = os.path.join(data_dir,"TRT_OCT/*.xml")
files = glob.glob(find)
for f in files:
    data.loadOct(f)


# register
reg_dir = os.path.join(data_dir, 'registration')
reg = EyeData.ScanRegistrator(reg_dir=reg_dir)

oct_scan = data.people['TRT02']['OCT_TRT02_37301230_OD']

print ("__ Registering Scans __")

# register all the mp scans to the oct

for k,v in data.people.iteritems():
    if k not in oct_scans.keys():
        continue
    _oct_scan = v[oct_scans[k]]
    for scan in v.mode("MP"):
        reg.register(scan,_oct_scan)



# overlay star and circle.
import cv2
oct_mp_img = reg.overlayMPGrid(mp_scan=data.people['TRT02']['MP_TRT02_OD_7619'],target_scan=oct_scan)
oct_mp_img = reg.overlayMPGrid(mp_scan=data.people['TRT02']['MP_TRT02_OD_7652'],target_scan=oct_scan,image=oct_mp_img)

cv2.imwrite("OCT_MP_TRT02.png",oct_mp_img)