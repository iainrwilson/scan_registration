import glob
import os,cv2

import tkinter as tk

from PIL import Image, ImageTk

import EyeData

class MyFirstGUI:
    def __init__(self, master):
        self.master = master
        master.title("A simple GUI")

        self.left_frame = tk.Frame(master, bg='blue', width=100, height=190)
        self.left_frame.grid(column=0,row=0)
        # self.middle_frame = tk.Frame(master, bg='black', width=100, height=190)
        # self.middle_frame.grid(column=1, row=0)

        self.right_frame = ScanDisplay(master)

        self.right_frame.grid(column=1,row=0)

        self.pid_listbox = tk.Listbox(self.left_frame)
        self.pid_listbox.pack()
        self.pid_listbox.bind('<Button-1>',self.pidListDoubleClick)

        self.scan_listbox = tk.Listbox(self.left_frame)
        self.scan_listbox.pack()
        self.scan_listbox.bind('<Button-1>', self.scanListDoubleClick)
        # self.listbox.insert(END, "a list entry")

    def loadPatients(self,patients):
        """
        Load the list box with pids

        :param people:
        :return:
        """
        self.patients = patients

        for p in self.patients.list():
            self.pid_listbox.insert(tk.END,p)

    def loadScans(self,patient):
        self.scan_listbox.delete(0,tk.END)
        for s in patient.scans():
            self.scan_listbox.insert(tk.END,s)


    def pidListDoubleClick(self,event):
        """ double click handler """
        w = event.widget
        index = int(w.curselection()[0])
        value = w.get(index)

        # get all scans associated with this patient.
        self.loadScans(self.patients[value])

        # store selected patient
        self.selected_pid = self.patients[value]
        # print 'You selected item %d: "%s"' % (index, value)

    def scanListDoubleClick(self,event):
        """ double click handler """
        w = event.widget
        index = int(w.curselection()[0])
        value = w.get(index)

        #testing new methods
        scan = self.selected_pid.getScanByID(value)
        self.right_frame.pack_forget()

        if scan.mode =="OCT":
            self.right_frame = OCTDisplay(self.master)
            self.right_frame.grid(column=1, row=0)
            self.right_frame.bind_all("<MouseWheel>",self._on_mouse_wheel)

        elif scan.mode == "MP":
            self.right_frame = MPDisplay(self.master)
            self.right_frame.grid(column=1, row=0)
        elif scan.mode == "SWAF":
            self.right_frame = SWAFDisplay(self.master)
            self.right_frame.grid(column=1, row=0)

        self.right_frame.clear()
        self.right_frame.setScan(scan)
        self.right_frame.displayScan()

    def _on_mouse_wheel(self,event):
        # print("Mouse Wheel",event.delta)
        #delta is + for up.
        self.right_frame.increment_slice(event.delta)


class ScanFrame(tk.Frame):
    """
    Container class for all the scan views.
    Inherit from this and implement different viewers
    """

    def __init__(self,parent):
        tk.Frame.__init__(self,parent,bg='black')

        #create layout
        self.info_label = tk.Label(self, text="Scan Information")
        self.info_label.grid(column=0, row=0, columnspan=2)




class ScanDisplay(tk.Frame):
    def __init__(self,parent):
        tk.Frame.__init__(self,parent,bg='black', width=1200, height=600)

        self.info_label = tk.Label(self,text="Scan Information")
        self.info_label.grid(column=0,row=0,columnspan=2)

        self.image_label = tk.Label(self)
        self.image_label.grid(column=0,row=1)
        self.slice_label = tk.Label(self)
        self.slice_label.grid(column=1,row=1)

        self.slice_label.bind('<Button-1>', self.setSliceCursor)

        self.buttons = []

        self.button_frame = tk.Frame(self)
        self.button_frame.grid(column=0,row=3,columnspan=2)

        self.slice_number = 0
        self.alpha = 1.0 #blend value
        self.reg = None
        self.slice_slider = None

        self.sliceCursor = None

    def clear(self):
        for b in self.buttons:
            b.pack_forget()
        if len(self.buttons)>0:
            self.alpha_slider.pack_forget()
            self.slice_slider.pack_forget()
            self.slice_slider = None

    def setScan(self,scan):
        self.scan = scan
        self.buttons = []

        #for each scan, find any associated registrations.
        sources = self.scan.person.registrations.getSources(scan)
        print "Found %d sources"%len(sources)
        print sources
        if len(sources)>0:
            # create a button for each source:
            for source in sources:
                s = source.source
                # print s
                self.buttons.append(
                    tk.Button(self.button_frame, text=s.getID(), command=lambda linked_scan=s: self.linkButtonPressed(linked_scan))
                )
                self.buttons[-1].pack()
            self.buttons.append(
                tk.Button(self.button_frame, text="Default", command=lambda reg=None: self.clearButtonPressed())
            )
            self.buttons[-1].pack()



    def clearButtonPressed(self):
        """ clear the reg """
        self.reg = None
        self.linkButtonPressed(None)

    def linkButtonPressed(self, scan):
        """
        e.g Toggle overlaying MP data

        display the overlayed data
        :param event:
        :return:
        """

        if scan is None:
            self.overlay = None
            self.localiser = None
            self.mp_overlay = None
            self.displayScan()
            return

        print("Pressed: ", scan)

        if scan.mode == "MP":
            print ("MP overlay")
            self.overlay = scan
            self.drawMPOverlay(scan)

        if scan.mode =="SWAF":
            print ("SWAF localiser")
            self.mp_overlay = None
            self.setLocaliser(scan)

        self.displayScan()


    def updateSlice(self,event):
        self.slice = self.slice_slider.get()
        self.linkButtonPressed(None)

    def setSliceCursor(self,pos):
        self.sliceCursor = (pos.x,pos.y)
        print self.sliceCursor
        self.linkButtonPressed(None)

    def drawScanImage(self,image,resize=(600,600)):
        if resize is not None:
            image = cv2.resize(image,resize)

        image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        im = Image.fromarray(image)
        imgtk = ImageTk.PhotoImage(image=im)

        self.image_label.config(image=imgtk)
        self.image_label.image = imgtk

    def drawSliceImage(self,image,resize=(600,600)):
        if resize is not None:
            image = cv2.resize(image, resize)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        im = Image.fromarray(image)
        imgtk = ImageTk.PhotoImage(image=im)

        self.slice_label.config(image=imgtk)
        self.slice_label.image = imgtk

    def displayScan(self,reg=None):
        pass

    def increment_slice(self,inc):
        pass

class MPDisplay(ScanDisplay):
    def __init__(self,parent):
        ScanDisplay.__init__(self,parent)

    def displayScan(self):
        """ Show MP IR image, with overlayed threholds"""
        ir_image = self.scan.getIRImage().copy()
        # ir_image = self.scan.data.renderGrid()

        self.scan.drawStimuli(image=ir_image,drawHistory=True)
        self.drawScanImage(ir_image)

class SWAFDisplay(ScanDisplay):
    def __init__(self,parent):
        ScanDisplay.__init__(self,parent)

    def displayScan(self):
        """ Show SWAF image, with overlayed threholds"""
        ir_image = self.scan.getIRImage().copy()
        self.drawScanImage(ir_image)


class OCTDisplay(ScanDisplay):
    """ mode specific display """

    def __init__(self,parent):
        ScanDisplay.__init__(self,parent)
        # super(OCTDisplay,self).__init__(parent)
        self.overlay = None
        self.localiser = None
        self.mp_overlay = None

    def increment_slice(self,inc):
        max = self.scan.getSliceCount()-1
        if (self.slice_number +inc) > max:
            self.slice_number = max
        elif (self.slice_number +inc)< 0:
            self.slice_number = 0
        else:
            self.slice_number+=inc
        self.displayScan()

    def displayScan(self):
        """ show the OCT IR and Selected Slice """

        if self.overlay is not None:
            self.drawMPOverlay(self.overlay)
            slice_image = self.scan.overlayMPData(self.overlay, slice=self.slice_number,alpha=0.3)
        else:
            slice_image = self.scan.getSliceImage(self.slice_number)

        ir_image = self.getLocaliser().copy()
        self.scan.drawSliceLine(ir_image,self.slice_number)

        self.drawScanImage(ir_image)
        self.drawSliceImage(slice_image,resize=None)

        # print("Displaying %s"%self.scan)

    def drawMPOverlay(self,mp):
        """
         Overlay MP data to oct
         auto - find registration

        only call when MP changes
        """

        if self.mp_overlay == mp:
            return
        self.localiser = None
        reg = self.scan.person.getRegistration(mp, self.scan)
        # print("Got regisgtation: %s",reg)
        ir_image = self.getLocaliser().copy()

        mp.drawStimuli(image=ir_image,reg=reg,drawHistory=False)
        self.localiser = ir_image
        self.mp_overlay = mp

    def setLocaliser(self,scan):
        """
        Change the backgroundd (localiser image)
        Default is oct ir.
        :return:
        """
        reg = self.scan.person.getRegistration(scan, self.scan)
        print reg,reg.transformed
        self.localiser = cv2.imread(reg.transformed)

    def getLocaliser(self):
        if self.localiser is None:
            return self.scan.getIRImage()
        return self.localiser


if __name__ == "__main__":

    """ Load the data """
    home = os.path.join(os.path.expanduser("~"), "experiments/vision classification/")
    baseDir = os.path.join(home, "scan_registration/data/mital/")
    files = glob.glob(baseDir+"/*/*/*/*.tif")
    files += glob.glob(baseDir+"/*/*/*/*.png")

    data = EyeData.Importer()

    for file in files:
        data.load(file)

    """ manualy register """

    reg_dir = os.path.join(home, 'scan_registration/data/registration/')
    reg = EyeData.ScanRegistrator(reg_dir=reg_dir)

    person = data.people["P10"]
    print person

    oct = person["OCT_P10_V1_OD"]
    swaf = person["SWAF_55_P10_V1_OD"]
    mp = person["MP_V1_OD_P10_6903"]

    swaf_oct = reg.register(source=swaf, target=oct)
    mp_oct = reg.register(source=mp, target=oct)


    person = data.people["C09"]

    oct = person["OCT_C09_V1_OD"]
    swaf = person["SWAF_55_C09_V1_OD"]
    mp = person["MP_V1_OD_C09_6982"]

    swaf_oct = reg.register(source=swaf, target=oct)
    mp_oct = reg.register(source=mp, target=oct)


    root = tk.Tk()
    my_gui = MyFirstGUI(root)
    my_gui.loadPatients(data.people)
    root.mainloop()