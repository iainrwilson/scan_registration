"""


Import the reg_jobs.cvs and parallelise them

"""



import subprocess


fname = "reg_jobs.csv"

n_cores = 12

for i in range(n_cores):
    cmd = "python3 subprocessReg.py %s %d %d &"%(fname,i,n_cores)
    subprocess.call(cmd,shell=True)
    # print(cmd)


