"""

Called by multiprocess reg

"""


import pandas as pd
import sys,os,glob,platform

from scanGui import EyeData





def main(argv):
    fname = argv[0]
    offset = int(argv[1])
    step  = int(argv[2])
    df = pd.read_csv(fname)

    if not os.path.exists(fname):
        print("File not found: %s"%fname)
        sys.exit(0)

    home = os.path.expanduser("~")
    data_dir = os.path.join(home, "experiments/vision classification/scan_registration/data/")
    files = glob.glob(os.path.join(data_dir, "SGP_OCT/*.xml"))

    if platform.system() == "Linux":
        mp_data_dir = "/data/backup/MAIA/MAIA_backup_20181030/"
        mp_fixation_dir = "/data/backup/MAIA/maia-4003_data"
        scan_dir = "/data/iain/Nexus365/"
    else:
        mp_data_dir = "/Users/iain/Documents/backup_maia-4003_20181030"
        mp_fixation_dir = os.path.join(home, "experiments/microperimetry/fixation_data")
        scan_dir = os.path.join(home, "OneDrive/Nexus365")



    print("----- importing Scans ----- ")
    data = EyeData.Importer2(mp_data_folder=mp_data_dir, mp_fixation_dir=mp_fixation_dir)

    # # mital has two MP pid files, load and combine:
    # pid_1 = "/Users/iain/OneDrive/Nexus365/Mital Shah - AOSLO_XML/STGD_Longitudinal_Clinical_Images_XML/MAIA_MP_summary.csv"
    # pid_2 = "/Users/iain/OneDrive/Nexus365/Mital Shah - AOSLO_XML/STGD_Pilot_Clinical_Images_XML/MAIA_MP_summary.csv"
    #
    # df_1 = pd.read_csv(pid_1)
    mp_df = pd.read_csv("mital_mp.csv")
    # df = pd.concat([df_1,df_2])

    pid_file = os.path.join(data_dir, "TRT/patient_ids.csv")
    # df = pd.DataFrame.from_csv(pid_file).dropna().astype(int)



    print("----- importing Scans ----- ")
    data = EyeData.Importer2(mp_data_folder=mp_data_dir, mp_fixation_dir=mp_fixation_dir)

    data.load(scan_dir)

    print("___ Loading MP___")
    for i, row in mp_df.iterrows():
        data.loadMP(row['Participant ID'], row["MAIA PID"], row["MAIA Exam ID"])


    reg_dir = os.path.join(data_dir, 'registration')
    reg = EyeData.ScanRegistrator(reg_dir=reg_dir)


    # print(df)

    for i in range(offset,len(df),step):
        source = data.people[df.iloc[i]['person']][df.iloc[i]['source']]
        target = data.people[df.iloc[i]['person']][df.iloc[i]['target']]
        reg.register(source=source,target=target)


if __name__ == "__main__":
    main(sys.argv[1:])
