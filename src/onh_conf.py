"""

Working out the confidence values


"""

import pandas as pd
import seaborn as sns
import numpy as np

df = pd.read_csv("all_stim.csv")


# try and generate a confidence weight for all stimulus

# filter by movement greater than stimulus size
conf = df[ (df.pid==549) & (df.ID < 8) & (df.scan_number>=5) & (df.movement < 0.46)]# & (df.reg_err_mag<0.6)]

# loop through each ID
ids = conf[conf.response==True].ID.unique()





val_ids=[]

for id in ids:
    values = {}
    for index, row in conf[(conf.ID == id)].iterrows():
        _val = row['value']
        if _val not in values.keys():
            values[_val] = {'count':1,'ID':id,'value':_val,'corr':0}
            if row['response'] == True:
                values[_val]['corr']+=1
        else:
            values[_val]['count']+=1
            if row['response']==True:
                values[_val]['corr']+=1
    val_ids.append(values)

vdf = []
for v in val_ids:
    for k,v in v.iteritems():
        vdf.append(v)

vdf = pd.DataFrame.from_records(vdf)


print val_ids

# look at the percentage correct for each value in
conf[(conf.ID==4)][['value','response']].sort_values(by='value')


vdf['per_corr'] = np.divide(vdf['corr'].astype(float),vdf['count'])
vdf['conf'] = np.multiply(vdf['count'],vdf['per_corr'])

# plot conf / ID
sns.barplot(x='value',y='conf',data=vdf,hue='ID',ci=None)