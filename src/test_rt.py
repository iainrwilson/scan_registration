"""

Load in the TRT data,

tryout the probablilty approach - incorporating the History and eye movements.


"""
import os, glob
import pandas as pd
import EyeData
from EyeData import Scans

import cv2
import numpy as np
import platform


home = os.path.expanduser("~")
dir = 'Dropbox/docs/ORG/Msc/project/experiments/microperimetry/data_from_jasleen/'
dir = os.path.join(home,dir)

files = glob.glob(dir+"CHM new/*threshold.txt")

fnames = {}
for file in files:
    f = os.path.basename(file).split('_')
    eid = int(f[2])
    fnames[eid] = file


# #load glaucoma data.
# g_filename = os.path.join(dir,'glaucoma.csv')
# gc = pd.DataFrame.from_csv(g_filename)
#
# #load normal data
# n_filename = os.path.join(dir,'normals.csv')
# nc = pd.DataFrame.from_csv(n_filename)

#load CHM
c_filename = os.path.join(dir,'chm.csv')
cc = pd.DataFrame.from_csv(c_filename)



chm_pairs = cc[['R T1','R T2']].dropna().astype(int)

# pair_1 = chm_pairs.iloc[20].tolist()
reg_dir = os.path.join(home, 'experiments/microperimetry/registered')
reg = EyeData.ScanRegistrator(reg_dir=reg_dir)

comp = []
for i,row in chm_pairs[:2].iterrows():

    pair_1 = row.tolist()

    # pair_1 = chm_pairs.iloc[16]
    scan_1 = Scans.Scan.create("MP")
    scan_2 = Scans.Scan.create("MP")


    scan_1.getid = scan_1.getOldID
    scan_2.getid = scan_2.getOldID

    eid_1 = scan_1.fixEid(pair_1[0])
    eid_2 = scan_2.fixEid(pair_1[1])


    # skip = [5950,5529,5528,5951]
    # if eid_1 in skip:
    #     continue

    # linux location:
    if platform.system() == "Linux":
        mp_data_dir = "/data/backup/MAIA/MAIA_backup_20180626/"
        mp_fixation_dir = "/data/backup/MAIA/maia-4003_data"
    else:
        mp_data_dir = "/Users/iain/Documents/backup_maia-4003_20180626"
        mp_fixation_dir = os.path.join(home, "experiments/microperimetry/fixation_data")



    scan_1.loadFromMPDataFile(fnames[eid_1],mp_data_dir=mp_data_dir)
    scan_2.loadFromMPDataFile(fnames[eid_2],mp_data_dir=mp_data_dir)

    reg_1_2 = reg.register(scan_1,scan_2)

    # # scan_1_img = scan_1.drawStimuli(image=cv2.imread(reg_1_2.transformed),reg=reg_1_2)
    #
    # scan_1_img = scan_1.drawStimuli(drawHistory=True,patient_data=True,drawIds=True,drawMovement=True)
    # # scan_2.drawStimuli(image=scan_1_img,drawHistory=False,patient_data=True)
    # scan_2_img = scan_2.drawStimuli(drawHistory=True,patient_data=True,drawIds=True,drawMovement=True)
    # # scan_2.drawStimuli(image=scan_1_img,drawHistory=False,patient_data=True))

    # resize = (800,800)
    #
    # scan_1_img = cv2.resize(scan_1_img,resize)
    # scan_2_img = cv2.resize(scan_2_img,resize)
    #

    comp.append(Scans.MP.compare(reg_1_2))


df = pd.concat(comp)


scan_1_img = cv2.rotate(scan_1.getIRImage(),cv2.ROTATE_90_CLOCKWISE)
scan_2_img = cv2.rotate(scan_2.getIRImage(),cv2.ROTATE_90_CLOCKWISE)

scan_1_img = scan_1.drawStimuli(image=scan_1_img,drawHistory=True,rotate=False,drawIds=True)
scan_2_img = scan_2.drawStimuli(image=scan_2_img,drawHistory=True,rotate=False)
cv2.imshow("scan_1",np.hstack([scan_1_img,scan_2_img]))
cv2.waitKey(0)
