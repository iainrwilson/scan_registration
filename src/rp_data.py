"""
Trying some of the RP data = similar format to my scans.

"""
import EyeData
from EyeData import Scans
import os,platform
import cv2
import numpy as np

home = os.path.expanduser("~")
data_dir = os.path.join(home, "experiments/vision classification/scan_registration/data/RP/")


oct_fname = os.path.join(data_dir,"3001-03 OS OCT.tif")

oct_dir =os.path.join(data_dir,"OCT/")

if platform.system() == "Linux":
    mp_data_dir = "/data/backup/MAIA/MAIA_backup_20180626/"
    mp_fixation_dir = "/data/backup/MAIA/maia-4003_data"
else:
    mp_data_dir = "/Users/iain/Documents/backup_maia-4003_20180626"
    mp_fixation_dir = os.path.join(home,"experiments/microperimetry/fixation_data")


data = EyeData.Importer2(mp_data_folder=mp_data_dir,mp_fixation_dir=mp_fixation_dir)

data.load(oct_dir)
data.loadMP("3001-03",444,7563)

reg_dir = os.path.join(data_dir, 'registration')
reg = EyeData.ScanRegistrator(reg_dir=reg_dir)

oct_scan = data.people['3001-03']['OCT_3001-03_1EC897E0_OS']
mp_scan  = data.people['3001-03']['MP_3001-03_OS_7563']

if platform.system() == "Linux":
    mp_oct = reg.register(mp_scan,oct_scan)
else:
    mp_oct = reg.register(mp_scan, oct_scan,dryrun=True)


# slice_number = 53
# oct_img = oct_scan.getIRImage()
# oct_slice = oct_scan.overlayMPData(mp_scan,slice_number,reg=mp_oct,invert=True)
# mp_scan.drawStimuli(image=oct_img,reg=mp_oct,drawHistory=False,drawIds=False,radius=10,fontSize=2)
# oct_scan.drawSliceLine(oct_img,slice_number)
#
#
#
# h = oct_slice.shape[0]
# oct_img = cv2.resize(oct_img,(h,h))
#
# img = np.hstack([oct_img,oct_slice])
#
# cv2.namedWindow("OCT-MP",cv2.WINDOW_NORMAL)
# cv2.imshow("OCT-MP",img)
# cv2.waitKey()

from gui import MyFirstGUI
import tkinter as tk

root = tk.Tk()
my_gui = MyFirstGUI(root)
my_gui.loadPatients(data.people)
root.mainloop()