import platform
# from . EyeData import Importer2
import pandas as pd
import glob,os
from scanGui import EyeData

# """ Load the data """
# home = os.path.join(os.path.expanduser("~"), "experiments/vision classification/")
# baseDir = os.path.join(home, "scan_registration/data/mital/")
# files = glob.glob(baseDir + "/*/*/*/*.tif")
# files += glob.glob(baseDir + "/*/*/*/*.png")
#
# data = EyeData.Importer()
#
# for file in files:
#     data.load(file)
#
# """ manualy register """
#
# reg_dir = os.path.join(home, 'scan_registration/data/registration/')
# reg = EyeData.ScanRegistrator(reg_dir=reg_dir)
#
# person = data.people["P10"]
# # print(person)
#
# oct = person["OCT_P10_V1_OD"]
# swaf = person["SWAF_55_P10_V1_OD"]
# mp = person["MP_V1_OD_P10_6903"]
#
# swaf_oct = reg.register(source=swaf, target=oct)
# mp_oct = reg.register(source=mp, target=oct)
#
# person = data.people["C09"]
#
# oct = person["OCT_C09_V1_OD"]
# swaf = person["SWAF_55_C09_V1_OD"]
# mp = person["MP_V1_OD_C09_6982"]
#
# swaf_oct = reg.register(source=swaf, target=oct)
# mp_oct = reg.register(source=mp, target=oct)

home = os.path.expanduser("~")
data_dir = os.path.join(home, "experiments/vision classification/scan_registration/data/")
files = glob.glob(os.path.join(data_dir, "SGP_OCT/*.xml"))

if platform.system() == "Linux":
    mp_data_dir = "/data/backup/MAIA/MAIA_backup_20181030/"
    mp_fixation_dir = "/data/backup/MAIA/maia-4003_data"
    scan_dir = "/data/iain/Nexus365/"
else:
    mp_data_dir = "/Users/iain/Documents/backup_maia-4003_20181030"
    mp_fixation_dir = os.path.join(home, "experiments/microperimetry/fixation_data")
    scan_dir = os.path.join(home,"OneDrive/Nexus365")


pid_file = os.path.join(data_dir, "TRT/patient_ids.csv")
df = pd.DataFrame.from_csv(pid_file).dropna().astype(int)

print("----- importing Scans ----- ")
data = EyeData.Importer2(mp_data_folder=mp_data_dir, mp_fixation_dir=mp_fixation_dir)




# # mital has two MP pid files, load and combine:
pid_1 = "/Users/iain/OneDrive/Nexus365/Mital Shah - AOSLO_XML/STGD_Longitudinal_Clinical_Images_XML/MAIA_MP_summary.csv"
pid_2 = "/Users/iain/OneDrive/Nexus365/Mital Shah - AOSLO_XML/STGD_Pilot_Clinical_Images_XML/MAIA_MP_summary.csv"

df_1 = pd.read_csv(pid_1)
df_2 = pd.read_csv(pid_2)

df = pd.concat([df_1,df_2])

pid_file = os.path.join(data_dir, "TRT/patient_ids.csv")
# df = pd.DataFrame.from_csv(pid_file).dropna().astype(int)

print("___ Loading MP___")
for i, row in df.iterrows():
    data.loadMP(row['Participant ID'], row["MAIA PID"], row["MAIA Exam ID"])

print("___ Loading OCT___")
data.load(scan_dir)

# data.load("/Users/iain/curec/CVL/participants/DATA/Imaging Data/export")

# print("___ Loding AF ___")

print("----- Finished importing ----- \n")

# try and do some registration
# for each person, find an OCT and register all MP to it.
reg_dir = os.path.join(data_dir, 'registration')
reg = EyeData.ScanRegistrator(reg_dir=reg_dir)

reg_jobs = []

for person in data.people.list():
    # print person
    for oct in data.people[person].mode("OCT"):

        for mp in data.people[person].mode("MP"):
            if mp.eye != oct.eye:
                continue
            if platform.system() == "Linux":
                reg.register(source=mp, target=oct)
            else:
                reg.register(source=mp, target=oct, dryrun=True)

            reg_jobs.append({
                "person": person,
                "source": mp.getID(),
                "target": oct.getID()
            })

        for af in data.people[person].mode("AF"):
            if af.eye != oct.eye:
                continue
            # if platform.system() == "Linux":
            #     reg.register(source=af, target=oct)
            # else:
            #     reg.register(source=af, target=oct, dryrun=True)
            reg_jobs.append({
                            "person":person,
                            "source":af.getID(),
                            "target":oct.getID()
                            })

        for af in data.people[person].mode("AF_IRAF"):
            if af.eye != oct.eye:
                continue
            # if platform.system() == "Linux":
            #     reg.register(source=af, target=oct)
            # else:
            #     reg.register(source=af, target=oct, dryrun=True)

            reg_jobs.append({
                "person": person,
                "source": af.getID(),
                "target": oct.getID()
            })


pd.DataFrame.from_records(reg_jobs).to_csv("reg_jobs.csv")