"""

Optic nerve head test-retest


import all ONH data.

Analise the longitudinal information.

Per patient, combine the first 5 scans and the last 5. compare the values.


"""

import os
import pandas as pd
import platform
import numpy as np
from EyeData import Importer2
# import matplotlib.pyplot as plt
# import seaborn as sns
import glob
import EyeData
import platform

from joblib import Parallel, delayed

home = os.path.expanduser("~")
data_dir = os.path.join(home, "experiments/vision classification/scan_registration/data/")

if platform.system() == "Linux":
    mp_data_dir = "/data/backup/MAIA/MAIA_backup_20180626/"
    mp_fixation_dir = "/data/backup/MAIA/maia-4003_data"
else:
    mp_data_dir = "/Users/iain/Documents/backup_maia-4003_20180626"
    mp_fixation_dir = os.path.join(home,"experiments/microperimetry/fixation_data")


onh_file = os.path.join(data_dir,'TRT/patient_ids.csv')

onh_df = pd.DataFrame.from_csv(onh_file).dropna()

onh_df.reset_index(inplace=True)

data = Importer2(mp_data_folder=mp_data_dir,mp_fixation_dir=mp_fixation_dir)

subjects = {}
n_scans = 10
print ("__ Loading MP Scans __ ")
# for pid in onh_df['Participant'].unique():
#     person = onh_df[onh_df['Participant'] == pid]
#     # row = person.iloc[0]
    # # data.loadAllMP("TRT%02d"%(pid),row['MAIA ID'])

for i,row in onh_df.iterrows():
    data.loadMP("TRT%02d"%(row['Participant']),row['MAIA ID'],int(row['Test ID']))

print ("__ Loading OCT Scans __ ")
# find oct scans
find = os.path.join(data_dir,"TRT_OCT/*.xml")
files = glob.glob(find)
for f in files:
    data.loadXML(f)

print ("__ Registering Scans __")

# register
reg_dir = os.path.join(data_dir, 'registration')
reg = EyeData.ScanRegistrator(reg_dir=reg_dir)

# need a list of PID to OCT scans, we have several scans and need to register to only one.
#  or just to all to all?


def runreg(data):
    global reg
    print "Running Reg: %s %s"%(data[0].getID(),data[1].getID())
    reg.register(source=data[0],target=data[1])

iterable = []
#
for pid,person in data.people.iteritems():
    print ("\t>> %s"%(pid))
    for oct in person.mode("OCT"):
        print ("\t\t>> OCT: %s"%oct.getID())
        for mp in person.mode("MP"):
            print ("\t\t\t>> MP: %s" % mp.getID())
            # iterable.append([mp,oct])
            reg.register(source=mp, target=oct)





# onh_oct = data.people['TRT02']["OCT_TRT02_FB852620_OD"]
#
# for scan in data.people['TRT02'].mode("MP"):
#     if platform.system() == "Linux":
#         mp_oct = reg.register(scan, onh_oct)
#     else:
#         mp_oct = reg.register(scan, onh_oct, dryrun=True)


comb_history={}
all_hist=[]
# # look at one person to start with
for k,v in data.people.iteritems():
    scans = v.mode("MP")
    for df in scans:
        all_hist.append(df.getHistory())
#
a_df = pd.concat(all_hist)
#

#
#
# # make a scatter plot of the histort(one person) with red = and green markers
# df = a_df[a_df['pid'] == 'TRT19']
# c = ['g' if c else 'r' for c in df.response]
# markers = {True: "o", False: "X"}
# # sns.scatterplot(y="value",x='ID',data=df,hue='response')
#
#
#
# combine all thrshoolds - genereate the trt stuff
comb_thresh={}
a_t = []
# for k,v in data.people.iteritems():
pid = 'TRT21'
v = data.people[pid]
# loop through all the MP SCANS
for scan in v.mode("MP"):
    for i,row in scan.data.dataframe.iterrows():
        if row.ID == 0:
            continue

        a_t.append({"pid": pid, 'ID': row.ID,'value':int(row.final_intensity),'scan':scan.data.exam_id})
        if row.ID not in comb_thresh.keys():
            comb_thresh[row.ID] = []
        comb_thresh[row.ID].append(int(row.final_intensity))

a_t = pd.DataFrame.from_records(a_t)

for k,v in comb_thresh.iteritems():
    print "%d: mean: %0.2f std: %0.2f cor: %0.2f"%(k, np.mean(v),np.std(v), np.std(v)*1.96)

# look at points 1,8 as both in ONH, for false positive
# look at 14,7 for false negatives

# false positives are simple, anything not 1 is a false positive
fp = comb_history[1]+comb_history[8]+comb_history[2]+comb_history[9]
count_fp=0
for p in fp:
    if p['response']:
        count_fp+=1

print "False positive rate: ",(float(count_fp)/len(fp))*100.


fn = comb_history[14]+comb_history[7]
count_fn=0
for p in fn:
    if not p['response']:
        if p['value']< -17:
            count_fn+=1

print "False Negative rate: ", (float(count_fn) / len(fn)) * 100.







# looking at on person, one ID, how they vary over time.
#
# t_df = a_df[ a_df.ID==4]
#
# col = ['#e74c3c','#2ecc71']
#
# # sns.scatterplot(x='pid',y='value',data=t_df,palette=sns.color_palette(col),hue='response')
# # plt.xticks(rotation=90)
# # plt.show()
#
# #
# # draw a single slice
# oct = data.people['TRT02']['OCT_TRT02_FB852620_OD']
# mp = data.people['TRT02']['MP_TRT02_OD_7626']
#
# slice_number = 25
# oct_img = oct.getIRImage(slice=slice_number)
# #
# # """ draw the stimuli onto the SWAF image. """
# mp.drawStimuli(image=oct_img,reg=mp_oct,drawIds=False,radius=7)
#
# oct_img = oct.drawMPGrid(mp_scan=mp,reg=mp_oct)
#
# """ Draw the slice line onto the SWAF image"""
# oct.drawSliceLine(oct_img,slice_number)
#
# """ draw the stimuli onto the OCT slice image. """
# oct_slice = oct.overlayMPData(mp,slice_number)
#
# import cv2
# cv2.imwrite("oct_slice.png",oct_slice)
# cv2.imwrite("oct_img.png",oct_img)

# import cv2
# oct_img=cv2.resize(oct_img,(oct_slice.shape[0],oct_slice.shape[0]))
# img = np.hstack([oct_img,oct_slice])
# cv2.namedWindow("OCT-MP",cv2.WINDOW_NORMAL)
# cv2.imshow("OCT-MP",img)
# cv2.waitKey(1)



import seaborn as sns
# eye movements...plot sactter with a line plot as welll.
m, out, eve = mp.data.calcEyeMovements()
ax = sns.scatterplot(x='x_deg', y='y_deg', data=eve, hue='stimID', palette=sns.color_palette("hls", 14))

# loop through and draw lines
for id in [11]:#eve.stimID.unique():
    for c in eve[eve.stimID == id].counter.unique():
        sns.lineplot(x='x_deg',y='y_deg',data=eve[(eve['stimID']==id) & (eve['counter']==c)],label="%d_%d"%(id,c))
        sns.scatterplot(x='x_deg',y='y_deg',data=eve[(eve['stimID']==id) & (eve['counter']==c)])


# # look at the history at this point
# t_df = a_df[(a_df.pid=='TRT02')&(a_df.scan=='MP_TRT02_OD_7626')&(a_df.ID==11)]
#
# # try and merger the two

#
#
# from gui import MyFirstGUI
# import tkinter as tk
#
# root = tk.Tk()
# my_gui = MyFirstGUI(root)
# my_gui.loadPatients(data.people)
# root.mainloop()
