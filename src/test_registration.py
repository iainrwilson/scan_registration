
import os,glob

import EyeData

import cv2
import numpy as np


""" Test this new importer thing """

# Set this to the location
home = os.path.join(os.path.expanduser("~"), "experiments/vision classification/")


# THis searched through your data folder and adds any file with the tif or png extension
dataDir = os.path.join(home, "scan_registration/data/mital/")
files = glob.glob(dataDir+"/*/*/*/*.tif")
files += glob.glob(dataDir+"/*/*/*/*.png")
files += glob.glob(dataDir+"/*/*/*/*.bmp")


""" The MP importer requires the raw data  """
mp_raw_data = os.path.join(home,"scan_registration/data/raw")
data = EyeData.Importer(mp_data_folder=mp_raw_data)


# load the data in that was found, this filters files based on your filename format.
# Have a look in EyeData/Importer.py for details.

for file in files:
    data.load(file)


""" list all patients loaded """
print data.people

""" List all scans loaded """
print data.people.scans()

count = len(data.people.scans())
print "Loaded %d scans for %d patients" %(count,len(data.people))

""" get a specific person """
person = data.people["P10"]

print person

""" get the scans """
oct = person["OCT_P10_V1_OD"]
swaf = person["SWAF_55_P10_V1_OD"]
mp = person["MP_V1_OD_P10_6903"]
colour = person["COLOUR_P10_V1_OD_1"]



"""
 Initialise the registrator
Add the output location, used for loading in already registered scans
"""
reg_dir = os.path.join(home, 'scan_registration/data/registration/')
reg = EyeData.ScanRegistrator(reg_dir=reg_dir)

""" Run the registration """

swaf_oct = reg.register(source=swaf, target=oct)
mp_oct = reg.register(source=mp, target=oct)
colour_oct = reg.register(source=colour, target=oct)

resize = (600,600)

#
# """
# mosaic is the two images overlayed, good for checking registration
# """
# img = cv2.imread(swaf_oct.mosaic)
# img = cv2.resize(img,(300,300))
# cv2.namedWindow("OCT-SWAF Mosaic",cv2.WINDOW_NORMAL)
# cv2.imshow("OCT-SWAF Mosaic",img)
#
# """
# src is the source image (SWAF) after transformation, used in the mosaic
# """
# img = cv2.imread(swaf_oct.src)
# img = cv2.resize(img,(300,300))
# cv2.namedWindow("OCT-SWAF Source",cv2.WINDOW_NORMAL)
# cv2.imshow("OCT-SWAF Source",img)
#
# """
# dst is the destination image (OCT, this one is not transformed bu the sahpe of the image is changed.
# """
# img = cv2.imread(swaf_oct.dst)
# img = cv2.resize(img,(300,300))
# cv2.namedWindow("OCT-SWAF Destination",cv2.WINDOW_NORMAL)
# cv2.imshow("OCT-SWAF Destination",img)
#
#
# """
# transformed is the source image (SWAF) transformed into the coordinate space of the destination (OCT)
# """
# img = cv2.imread(swaf_oct.transformed)
# img = cv2.resize(img,(300,300))
# cv2.namedWindow("OCT-SWAF Transformed",cv2.WINDOW_NORMAL)
# cv2.imshow("OCT-SWAF Transformed",img)


""" Draw MP Stimuli over SWAF image """

slice_number = 26

oct_img = oct.getIRImage(slice=slice_number)
print oct_img.dtype, oct_img.shape


# colour_mp = mp.drawStimuli(image=cv2.imread(colour_oct.transformed),reg=mp_oct)
colour_img = cv2.imread(colour_oct.transformed)
swaf_img = cv2.imread(swaf_oct.transformed)

""" draw the stimuli onto the SWAF image. """
mp.drawStimuli(image=swaf_img,reg=mp_oct,drawIds=False)

""" Draw the slice line onto the SWAF image"""
oct.drawSliceLine(swaf_img,slice_number)

""" draw the stimuli onto the OCT slice image. """
oct_slice = oct.overlayMPData(mp,slice_number)


img = np.hstack([swaf_img,oct_slice])
cv2.namedWindow("OCT-MP",cv2.WINDOW_NORMAL)
cv2.imshow("OCT-MP",img)
# #
# # img = cv2.resize(oct_slice,resize)
# # cv2.namedWindow("OCT-MP_slice",cv2.WINDOW_NORMAL)
# # cv2.imshow("OCT-MP_slice",img)
# #
# #
# #
# # """
# # using the same registration we can draw the MP stimuli onto the SWAF.
# # You need to use the transformed SWAF image, as this is in the same coordinate space as the OCT
# # """
# # img = mp.drawStimuli(image=cv2.imread(swaf_oct.transformed),reg=mp_oct)
# # oct.drawSliceLine(img,slice_number)
# # img = cv2.resize(img,resize)
# # cv2.namedWindow("SWAF-MP",cv2.WINDOW_NORMAL)
# # cv2.imshow("SWAF-MP",img)
#
cv2.waitKey()

