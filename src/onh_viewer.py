"""

running the gui for the ONH data

"""


import os
import pandas as pd
import platform
import numpy as np
from EyeData import Importer2
# import matplotlib.pyplot as plt
# import seaborn as sns
import glob
import EyeData
import platform

from joblib import Parallel, delayed

home = os.path.expanduser("~")
data_dir = os.path.join(home, "experiments/vision classification/scan_registration/data/")

if platform.system() == "Linux":
    mp_data_dir = "/data/backup/MAIA/MAIA_backup_20180626/"
    mp_fixation_dir = "/data/backup/MAIA/maia-4003_data"
else:
    mp_data_dir = "/Users/iain/Documents/backup_maia-4003_20180626"
    mp_fixation_dir = os.path.join(home,"experiments/microperimetry/fixation_data")


onh_file = os.path.join(data_dir,'TRT/patient_ids.csv')

onh_df = pd.DataFrame.from_csv(onh_file).dropna()

onh_df.reset_index(inplace=True)

data = Importer2(mp_data_folder=mp_data_dir,mp_fixation_dir=mp_fixation_dir)

subjects = {}
n_scans = 10
print ("__ Loading MP Scans __ ")
# for pid in onh_df['Participant'].unique():
#     person = onh_df[onh_df['Participant'] == pid]
#     row = person.iloc[0]
#     data.loadAllMP("TRT%02d"%(pid),row['MAIA ID'])
#     # for i,row in person.iterrows():
#     #     data.loadMP("TRT%02d"%(pid),row['MAIA ID'],int(row['Test ID']))

for i,row in onh_df.iterrows():
    data.loadMP("TRT%02d"%(row['Participant']),row['MAIA ID'],int(row['Test ID']))


print ("__ Loading OCT Scans __ ")
# find oct scans
find = os.path.join(data_dir,"TRT_OCT/*.xml")
files = glob.glob(find)
for f in files:
    data.loadXML(f)

print ("__ Registering Scans __")

# register
reg_dir = os.path.join(data_dir, 'registration')
reg = EyeData.ScanRegistrator(reg_dir=reg_dir)

# need a list of PID to OCT scans, we have several scans and need to register to only one.
#  or just to all to all?


def runreg(data):
    global reg
    print "Running Reg: %s %s"%(data[0].getID(),data[1].getID())
    reg.register(source=data[0],target=data[1])

iterable = []

for pid,person in data.people.iteritems():
    print ("\t>> %s"%(pid))
    for oct in person.mode("OCT"):
        print ("\t\t>> OCT: %s"%oct.getID())
        for mp in person.mode("MP"):
            print ("\t\t\t>> MP: %s" % mp.getID())
            # iterable.append([mp,oct])
            reg.register(source=mp, target=oct)


print "__ Running GUI __"

from gui import MyFirstGUI
import tkinter as tk

root = tk.Tk()
my_gui = MyFirstGUI(root)
my_gui.loadPatients(data.people)
root.mainloop()

