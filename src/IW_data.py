from EyeData import Importer2
import EyeData
import os
import cv2
import numpy as np
import glob
import pandas as pd
import platform

from gui import MyFirstGUI

home = os.path.expanduser("~")
data_dir = os.path.join(home, "experiments/vision classification/scan_registration/data/")
files = glob.glob(os.path.join(data_dir,"SGP_OCT/*.xml"))

if platform.system() == "Linux":
    mp_data_dir = "/data/backup/MAIA/MAIA_backup_20180626/"
    mp_fixation_dir = "/data/backup/MAIA/maia-4003_data"
else:
    mp_data_dir = "/Users/iain/Documents/backup_maia-4003_20180626"
    mp_fixation_dir = os.path.join(home,"experiments/microperimetry/fixation_data")

pid_file = os.path.join(data_dir,"TRT/patient_ids.csv")
df = pd.DataFrame.from_csv(pid_file).dropna().astype(int)



print("----- importing Scans ----- ")
data = Importer2(mp_data_folder=mp_data_dir,mp_fixation_dir=mp_fixation_dir)
mp_scans=[]

mp = []
mp.append({"pid":"SGP002","MAIA ID":517,"Test ID":7424})
mp.append({"pid":"SGP002","MAIA ID":517,"Test ID":7423})
mp.append({"pid":"SGP003","MAIA ID":543,"Test ID":7580})
mp.append({"pid":"SGP003","MAIA ID":543,"Test ID":7579})
mp.append({"pid":"SGP004","MAIA ID":539,"Test ID":7560})
mp.append({"pid":"SGP004","MAIA ID":539,"Test ID":7559})
mp.append({"pid":"SGP005","MAIA ID":538,"Test ID":7550})
mp.append({"pid":"SGP005","MAIA ID":538,"Test ID":7549})
mp.append({"pid":"SGP006","MAIA ID":542,"Test ID":7577})
mp.append({"pid":"SGP006","MAIA ID":542,"Test ID":7576})
mp.append({"pid":"SGP007","MAIA ID":541,"Test ID":7574})
mp.append({"pid":"SGP007","MAIA ID":541,"Test ID":7573})
mp.append({"pid":"SGP011","MAIA ID":559,"Test ID":7748})
mp.append({"pid":"SGP012","MAIA ID":566,"Test ID":7835})
# mp.append({"pid":"SGP014","MAIA ID":577,"Test ID":7969})





df = pd.DataFrame.from_records(mp)

print( "___ Loading MP___")
for i,row in df.iterrows():
    data.loadMP(row['pid'],row["MAIA ID"],row["Test ID"])


print( "___ Loading OCT___")
for file in files[:2]:
    data.loadOct(file)


print("___ Loading AF ___")



print("----- Finished importing ----- \n")

# try and do some registration
# for each person, find an OCT and register all MP to it.
reg_dir = os.path.join(data_dir, 'registration')
reg = EyeData.ScanRegistrator(reg_dir=reg_dir)

for person in data.people.list():
    # print person
    for oct in data.people[person].mode("OCT"):
        #for each oct, register all MP to it.
        for mp in data.people[person].mode("MP"):
            if mp.eye != oct.eye:
                continue
            if platform.system() == "Linux":
                reg.register(source=mp,target=oct)
            else:
                reg.register(source=mp, target=oct,dryrun=True)


# import tkinter as tk
# root = tk.Tk()
# my_gui = MyFirstGUI(root)
# my_gui.loadPatients(data.people)
# root.mainloop()
