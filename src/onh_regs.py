"""

register all the ONH MP data
"""
import os
import pandas as pd
import platform
import numpy as np
from EyeData import Importer2
import matplotlib.pyplot as plt
import seaborn as sns
import glob
import EyeData
import platform



home = os.path.expanduser("~")
data_dir = os.path.join(home, "experiments/vision classification/scan_registration/data/")

if platform.system() == "Linux":
    mp_data_dir = "/data/backup/MAIA/MAIA_backup_20180626/"
    mp_fixation_dir = "/data/backup/MAIA/maia-4003_data"
else:
    mp_data_dir = "/Users/iain/Documents/backup_maia-4003_20180626"
    mp_fixation_dir = os.path.join(home,"experiments/microperimetry/fixation_data")


onh_file = os.path.join(data_dir,'TRT/patient_ids.csv')

onh_df = pd.DataFrame.from_csv(onh_file).dropna()

onh_df.reset_index(inplace=True)

data = Importer2(mp_data_folder=mp_data_dir,mp_fixation_dir=mp_fixation_dir)



subjects = {}
n_scans = 10
print ("__ Loading MP Scans __ ")
# for pid in onh_df['Participant'].unique():
#     person = onh_df[onh_df['Participant'] == pid]
#     # row = person.iloc[0]
    # # data.loadAllMP("TRT%02d"%(pid),row['MAIA ID'])
for i,row in onh_df.iterrows():
    data.loadMP("TRT%02d"%(row['Participant']),row['MAIA ID'],int(row['Test ID']))

# print data.people


print ("__ Loading OCT Scans __ ")
find = os.path.join(data_dir,"TRT_OCT/*.xml")
files = glob.glob(find)
for f in files:
    data.loadXML(f)



# register
reg_dir = os.path.join(data_dir, 'registration')
reg = EyeData.ScanRegistrator(reg_dir=reg_dir)


print ("__ registration __")
for pid,person in data.people.iteritems():
    print ("\t>> %s"%(pid))
    for oct in person.mode("OCT"):
        print ("\t\t>> OCT: %s"%oct.getID())
        for mp in person.mode("MP"):
            print ("\t\t\t>> MP: %s" % mp.getID())
            # iterable.append([mp,oct])
            reg.register(source=mp, target=oct)




#
# just look at one person
# pid = "TRT21"
results = []
for pid,person in data.people.iteritems():
    # person = data.people[pid]
    print ("__ Processing %s __" %pid)

    # person = data.people[pid]
    scans = person.mode("MP")
    if len(scans)==0:
        print ("  >> no data <<")
        continue

    scans[0].data.calcEyeMovements()

    # waht is this persons mp pid
    mp_pid = scans[0].data.patient_id

    # analyse all
    compare = []
    history = []
    _hist = scans[0].getHistory()
    _hist['scan_number'] = 0
    history.append(_hist)
    for i in range(1,len(scans)):
        if scans[i].data.length != 15:
            continue
        reg.register(scans[i],scans[0])
        _reg = reg.getRegistration(scans[i], scans[0])
        if _reg is None:
            print ("  >> %s : no reg <<"%(scans[i].getID()))
            continue
        scans[i].data.calcEyeMovements()
        df = EyeData.Scans.MP.compare(_reg)
        compare.append(df)

        #also build history
        _hist = scans[i].getHistory()
        _hist['scan_number'] = i
        history.append(_hist)

    df_h = pd.concat(history)
    df_h.reset_index(inplace=True)
    compare = pd.concat(compare)

    # TODO add the registration error to the df_h - target is always the first scan
    df_h['reg_err']= None
    df_h['reg_err_mag'] = 0.0
    for i,row in df_h.iterrows():
        eid = int(row['scan'][-4:])
        # print eid
        if row['scan_number']==0:
            df_h.set_value(i, 'reg_err',0)
            continue

        cmp = compare[(compare['pid'] == mp_pid) & (compare['eid_1'] == eid) & (compare['ID'] == row['ID'])]
        # print cmp['registration_error_magnitude'],i
        df_h.set_value(i,'reg_err_mag', cmp['registration_error_magnitude'])
        df_h.set_value(i, 'reg_err', cmp['registration_error'].values[0])


    results.append(df_h)


n_df_h = pd.concat(results)

# looking at ID 4
print n_df_h[(n_df_h.pid==573) & (n_df_h.ID==4)][['movement','value','response','reg_err','scan']]

# draw out slice 23

df = n_df_h
# # plotting ID 4
#
# id_4 = df_h[(df_h.ID == 4) & (df_h.movement < 0.5) & (df_h.response == True)]
# id_4 = df_h[(df_h.ID == 4)]
#
#
# col = ['#e74c3c','#2ecc71']
# sns.scatterplot(x='scan_number',y='value',data=df_h[(df_h.ID == 4)],hue='response',palette=sns.color_palette(col),size='reg_err')
# # sns.scatterplot(x='scan',y='value',data=id_4,size='movement')
# plt.xticks(rotation=90)
# plt.show()
#
#
