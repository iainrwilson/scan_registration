import sys
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow,QVBoxLayout,QHBoxLayout,QLabel

from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from PyQt5.QtGui import QStandardItemModel,QStandardItem
from PyQt5.QtCore import Qt
from scanGui import EyeData
import glob,os

from scanGui.ScanWidget import ScanWidget
from scanGui.OCTScanWidget import OCTScanWidget
from scanGui.MPScanWidget import MPScanWidget
from scanGui.AFScanWidget import AFScanWidget

from scanGui.SettingsDialog import SettingsDialog

class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.title = 'PyQt5'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()



    def initUI(self):
        #set central widget
        wid = QWidget(self)
        self.setCentralWidget(wid)


        self.vbox = QVBoxLayout()
        self.patient_list = QListWidget()
        self.patient_list.itemClicked.connect(self.patient_clicked)

        self.scan_list = QListWidget()
        self.scan_list.itemClicked.connect(self.scan_clicked)

        self.reg_list = QListWidget()
        self.reg_list.itemClicked.connect(self.reg_clicked)

        self.vbox.addWidget(QLabel("Patient List"))
        self.vbox.addWidget(self.patient_list)
        self.vbox.addWidget(QLabel("Scan List"))
        self.vbox.addWidget(self.scan_list)
        self.vbox.addWidget(QLabel("Registration List"))
        self.vbox.addWidget(self.reg_list)

        self.vboxWrapper = QWidget()
        self.vboxWrapper.setFixedWidth(200)
        self.vboxWrapper.setLayout(self.vbox)

        self.hbox = QHBoxLayout()
        # self.hbox.addStretch()
        self.hbox.addWidget(self.vboxWrapper)

        self.scanWidget = ScanWidget(parent=None)
        self.hbox.addWidget(self.scanWidget)
        wid.setLayout(self.hbox)

        # self.statusBar().showMessage('Message in statusbar.')
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)



        # configure menu
        bar = self.menuBar()
        file = bar.addMenu("File")
        file.addAction("Pref")
        # edit = file.addMenu("Edit")
        # edit.addAction("copy")
        # edit.addAction("paste")
        #
        # bar.addMenu("Edit")
        file.triggered[QAction].connect(self.processTrigger)

        self.show()

    def processTrigger(self,q):
        print(q.text() + " is triggered")
        dialog = SettingsDialog(self)
        dialog.exec_()
        print("Scan data " , len(dialog.data.people))
        if dialog.data is not None:
            self.setData(dialog.data)

    def setData(self,data):
        self.data = data
        self.loadPatients()

    def loadPatients(self):
        """
        add all patients to list
        :param data:
        :return:
        """

        # model = QStandardItemModel()

        for p in self.data.people.list():
            # item = QStandardItem(p)
            # item.setData(self.data.people[p],role=Qt.UserRole)
            # model.appendRow(item)
            item = QListWidgetItem(p,self.patient_list)
            item.setData(Qt.UserRole,self.data.people[p])
            self.patient_list.addItem(item)
            # print(p)

        # self.patient_list.setModel(model)

    def patient_clicked(self,item):
        # print("Clicked", str(item.text()))
        self.loadScans(item.data(Qt.UserRole))

    def loadScans(self,patient):
        # clear scans

        self.current_patient = patient
        self.scan_list.clear()

        scans = patient.mode("OCT")
        for s in scans:
            item = QListWidgetItem(s.getID(), self.scan_list)
            item.setData(Qt.UserRole,s)
            # print(s)
        sep = QListWidgetItem()
        sep.setText("----------")
        sep.setFlags(Qt.NoItemFlags)
        self.scan_list.addItem(sep)

        scans = patient.mode("MP")
        for s in scans:
            item = QListWidgetItem(s.getID(), self.scan_list)
            item.setData(Qt.UserRole, s)

        sep = QListWidgetItem()
        sep.setText("----------")
        sep.setFlags(Qt.NoItemFlags)
        self.scan_list.addItem(sep)


        scans = patient.mode("AF")
        for s in scans:
            item = QListWidgetItem(s.getID(), self.scan_list)
            item.setData(Qt.UserRole, s)

        scans = patient.mode("AF_IRAF")
        for s in scans:
            item = QListWidgetItem(s.getID(), self.scan_list)
            item.setData(Qt.UserRole, s)

        # self.scan_list.addItem(sep)



    def scan_clicked(self,item):
        # print("Scan %s clicked"%item.text())
        self.current_scan = item.data(Qt.UserRole)
        if self.current_scan is None:
            return

        self.hbox.removeWidget(self.scanWidget)
        self.scanWidget.deleteLater()

        # work out which scan it is
        mode = self.current_scan.mode
        if mode == "OCT":
            self.scanWidget = OCTScanWidget()
        elif mode == "MP":
            self.scanWidget = MPScanWidget()
        elif mode == "AF_IRAF" or mode == "AF":
            self.scanWidget = AFScanWidget()

        else:
            self.scanWidget = ScanWidget()

        self.hbox.addWidget(self.scanWidget)
        self.hbox.update()
        self.update()
        self.scanWidget.loadScan(self.current_scan)
        self.loadRegistrations()

    def loadRegistrations(self):
        """
        Find all registratison associtaed with this scan.
        :return:
        """
        self.reg_list.clear()

        sources = self.current_patient.registrations.getSources(self.current_scan)
        # print("Found %d sources"%len(sources))
        # print(sources)
        if len(sources)>0:
            for source in sources:
                s = source.source
                item = QListWidgetItem(s.getID(),self.reg_list)
                item.setData(Qt.UserRole,source)



    def reg_clicked(self,item):

        # print(item.text())
        self.scanWidget.setReg(item.data(Qt.UserRole))



if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = App()
    dialog = SettingsDialog()
    ex.setData(dialog.data)
    sys.exit(app.exec_())