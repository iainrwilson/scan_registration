# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'scanwidget.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ScanWidget(object):
    def setupUi(self, ScanWidget):
        ScanWidget.setObjectName("ScanWidget")
        ScanWidget.resize(1520, 589)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(ScanWidget.sizePolicy().hasHeightForWidth())
        ScanWidget.setSizePolicy(sizePolicy)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(ScanWidget)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.localiserLabel = OCTLocaliserImage(ScanWidget)
        self.localiserLabel.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.localiserLabel.sizePolicy().hasHeightForWidth())
        self.localiserLabel.setSizePolicy(sizePolicy)
        self.localiserLabel.setMinimumSize(QtCore.QSize(256, 256))
        self.localiserLabel.setScaledContents(True)
        self.localiserLabel.setObjectName("localiserLabel")
        self.verticalLayout.addWidget(self.localiserLabel)
        self.imageInfo = QtWidgets.QGroupBox(ScanWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.imageInfo.sizePolicy().hasHeightForWidth())
        self.imageInfo.setSizePolicy(sizePolicy)
        self.imageInfo.setMinimumSize(QtCore.QSize(401, 141))
        font = QtGui.QFont()
        font.setPointSize(13)
        self.imageInfo.setFont(font)
        self.imageInfo.setStyleSheet("QGroupBox {\n"
"    border: 2px solid grey;\n"
"    border-radius: 10px;\n"
"}")
        self.imageInfo.setFlat(False)
        self.imageInfo.setCheckable(False)
        self.imageInfo.setObjectName("imageInfo")
        self.label = QtWidgets.QLabel(self.imageInfo)
        self.label.setGeometry(QtCore.QRect(10, 30, 31, 16))
        self.label.setObjectName("label")
        self.scanModeLabel = QtWidgets.QLabel(self.imageInfo)
        self.scanModeLabel.setGeometry(QtCore.QRect(60, 30, 71, 16))
        self.scanModeLabel.setObjectName("scanModeLabel")
        self.scanResolutionLabel = QtWidgets.QLabel(self.imageInfo)
        self.scanResolutionLabel.setGeometry(QtCore.QRect(60, 50, 101, 16))
        self.scanResolutionLabel.setObjectName("scanResolutionLabel")
        self.label_2 = QtWidgets.QLabel(self.imageInfo)
        self.label_2.setGeometry(QtCore.QRect(10, 50, 71, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.imageInfo)
        self.label_3.setGeometry(QtCore.QRect(180, 30, 41, 16))
        self.label_3.setObjectName("label_3")
        self.dateLabel = QtWidgets.QLabel(self.imageInfo)
        self.dateLabel.setGeometry(QtCore.QRect(230, 30, 161, 16))
        self.dateLabel.setObjectName("dateLabel")
        self.label_4 = QtWidgets.QLabel(self.imageInfo)
        self.label_4.setGeometry(QtCore.QRect(180, 50, 41, 16))
        self.label_4.setObjectName("label_4")
        self.scanAngleLabel = QtWidgets.QLabel(self.imageInfo)
        self.scanAngleLabel.setGeometry(QtCore.QRect(230, 50, 71, 16))
        self.scanAngleLabel.setObjectName("scanAngleLabel")
        self.label_5 = QtWidgets.QLabel(self.imageInfo)
        self.label_5.setGeometry(QtCore.QRect(10, 70, 41, 16))
        self.label_5.setObjectName("label_5")
        self.scanScaleLabel = QtWidgets.QLabel(self.imageInfo)
        self.scanScaleLabel.setGeometry(QtCore.QRect(60, 70, 111, 16))
        self.scanScaleLabel.setObjectName("scanScaleLabel")
        self.label_6 = QtWidgets.QLabel(self.imageInfo)
        self.label_6.setGeometry(QtCore.QRect(180, 70, 41, 16))
        self.label_6.setObjectName("label_6")
        self.scanFocusLabel = QtWidgets.QLabel(self.imageInfo)
        self.scanFocusLabel.setGeometry(QtCore.QRect(230, 70, 71, 16))
        self.scanFocusLabel.setObjectName("scanFocusLabel")
        self.label_7 = QtWidgets.QLabel(self.imageInfo)
        self.label_7.setGeometry(QtCore.QRect(10, 90, 41, 16))
        self.label_7.setObjectName("label_7")
        self.scanNumAvgLabel = QtWidgets.QLabel(self.imageInfo)
        self.scanNumAvgLabel.setGeometry(QtCore.QRect(60, 90, 71, 16))
        self.scanNumAvgLabel.setObjectName("scanNumAvgLabel")
        self.label_8 = QtWidgets.QLabel(self.imageInfo)
        self.label_8.setGeometry(QtCore.QRect(180, 90, 41, 16))
        self.label_8.setObjectName("label_8")
        self.scanGainLabel = QtWidgets.QLabel(self.imageInfo)
        self.scanGainLabel.setGeometry(QtCore.QRect(230, 90, 71, 16))
        self.scanGainLabel.setObjectName("scanGainLabel")
        self.verticalLayout.addWidget(self.imageInfo)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.sliceLabel = OCTSliceImage(ScanWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sliceLabel.sizePolicy().hasHeightForWidth())
        self.sliceLabel.setSizePolicy(sizePolicy)
        self.sliceLabel.setMinimumSize(QtCore.QSize(256, 256))
        self.sliceLabel.setObjectName("sliceLabel")
        self.verticalLayout_2.addWidget(self.sliceLabel)
        self.sliceInfo = QtWidgets.QGroupBox(ScanWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sliceInfo.sizePolicy().hasHeightForWidth())
        self.sliceInfo.setSizePolicy(sizePolicy)
        self.sliceInfo.setMinimumSize(QtCore.QSize(401, 141))
        font = QtGui.QFont()
        font.setPointSize(13)
        self.sliceInfo.setFont(font)
        self.sliceInfo.setStyleSheet("QGroupBox {\n"
"    border: 2px solid grey;\n"
"    border-radius: 10px;\n"
"}")
        self.sliceInfo.setFlat(False)
        self.sliceInfo.setCheckable(False)
        self.sliceInfo.setObjectName("sliceInfo")
        self.label_9 = QtWidgets.QLabel(self.sliceInfo)
        self.label_9.setGeometry(QtCore.QRect(10, 30, 51, 16))
        self.label_9.setObjectName("label_9")
        self.sliceNumberLabel = QtWidgets.QLabel(self.sliceInfo)
        self.sliceNumberLabel.setGeometry(QtCore.QRect(150, 30, 61, 16))
        self.sliceNumberLabel.setObjectName("sliceNumberLabel")
        self.sliceResolutionLabel = QtWidgets.QLabel(self.sliceInfo)
        self.sliceResolutionLabel.setGeometry(QtCore.QRect(70, 50, 101, 16))
        self.sliceResolutionLabel.setObjectName("sliceResolutionLabel")
        self.label_10 = QtWidgets.QLabel(self.sliceInfo)
        self.label_10.setGeometry(QtCore.QRect(10, 50, 71, 16))
        self.label_10.setObjectName("label_10")
        self.label_13 = QtWidgets.QLabel(self.sliceInfo)
        self.label_13.setGeometry(QtCore.QRect(10, 70, 41, 16))
        self.label_13.setObjectName("label_13")
        self.sliceScaleLabel = QtWidgets.QLabel(self.sliceInfo)
        self.sliceScaleLabel.setGeometry(QtCore.QRect(70, 70, 111, 16))
        self.sliceScaleLabel.setObjectName("sliceScaleLabel")
        self.spinbox = QtWidgets.QSpinBox(self.sliceInfo)
        self.spinbox.setGeometry(QtCore.QRect(70, 30, 42, 22))
        self.spinbox.setObjectName("spinbox")
        self.label_11 = QtWidgets.QLabel(self.sliceInfo)
        self.label_11.setGeometry(QtCore.QRect(120, 30, 16, 16))
        self.label_11.setObjectName("label_11")
        self.overlayButton = QtWidgets.QPushButton(self.sliceInfo)
        self.overlayButton.setGeometry(QtCore.QRect(10, 90, 114, 32))
        self.overlayButton.setObjectName("overlayButton")
        self.verticalLayout_2.addWidget(self.sliceInfo)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.mpInfoBox = MPInfoBox(ScanWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mpInfoBox.sizePolicy().hasHeightForWidth())
        self.mpInfoBox.setSizePolicy(sizePolicy)
        self.mpInfoBox.setMinimumSize(QtCore.QSize(430, 516))
        self.mpInfoBox.setObjectName("mpInfoBox")
        self.verticalLayout_3.addWidget(self.mpInfoBox)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        spacerItem1 = QtWidgets.QSpacerItem(16, 17, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout_4.addLayout(self.horizontalLayout)
        spacerItem2 = QtWidgets.QSpacerItem(20, 196, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem2)

        self.retranslateUi(ScanWidget)
        QtCore.QMetaObject.connectSlotsByName(ScanWidget)

    def retranslateUi(self, ScanWidget):
        _translate = QtCore.QCoreApplication.translate
        ScanWidget.setWindowTitle(_translate("ScanWidget", "Form"))
        self.localiserLabel.setText(_translate("ScanWidget", "TextLabel"))
        self.imageInfo.setTitle(_translate("ScanWidget", "Image Information"))
        self.label.setText(_translate("ScanWidget", "Type: "))
        self.scanModeLabel.setText(_translate("ScanWidget", "scan mode"))
        self.scanResolutionLabel.setText(_translate("ScanWidget", "scan res"))
        self.label_2.setText(_translate("ScanWidget", "Res:"))
        self.label_3.setText(_translate("ScanWidget", "Date:"))
        self.dateLabel.setText(_translate("ScanWidget", "scan date"))
        self.label_4.setText(_translate("ScanWidget", "Angle:"))
        self.scanAngleLabel.setText(_translate("ScanWidget", "scan angle"))
        self.label_5.setText(_translate("ScanWidget", "Scale:"))
        self.scanScaleLabel.setText(_translate("ScanWidget", "scan scale"))
        self.label_6.setText(_translate("ScanWidget", "Focus:"))
        self.scanFocusLabel.setText(_translate("ScanWidget", "scan focus"))
        self.label_7.setText(_translate("ScanWidget", "Avg:"))
        self.scanNumAvgLabel.setText(_translate("ScanWidget", "scan avg"))
        self.label_8.setText(_translate("ScanWidget", "Gain:"))
        self.scanGainLabel.setText(_translate("ScanWidget", "scan gain"))
        self.sliceLabel.setText(_translate("ScanWidget", "TextLabel"))
        self.sliceInfo.setTitle(_translate("ScanWidget", "Slice Information"))
        self.label_9.setText(_translate("ScanWidget", "Number"))
        self.sliceNumberLabel.setText(_translate("ScanWidget", "scan mode"))
        self.sliceResolutionLabel.setText(_translate("ScanWidget", "scan res"))
        self.label_10.setText(_translate("ScanWidget", "Res:"))
        self.label_13.setText(_translate("ScanWidget", "Scale:"))
        self.sliceScaleLabel.setText(_translate("ScanWidget", "scan scale"))
        self.label_11.setText(_translate("ScanWidget", "of"))
        self.overlayButton.setText(_translate("ScanWidget", "Hide MP Grid"))
        self.mpInfoBox.setTitle(_translate("ScanWidget", "GroupBox"))

from .MPInfoBox import MPInfoBox
from .OCTLocaliserImage import OCTLocaliserImage
from .OCTSliceImage import OCTSliceImage

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ScanWidget = QtWidgets.QWidget()
    ui = Ui_ScanWidget()
    ui.setupUi(ScanWidget)
    ScanWidget.show()
    sys.exit(app.exec_())

