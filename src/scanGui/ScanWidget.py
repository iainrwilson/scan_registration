"""

PyQT5 scan Widgert


Base class for the scan display widget

just do oct to start.

"""
from PyQt5.QtWidgets import QWidget,QLabel,QVBoxLayout,QSplitter,QHBoxLayout
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtCore import Qt
import cv2
import numpy as np
import glob,os

from .ScanImage import ScanImage
from .Markers import Markers
from .ScanWidgetGui import Ui_ScanWidget

class ScanWidget(QWidget,Ui_ScanWidget):

    def __init__(self,parent=None, scan=None):
        # super().__init__(parent)
        QWidget.__init__(self,parent=parent)
        Ui_ScanWidget.__init__(self)
        self.setupUi(self)

        # self.minSize = QSize(400,400)



        # self.splitter = QSplitter(Qt.Vertical)
        # self.vbox = QVBoxLayout()
        # self.hbox = QHBoxLayout()
        # self.localiserLabel = ScanImage(self,scan=scan)
        # self.hbox.addWidget(self.localiserLabel)
        # self.vbox.addLayout(self.hbox)
        # self.setLayout(self.vbox)
        self.sliceNumber = 0

        self.reg = None
        self.alt_localiser = None
        if scan is not None:
            self.loadScan(scan)
        self.markers = None

    def draw(self):
        self.localiserLabel.update()

    def mouseMoveEvent(self, event):
        print("Move: ",event.pos())

    # def minimumSizeHint(self):
    #     return self.minSize
    #
    # def sizeHint(self):
    #     return self.minSize

    def loadScan(self,scan):
        self.scan = scan
        self.markers = self.findMarkers()
        self.loadLocaliser()
        self.draw()

    def findMarkers(self):
        """ search in a default location for a marker file"""
        settings = QSettings("oculab", "OCT Labeler")
        dir = settings.value("marker_data_dir")
        # dir = "/Users/iain/Dropbox/docs/ORG/Msc/project/experiments/vision classification/scan_registration/data/markers"

        if not os.path.exists(dir):
            print("%s does not exist"%dir)
            return Markers(self.scan)

        files = glob.glob(dir+"/*.csv")
        markers = Markers(self.scan)
        for f in files:
            if os.path.basename(f)[8:-4] == self.scan.getID():
                markers.read(fname=f)
                return markers
        return markers



    def saveMarkers(self):
        if self.markers is not None:
            self.markers.save()

    def __del__(self):
        # print("\tScanWidgted destructor called")
        self.saveMarkers()

    def loadLocaliser(self):
        self.localiserLabel.load(self.scan,self.markers)

        self.scanModeLabel.setText(self.scan.mode)
        text = "%dx%d"%(self.scan.localiser_width,self.scan.localiser_height)
        self.scanResolutionLabel.setText(text)
        text = "%.4fx%.4f"%(self.scan.scale_x,self.scan.scale_y)
        self.scanScaleLabel.setText(text)

        self.scanAngleLabel.setText("%.2f"%self.scan.localiser_angle)
        self.dateLabel.setText(self.scan.datetime.strftime("%H:%M %d/%m/%Y"))
        self.scanFocusLabel.setText("%.2f"%self.scan.localiser_focus)
        self.scanGainLabel.setText("%.2f"%self.scan.localiser_sensor_gain)
        self.scanNumAvgLabel.setText("%d"%self.scan.localiser_num_ave)


    def overlaySliceLine(self,image=None):
        """
        Overlay OCT slice line on to localiser
        :return:
        """
        pass


    def setReg(self,reg):
        """
        Overlay new scan onto localiser
        :param reg:
        :return:
        """
        if self.reg.source.mode == "MP":
            self.reg = reg
        else:
            self.alt_localiser = reg

    def cv2pixmap(self,image):
        """
        convert opencv image to pixmap
        :return:
        """
        height, width, bytesPerComponent = image.shape
        bytesPerLine = 3 * width
        cv2.cvtColor(image, cv2.COLOR_BGR2RGB, image)
        qimg = QImage(image.tobytes(), width, height, bytesPerLine, QImage.Format_RGB888)
        return QPixmap.fromImage(qimg)

    def wheelEvent(self, event):
        pass



    def keyPressEvent(self, event):
        print ("Key pressed")
        if event.key() == Qt.Key_Up:
            self.sliceNumber+=1

        elif event.key() == Qt.Key_Down:
            self.sliceNumber-=1


        if self.sliceNumber >= self.scan.getSliceCount():
            self.sliceNumber = self.scan.getSliceCount()-1
        elif self.sliceNumber <0:
            self.sliceNumber = 0

        self.drawLocaliser()
        self.loadSlice()