import sys,platform,os
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QSettings
from PyQt5 import QtGui
import glob

from .SettingsDialogGui import Ui_Dialog
from .EyeData import Importer2, ScanRegistrator


class SettingsDialog(QDialog,Ui_Dialog):
    """
    Class that handles all the settings.
    Folder loacations for the importer . registrator etc

    """


    def __init__(self,parent=None):
        QDialog.__init__(self,parent=parent)
        Ui_Dialog.__init__(self)
        self.setupUi(self)
        self.home = os.path.expanduser("~")

        self.data = None
        self.reg = None

        self.mp_data_dir = None
        self.mp_fixation_dir = None
        self.scan_dir = None
        self.marker_data_dir = None


        if platform.system() == "Linux":
            self.mp_data_dir = "/data/backup/MAIA/MAIA_backup_20181030/"
            self.mp_fixation_dir = "/data/backup/MAIA/maia-4003_data"
        else:
            self.mp_data_dir = os.path.join(self.home,"Documents/backup_maia-4003_20181030")
            self.mp_fixation_dir = os.path.join(self.home, "experiments/microperimetry/fixation_data")

        self.scan_dir = os.path.join(self.home,"curec/CVL/participants/DATA/Imaging Data/export/")

        self.reg_dir = os.path.join(self.home, "experiments/vision classification/scan_registration/data/registration")

        self.mp_data_file = os.path.join(self.home,"experiments/vision classification/scan_registration/data/mp_data_file.csv")

        self.marker_data_dir = os.path.join(self.home,"experiments/vision classification/scan_registration/data/markers")

        # self.scanUpdateButton.clicked.connect(self.updateScanInfo)
        # self.regUpdateButton.clicked.connect(self.updateReg)
        # self.mpDataFileUpdate.clicked.connect(self.updateMP)




        self.scanDataFolderButton.clicked.connect(self.scanDataDirButton)
        self.mpDataFileButton.clicked.connect(self.mpDataFileButtonClicked)
        self.mpDataFolderButton.clicked.connect(self.mpDataFolderButtonClicked)
        self.markerDataFolderButton.clicked.connect(self.markerDataFolderButtonClicked)
        self.regDataFolderButton.clicked.connect(self.regDataFolderButtonClicked)
        self.setup()

    def regDataFolderButtonClicked(self):
        path = self.reg_dir
        dir = str(QFileDialog.getExistingDirectory(self, "Select Directory", path))
        self.reg_dir = dir
        self.markerDataEdit.setText(self.reg_dir)
        self.setup()


    def markerDataFolderButtonClicked(self):
        path = self.marker_data_dir
        dir = str(QFileDialog.getExistingDirectory(self, "Select Directory", path))
        self.marker_data_dir = dir
        self.markerDataEdit.setText(self.marker_data_dir)

    def mpDataFileButtonClicked(self):
        filter = "csv(*.csv)"
        path = os.path.dirname(self.mp_data_file)
        file,_ = QFileDialog.getOpenFileName(self,"Select csv file",path,filter)
        # print(file)
        self.mp_data_file = file
        self.mpDataFileEdit.setText(self.mp_data_file)
        self.updateMP()

    def scanDataDirButton(self):
        path = self.scan_dir
        dir = str(QFileDialog.getExistingDirectory(self, "Select Directory",path))
        self.scan_dir = dir
        self.scanDataEdit.setText(self.scan_dir)
        self.updateScanInfo()

    def mpDataFolderButtonClicked(self):
        path = self.mp_data_dir
        dir = str(QFileDialog.getExistingDirectory(self, "Select Directory", path))
        self.mp_data_dir = dir
        self.mpDataEdit.setText(self.mp_data_dir)
        self.updateMP()



    def updateMP(self,data=None):
        import pandas as pd

        self.mp_data_file = self.mpDataFileEdit.text()

        if not os.path.exists(self.mp_data_file):
            self.mpDataFileInfoLabel.setText("File does not exist!")
            return

        if data is None:
            data = Importer2(mp_data_folder=self.mp_data_dir, mp_fixation_dir=self.mp_fixation_dir)

        df = pd.read_csv(self.mp_data_file)
        # print(df)
        for i, row in df.iterrows():
            data.loadMP(row['Participant ID'], row["MAIA PID"], row["MAIA Exam ID"])

            # data.loadMP(row['pid'], row["MAIA ID"], row["Test ID"])

        mp = len(data.people.mode("MP"))

        text = "Found %d MP scans" % (mp)
        self.mpDataFileInfoLabel.setText(text)

    def updateScanInfo(self):

        data = Importer2(mp_data_folder=self.mp_data_dir,mp_fixation_dir=self.mp_fixation_dir)
        data.load(self.scanDataEdit.text())
        # mp = len(self.data.people.mode("MP"))
        oct = len(data.people.mode("OCT"))
        af = len(data.people.mode("AF"))
        af += len(data.people.mode("AF_IRAF"))

        text = "Found %d subjects: %d OCT, %d AF" % (len(data.people), oct, af)
        self.scanDataInfoLabel.setText(text)
        return data

    def updateReg(self,data):
        self.reg_dir = self.regDataEdit.text()
        reg = ScanRegistrator(reg_dir=self.reg_dir)
        self.runRegistration(reg,data)

        # print(reg.registrations)

        text = "Found %d registrations"%(len(reg.registrations))
        self.regDataInfoLabel.setText(text)
        return reg

    def runRegistration(self,reg,data):
        if data is None:
            return

        for person in data.people.list():
            for oct in data.people[person].mode("OCT"):
                # for each oct, register all MP to it.
                for mp in data.people[person].mode("MP"):
                    if mp.eye != oct.eye:
                        continue
                    reg.register(source=mp, target=oct, dryrun=True)

                for af in data.people[person].mode("AF"):
                    if af.eye != oct.eye:
                        continue
                    reg.register(source=af, target=oct, dryrun=True)

                for af in data.people[person].mode("AF_IRAF"):
                    if af.eye != oct.eye:
                        continue
                    reg.register(source=af, target=oct, dryrun=True)

    def setup(self):
        """ configure the Gui """
        # Ui_Dialog.setupUi(self)

        print("Loading defaults:: ")


        # look for local settings, overide defaults if they exist
        self.settings = QSettings("oculab","OCT Labeler")

        scan_dir = self.settings.value("scan_dir")
        mp_data_dir = self.settings.value("mp_data_dir")
        marker_data_dir = self.settings.value("marker_data_dir")
        reg_dir = self.settings.value("reg_dir")
        mp_data_file = self.settings.value("mp_data_file")

        if scan_dir is not None:
            self.scan_dir = scan_dir
        if mp_data_dir is not None:
            self.mp_data_dir = mp_data_dir
        if marker_data_dir is not None:
            if marker_data_dir != "":
                self.marker_data_dir = marker_data_dir
        if reg_dir is not None:
            self.reg_dir = reg_dir
        if mp_data_file is not None:
            self.mp_data_file = mp_data_file

        self.scanDataEdit.setText(self.scan_dir)
        self.mpDataEdit.setText(self.mp_data_dir)
        self.markerDataEdit.setText(self.marker_data_dir)
        self.regDataEdit.setText(self.reg_dir)
        self.mpDataFileEdit.setText(self.mp_data_file)



        # search for files in scandir
        # files = glob.glob(os.path.join(self.scan_dir,"*.xml"))
        # self.scanDataInfoLabel.setText("Found %d files"%(len(files)))

        self.data = self.updateScanInfo()
        self.updateMP(self.data)
        self.reg = self.updateReg(self.data)

    def accept(self):
        self.settings.setValue("scan_dir",self.scanDataEdit.text())
        self.settings.setValue("mp_data_dir",self.mpDataEdit.text())
        self.settings.setValue("marker_data_dir", self.markerDataEdit.text())
        self.settings.setValue("reg_dir", self.regDataEdit.text())
        self.settings.setValue("mp_data_file", self.mpDataFileEdit.text())

        self.data = self.updateScanInfo()
        self.updateMP(self.data)
        self.reg = self.updateReg(self.data)

        print("Accept - exit")

        del self.settings
        QDialog.accept(self)





