"""

Diaplys a sinlge MP image

"""

from .ScanImage import ScanImage
import cv2

from PyQt5.QtGui import QPainter
from PyQt5.QtCore import pyqtSlot

class MPScanImage(ScanImage):


    # @pyqtslot(bool)
    def enableFixationOverlay(self,enabled):
        self.enable_mp_fixation_overlay=enabled
        self.update()

    # @pyqtSlot(bool)
    def enableGridOverlay(self, enabled):
        self.enable_mp_overlay = enabled
        self.update()

    def __init__(self,parent=None,scan=None):
        super().__init__(parent)
        self.initUI()

        self.enable_mp_fixation_overlay = False
        self.enable_mp_overlay = True



    def drawOverlays(self,image=None):

        if image is None:
            image = self.scan.getIRImage().copy()

        if self.enable_mp_overlay:
            image = self.scan.data.overlayGrid()

        if self.enable_mp_fixation_overlay:
            if image.shape[2] != 4:
                image = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)
            fixation = cv2.imread(self.scan.data.getFixationImage(),-1)

            image = cv2.addWeighted(image,1.0,fixation,0.5,1.0)

        image = cv2.cvtColor(image, cv2.COLOR_BGRA2BGR)

        return image

    def paintEvent(self, event):
        if self.scan is None:
            return

        if self.alt_localiser is not None:
            if self.enable_alt_localiser:
                if (self.alt_localiser_image is None) or (self.alt_localiser_fname != self.alt_localiser.transformed):
                    self.alt_localiser_fname = self.alt_localiser.transformed
                    self.alt_localiser_image = cv2.imread(self.alt_localiser_fname)
                    self.reg_cache = None
                localiserImage = self.alt_localiser_image.copy()

        else:
            localiserImage = self.scan.getIRImage().copy()

        localiserImage = self.drawOverlays(image=localiserImage)

        painter = QPainter(self)
        pixmap = self.cv2pixmap(localiserImage)
        painter.drawPixmap(self.rect(), pixmap)

