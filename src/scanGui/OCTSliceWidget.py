
from PyQt5.QtWidgets import QLabel,QWidget
from PyQt5.QtCore import Qt,pyqtSignal,QSize
from PyQt5.QtGui import QImage, QPixmap,QPainter,QPen,QColor


from .OCTSliceWidgetGui import Ui_OCTSliceWidget

class OCTSliceWidget(QWidget,Ui_OCTSliceWidget):

    def __init__(self,parent=None):
        QWidget.__init__(self,parent)
        Ui_OCTSliceWidget.__init__(self)
        self.setupUi(self)


