"""

Custom class / QLable for displaying the localiser image

"""

from PyQt5.QtWidgets import QLabel
from PyQt5.QtCore import Qt,pyqtSignal,QSize,QPoint
from PyQt5.QtGui import QImage, QPixmap,QPainter,QPen,QColor
import cv2

from .ScanImage import ScanImage
from .Markers import Marker
import numpy as np

class OCTLocaliserImage(ScanImage):
    """ OCT Specific at the moment"""

    # add custom signals
    addMarker = pyqtSignal(object)
    removeMarker = pyqtSignal(object)

    def on_add_marker(self,value):
        print("LocaliserImage::on_add_marker")
        self.addMarker.emit(value)

    def on_remove_marker(self,value):
        print("LocaliserImage::on_remove_marker")
        self.addMarker.emit(value)


    def __init__(self,parent=None,scan=None,markers=None):
        super().__init__(parent)

        self.initUI()

    def load(self, scan, markers, reg=None, alt_localiser=None):
        self.scan = scan
        self.setMinimumSize(496, 496)
        self.markers = markers
        self.reg = reg
        self.alt_localiser = alt_localiser
        self.markers.markersChanged.connect(self.update)
        self.scale = (np.array((self.width(), self.height()) / np.array(self.scan.getIRImage().shape[:2])))


    # def zoom(self,ammount):


    def overlayMPGrid(self,image=None):
        """ Overlay MP only if reg is mp"""
        if image is None:
            image = self.scan.getIRImage().copy()

        if not self.enable_mp_overlay:
            return image

        if self.reg is None:
            return image

        if self.reg.source.mode != "MP":
            return image

        # if has cache
        if self.reg_cache is not None:
            if self.reg == self.reg_cache:
                if self.mp_cache is not None:
                    return self.mp_cache.copy()


        if self.reg.source.mode != "MP":
            return image

        mp = self.reg.source
        mp.drawStimuli(image=image,reg=self.reg,drawHistory=False,radius=10)

        self.mp_cache = image.copy()
        self.reg_cache = self.reg
        return image

    def mousePressEvent(self, event):

        x = int(round(event.pos().x() / self.scale[0]))
        if self.scan is None:
            return

        # check x within slice limit
        slice_0 = self.scan.getSlice(self.parent().sliceNumber)
        x1 = int(round((slice_0.start_x / self.scan.scale_x)))
        x2 = int(round((slice_0.end_x / self.scan.scale_x)))

        if x < x1 or x > x2:
            return

        if event.button() == Qt.RightButton:
            if self.highlight != Marker(-1, -1, -1):
                self.markers.remove(self.highlight)
        else:
            self.markers.add(self.parent().sliceNumber, x)
        self.update()

    def paintEvent(self, event):
        if self.scan is None:
            return


        if self.alt_localiser is not None:
            if self.enable_alt_localiser:
                if (self.alt_localiser_image is None) or (self.alt_localiser_fname != self.alt_localiser.transformed):
                    self.alt_localiser_fname = self.alt_localiser.transformed
                    self.alt_localiser_image = cv2.imread(self.alt_localiser_fname)
                    self.reg_cache = None
                localiserImage = self.alt_localiser_image.copy()

        else:
            localiserImage = self.scan.getIRImage().copy()

        localiserImage = self.overlayMPGrid(image=localiserImage)

        painter = QPainter(self)
        pixmap = self.cv2pixmap(localiserImage)
        painter.drawPixmap(self.rect(), pixmap)


        if self.scan.mode == "OCT":
            # draw slice line.
            slice_0 = self.scan.getSlice(self.parent().sliceNumber)
            x1 = int(round((slice_0.start_x / self.scan.scale_x) * self.scale[0]))
            x2 = int(round((slice_0.end_x / self.scan.scale_x) * self.scale[0]))
            y1 = int(round((slice_0.start_y / self.scan.scale_y) * self.scale[1]))
            y2 = int(round((slice_0.end_y / self.scan.scale_y) * self.scale[1]))
            painter.setPen(QPen(QColor(0,255,0,128), 2))
            painter.drawLine(x1, y1, x2, y2)

        # draw markers
        if self.enableMarkers and (self.markers is not None):
            # pen = QPen(Qt.green,2)
            painter.setBrush(Qt.green)
            painter.setPen(QPen(Qt.NoPen))
            for m in self.markers:
                if m == self.highlight:
                    painter.setBrush(QColor(251, 155, 152, 128))
                else:
                    painter.setBrush(QColor(155, 251, 152, 128))

                x = int(round(m.pos_x * self.scale[0]))
                y = int(round((self.scan.getSlice(m.slice).start_y / self.scan.scale_y) * self.scale[1]))
                # print("drawing %d, %d" % (x, y))
                painter.drawEllipse(QPoint(x,y), 5, 5)

        if self.enable_etdrs_overlay:
            self.overlayETDRS(painter)

    def overlayETDRS(self,painter,centre_point=None):
        """
               Draw the ETDRS grid onto the localiser image.
               default select the centre pixel, can be user selected to localise over the fovea.

               grid has 3 circles, 1,3 and 6 mm diameter.

               :return:
        """
        if centre_point is None:
            centre_point = QPoint((self.scan.localiser_width/2) * self.scale[0], (self.scan.localiser_height/2)* self.scale[1])



        # is this pixels to mm?
        radius_1 = (0.5/self.scan.scale_x) * self.scale[0]
        radius_3 = (1.5/self.scan.scale_x) * self.scale[0]
        radius_6 = (3.0/self.scan.scale_x) * self.scale[0]
        painter.setPen(QPen(QColor(0, 255, 0, 128), 2))
        painter.setBrush(Qt.NoBrush)
        painter.drawEllipse(centre_point,radius_1,radius_1)
        painter.drawEllipse(centre_point,radius_3,radius_3)
        painter.drawEllipse(centre_point,radius_6,radius_6)

        outer_ring = QPoint(radius_6,radius_6)
        painter.drawLine(centre_point-outer_ring,centre_point+outer_ring)
        painter.setPen(Qt.NoPen)

