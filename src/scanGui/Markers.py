"""

Marker class for handling labeling the OCT /  AF data

"""
from PyQt5.QtCore import QObject, pyqtSignal, QSettings
import os

class Marker(object):
    def __init__(self,pos_x,slice,type):
        self.pos_x = pos_x
        self.slice = slice
        self.type = type

    def __eq__(self, other):
        return (self.pos_x == other.pos_x and self.slice == other.slice and self.type == other.type)

class Markers(QObject):

    markersChanged = pyqtSignal()

    def __init__(self,scan):
        super().__init__()
        """ Initialise marker agains a scan. """
        self.slice = None
        self.scan = scan
        self.settings = QSettings("oculab", "OCT Labeler")
        self.data_dir = self.settings.value("marker_data_dir")

        if (self.data_dir is None) or (self.data_dir==""):
            self.data_dir = "/Users/iain/Dropbox/docs/ORG/Msc/project/experiments/vision classification/scan_registration/data/markers"



        self.markers = []

    def __iter__(self):
        return iter(self.markers)


    def add(self,slice,x,type=0):
        marker = Marker(pos_x=x,slice=slice,type=type)
        if marker in self.markers:
            return False
        self.markers.append(marker)
        self.markersChanged.emit()
        return True

    def remove(self,marker):
        # marker = Marker(pos_x=x,slice=slice,type=type)
        if marker not in self.markers:
            return False
        self.markers.remove(marker)
        self.markersChanged.emit()
        return True


    def get(self,slice):
        """ return all markers for a given slice """
        if slice not in self.markers.keys():
            return None
        return self.markers[slice]

    def filename(self):
        return os.path.join(self.data_dir,"markers_%s.csv" % (self.scan.getID()))

    def save(self):
        """save all the items to file"""

        # don't save if there are no markers.
        if len(self.markers)==0:
            return

        fname = self.filename()
        f = open(fname,'w')
        f.write("slice,pos_x,type\n")
        for m in self.markers:
            f.write("%d,%d,%s\n"%(m.slice,m.pos_x,m.type))
        f.close()

        print("Saving Markers to %s"%fname)


    def read(self,fname=None):
        if fname is None:
            fname = self.filename()

        # could just use pandas to read in csv
        import pandas as pd
        df = pd.read_csv(fname)
        print(df)
        for i,row in df.iterrows():
            slice = row['slice']
            x = row['pos_x']
            type = row['type']
            self.add(slice,x,type)
