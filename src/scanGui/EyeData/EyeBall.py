from . import Scans

class EyeBall:

    """
    A class that contains all data with regards to an eye
    Loads in OCT, FAF, Colour , Microperimetry
    registers all

    """

    class Registration:
        """
        inner class that containst the registration details
        """
        def __init__(self,type,name=None):
            self.name = name
            self.type = type


    def __init__(self,eye=None,pid=None,visit=None,person=None):
        self.has_oct = False
        self.oct = {}

        self.scans=[]

        """ subject details """
        self.eye = eye
        self.pid = pid
        self.visit = visit

        #reference to parent
        self.person = person

        """ set the dfault that all images will be registered to """
        self.base_for_registration = "OCT"

    def __repr__(self):
        txt =""
        for scan in self.scans:
            txt+= "%s\n"%scan.__repr__()
        return txt

    def addScan(self,scan):
        """
        Load scan, assign to correct modality
        :return:
        """
        # if scan.mode == "OCT":
        #     self.oct[scan.visit] = scan
        # self.has_oct=True

        scan.eye = self.eye
        scan.pid = self.pid
        scan.person = self.person

        # print "addScan %s"%scan.eye

        if scan in self.scans:
            # print("Already got the scan: %s"%scan.getID())
            return

        self.scans.append(scan)

    def getScan(self,mode,visit,date,fov=None):
        """
        get an exsiting scan, or create a new one.
        :param mode:
        :param visit:
        :return:
        """

        # if mode == "OCT":
        #     if visit in self.oct.keys():
        #         return self.oct[visit]
        #     return OCT()
        # return None

        for scan in self.scans:
            if scan.mode == mode and\
                            scan.visit == visit and\
                            scan.date == date:

                if fov is not None:
                    if scan.fov == fov:
                        return scan
                else:
                    return scan

        # print "Making new Scan"

        scan = self.createScan(mode)
        scan.visit = visit
        scan.eye = self.eye
        scan.pid = self.pid
        scan.person = self.person
        scan.date = date
        scan.fov = fov
        return scan

    def mode(self,mode):
        out = []
        for scan in self.scans:
            if scan.mode == mode:
                out.append(scan)
        return out


    @staticmethod
    def createScan(mode):
        if mode == "OCT":
            return Scans.OCT()
        if mode == "SWAF":
            return Scans.SWAF()
        if mode == "MP":
            return Scans.MP()
        if mode == "COLOUR":
            return Scans.Colour()