import os,cv2,shutil,subprocess
import numpy as np
import platform,warnings

class Registration:
    """
    inner class that containst the registration details
    """

    def __init__(self, type, name=None, target=None, inverse=None, source=None):
        self.name = name
        self.type = type
        self.source = source
        self.target = target
        self.inverse = inverse
        self.t_points = None # store transformed MP points

    def __eq__(self, other):
        """
        equality overload,
        returns true if source and target match
        :param other:
        :return:
        """
        if isinstance(other, Registration):
            return (other.target == self.target) and (other.source == self.source)
        return False

    def __repr__(self):
        return "Source [%s] Target [%s]" % (self.source, self.target)

    def matches(self, source, target):
        """
        Returns true if this registration is for the scan pair.

        :param source:
        :param target:
        :return:
        """
        return (target == self.target) and (source == self.source)


class ScanRegistrator:
    """
    Class that handles all of the registration, using gdb-icp
    """

    def __init__(self,reg_dir=None, gdbicp_dir=None):
        """
        Setup registration output folder location
        also set the location of the gdbicp binary (linux only)
        :param reg_dir:
        :param gdbicp_dir:
        """
        home = os.path.expanduser('~')
        if reg_dir is None:
            data_folder = os.path.join(home, 'testexperiments/vision classification/scan_registration/data/registration/')
            self.reg_dir = data_folder
        else:
           self.reg_dir = reg_dir

        if not os.path.exists(self.reg_dir):
            print("[ScanRegistrator::init] path does not exist: %s"%self.reg_dir)

            # raise Exception("[ScanRegistrator::init] path does not exist: %s"%self.reg_dir)

        if gdbicp_dir is None:
            wrd = "experiments/vision classification/scan_registration/gdb-icp/"
            wrd = os.path.join(home, wrd)
            self.gdbicp_dir = wrd
        else:
            self.gdbicp_dir = gdbicp_dir


        # act as a store for all the registrations
        self.registrations = []

    def _register(self, src, dst, model=2, dryrun=False, complete=False, rotate=True,quiet=True):
        """
        Use GDB-ICP to calculate affine transformation

        -model 0 = affine
        -model 1 = homography
        -model 2 = Quadratic
        -model 3 = Homography with Radial Lens Distortion
        -model 4 = Similarity

        output should be in the file:
        mosaic_stillPicture_to_stillPicture.xform


        idea: copy files to tmp folder, rename as to easier identify

        src and dst are both Scan objects

        """
        # prev = self.checkPreviousRegistration(src,dst)
        # if prev is not None:
        #     return self.loadRegistrationOutput(prev)

        data_folder = self.reg_dir
        tmp = "/tmp/"

        src_fname = os.path.join(tmp,"%s.tif"%src.getID())
        dst_fname = os.path.join(tmp,"%s.tif"%dst.getID())

        cv2.imwrite(src_fname, src.getIRImage())
        cv2.imwrite(dst_fname, dst.getIRImage())

        extra = ""
        if complete:
            extra = "-complete"
        _quiet=""
        if quiet:
            _quiet = "2>&1 > /dev/null"
        gdbicp_cmd = "./gdbicp %s %s -model %d %s -xform_as_to %s" % (src_fname, dst_fname, model, extra, _quiet)

        if dryrun:
            return gdbicp_cmd
        else:
            print("--- runnning registartion ---")
            print(gdbicp_cmd)
            subprocess.call(gdbicp_cmd, cwd=self.gdbicp_dir, shell=True)


        #make dir for all files
        dest_dir = os.path.join(data_folder,"%s_%s" % (src.getID(), dst.getID()))
        if not os.path.exists(dest_dir):
            os.makedirs(dest_dir)

        # move xform
        out = "mosaic_%s_to_%s.xform" % (src.getID(), dst.getID())
        if not os.path.exists(os.path.join(self.gdbicp_dir,out)):
            if complete:
                print("Registration failed")
                open(os.path.join(dest_dir, "gdbicp_failed"), 'a').close()

                # copy the two offending images
                cv2.imwrite(os.path.join(dest_dir,"%s.tif"%src.getID()), src.getIRImage())
                cv2.imwrite(os.path.join(dest_dir,"%s.tif"%dst.getID()), dst.getIRImage())

                return None

            print("Registration Failed, trying again.")
            return self._register(src,dst,model=model,complete=True)

        shutil.move(os.path.join(self.gdbicp_dir,out),os.path.join(dest_dir,out))
        #move the 3 png outputs

        out1 = "xformed_%s_to_%s.png" % (src.getID(), dst.getID())
        shutil.move(os.path.join(self.gdbicp_dir,out1),os.path.join(dest_dir,out1))
        out2 = "xformed_%s_to_%s.png" % (dst.getID(), dst.getID())
        shutil.move(os.path.join(self.gdbicp_dir,out2),os.path.join(dest_dir,out2))
        out3 = "mosaic_%s_to_%s.png" % (src.getID(), dst.getID())
        shutil.move(os.path.join(self.gdbicp_dir,out3),os.path.join(dest_dir,out3))

        out4 = "xto_%s_to_%s.png" % (src.getID(), dst.getID())
        shutil.move(os.path.join(self.gdbicp_dir, out4), os.path.join(dest_dir, out4))

        reg = self.loadRegistrationOutput(os.path.join(dest_dir,out))
        reg.src = out1
        reg.dst = out2
        reg.mosaic = out3
        reg.target = dst
        return reg

    def register(self,source,target,complete=False,dryrun=False):
        """
        Wrapper for the registration function

        :param source:
        :param target:
        :return:
        """

        if source.eye != target.eye:
            warnings.warn("Trying to register Left against Right. - This will always fail! ")
            return None

        # print("Trying to register %s to %s"%(source.getID(),target.getID()))

        reg = None
        prev = self.checkPreviousRegistration(source, target)
        if prev is not None:
            # if prev == "Failed":
            #     return None
            # print("Running reg")
            reg = self.loadRegistrationOutput(prev)

        else:
            # check os, if OSX force Dry Run.
            if platform.system() == 'Darwin':
                warnings.warn("Cannot run GDB-ICP registration on OS X - forcing dry run")
                dryrun = True

            if not dryrun:
                reg = self._register(source, target, complete=complete)

        if reg is None:
            return None

        reg.source = source
        reg.target = target

        #get person and add the registration
        if source.person is not None:
            source.person.addRegistration(reg)

        # store localy
        self.registrations.append(reg)

        return reg

    def checkPreviousRegistration(self, src, dst):
        """
        Have we run the registration before?
        """

        src_fname = src.getID()
        dst_fname = dst.getID()
        dest_dir = os.path.join(self.reg_dir, "%s_%s" % (src.getID(), dst.getID()))
        filename = "mosaic_%s_to_%s.xform" % (src_fname, dst_fname)
        filename = os.path.join(dest_dir,filename)
        if os.path.exists(filename):
            # print("Found existing registration")
            return filename
        # check for faliures
        filename = os.path.join(dest_dir, "gdbicp_failed")
        if os.path.exists(filename):
            # print "Failed Registration"
            return "failed"

        return None

    def getRegistration(self,source,target):
        """
        find a reg that matched
        :param src:
        :param dst:
        :return:
        """
        for reg in self.registrations:
            if reg.matches(source,target):
                return reg
        return None

    def loadRegistrationOutput(self, filename):
        """
        Read in gdbicp output, depends on mode

        i think numpy is row major
        """

        if not os.path.exists(filename):
            # print(" --- Registration Failed --- ")
            # try again
            return None

        data = [line.rstrip() for line in open(filename)]

        reg = Registration("QUADRATIC")
        reg_inv = Registration("QUADRATIC")
        if data[5] == "QUADRATIC":

            reg.B = np.zeros(shape=(2, 3))
            row = 0
            for r in data[7:9]:
                col = 0
                for c in r.split(' '):
                    reg.B[row][col] = float(c)
                    col += 1
                row += 1

            row = 0
            reg.A = np.zeros(shape=(2, 2))
            for r in data[9:11]:
                col = 0
                for c in r.split(' '):
                    reg.A[row][col] = float(c)
                    col += 1
                row += 1

            reg.T = np.zeros(shape=(2, 1))
            reg.C = np.zeros(shape=(2, 1))
            t = data[11].split(' ')
            reg.T[0][0] = float(t[0])
            reg.T[1][0] = float(t[1])
            reg.C[0][0] = float(t[2])
            reg.C[1][0] = float(t[3])

            reg.scale = np.array(data[29].split(' ')).astype(np.float)

            #now the inverse
            reg_inv.B = np.zeros(shape=(2, 3))
            row = 0
            for r in data[35:37]:
                col = 0
                for c in r.split(' '):
                    reg_inv.B[row][col] = float(c)
                    col += 1
                row += 1

            row = 0
            reg_inv.A = np.zeros(shape=(2, 2))
            for r in data[37:39]:
                col = 0
                for c in r.split(' '):
                    reg_inv.A[row][col] = float(c)
                    col += 1
                row += 1

            reg_inv.T = np.zeros(shape=(2, 1))
            reg_inv.C = np.zeros(shape=(2, 1))
            t = data[39].split(' ')
            reg_inv.T[0][0] = float(t[0])
            reg_inv.T[1][0] = float(t[1])
            reg_inv.C[0][0] = float(t[2])
            reg_inv.C[1][0] = float(t[3])

            reg_inv.scale = np.array(data[57].split(' ')).astype(np.float)
            reg.inverse = reg_inv

        fnames = os.path.basename(filename)[:-6].split('_to_')

        src = fnames[0][7:]
        dst = fnames[1]
        out1 = "xformed_%s_to_%s.png" % (src, dst)
        out2 = "xformed_%s_to_%s.png" % (dst, dst)
        out3 = "mosaic_%s_to_%s.png" % (src, dst)
        out4 = "xto_%s_to_%s.png" % (src, dst)

        dir = os.path.dirname(filename)
        reg.src = os.path.join(dir,out1)
        reg.dst = os.path.join(dir,out2)
        reg.mosaic = os.path.join(dir,out3)
        reg.transformed = os.path.join(dir,out4)

        return reg




    def overlayMPGrid(self,mp_scan,target_scan, image=None, *args):
        """

        overlay an MP Grid onto a given scan, auto search for the registrations.

        :param mp_scan:
        :param target_scan:
        :return:
        """

        reg = self.getRegistration(source=mp_scan,target=target_scan)
        if reg is None:
            return None

        radius = int(target_scan.degToPixel() * 0.215)

        if image is None:
            image = target_scan.getIRImage().copy()

        return mp_scan.drawStimuli(image=image,reg=reg, radius=radius, *args)


    def overlayMPSlice(self,mp_scan,oct_scan,slice,alpha=0.3):
        """

        overlay an MP Gring onto oct slice, auto search for the registrations..

        :param mp_scan:
        :param target_scan:
        :param image:
        :param args:
        :return:
        """
        reg = self.getRegistration(source=mp_scan, target=oct_scan)
        if reg is None:
            return None

        return oct_scan.overlayMPData(mp_scan, slice=slice, reg=reg,alpha=alpha)