#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  1 17:09:49 2017

@author: iain

Microperimetry data manager

Loads from raw file and creates an eye object


TODO: pull all data from Projection.xml


"""
from datetime import datetime
import pandas as pd
from . import distanceFunctions as df
import glob,os,shutil, subprocess
import fnmatch
import math
import numpy as np
import cv2
from . import helperFunctions as hf
import colorsys

class MPData(object):
    """
        Class that holds all the MP data for a single Eye
    """

    methods = ['averge','ssd','hamming']


    threshold_lookup = {
        36:(63,142,39),
        35:(68,152,43),
        34:(73,161,46),
        33:(78,171,49),
        32:(83,181,53),
        31:(88,191,56),
        30:(98,201,60),
        29:(113,211,63),
        28:(131,221,67),
        27:(153,231,71),
        26:(176,241,76),
        25:(200,251,80),
        24:(223,252,82),
        23:(239,253,83),
        22:(255,253,84),
        21:(247,205,70),
        20:(245,185,64),
        19:(242,165,59),
        18:(240,146,54),
        17:(238,127,49),
        16:(238,118,47),
        15:(237,109,45),
        14:(237,101,43),
        13:(236,93,42),
        12:(236,85,40),
        11:(235,78,39),
        10:(235,71,38),
        9:(235,65,37),
        8:(235,60,36),
        7:(234,56,36),
        6:(234,53,36),
        5:(234,51,35),
        4:(207,43,44),
        3:(179,37,63),
        2:(151,31,89),
        1:(123,26,116),
        0:(96,24,144),
        -1:(1,1,1)
    }



    
    def __init__(self,threshold_filename=None, fixation_filename=None,preload=True,datadir=None):
        """
            Loads threshold data and fixation data.
        """
        if datadir is None:
            home = os.path.expanduser('~')
            self.datadir = os.path.join(home, "experiments/microperimetry/data/")
        else:
            self.datadir = datadir

        self.threshold_filename = threshold_filename
        self.fixation_filename =  fixation_filename
        self.loaded = False
        if not preload:
            
            #pull pid and exam id from filenames
            fname = os.path.basename(self.threshold_filename).split('_')
            self.patient_id = int(fname[1])
            self.exam_id = int(fname[2])            
            return
        
        if threshold_filename is not None:
            self.loadThreshold(threshold_filename)
            if fixation_filename is not None:
                self.loadFixation(fixation_filename)
            else:
                #try and load anyway
                fixation_filename = self.threshold_filename[:-13]+"fixation.txt"
                self.loadFixation(fixation_filename)
            self.loaded=True    

    def init(self,pid,eid):
        self.patient_id = pid
        self.exam_id = eid

    def load(self):
        if self.threshold_filename is not None:
            self.loadThreshold(self.threshold_filename)
        if self.fixation_filename is not None:
            self.loadFixation(self.fixation_filename)
        else:
            #try and load anyway
            self.fixation_filename = self.threshold_filename[:-13]+"fixation.txt"
            self.loadFixation(self.fixation_filename)
        self.loaded=True

    def findFixation(self,search_dir=None):
        """using pid and eid, try and find the fixation file."""

        if search_dir is None:
            home = os.path.expanduser('~')
            search_dir = os.path.join(home, "experiments/microperimetry/data_from_jasleen/")

        fname = "maia-4003_%d_%d_fixation.txt"%(self.patient_id,self.exam_id)
        fname=os.path.join(search_dir,fname)
        # print("Searcing for: %s"%fname)
        files = glob.glob(fname)
        if len(files)>0:
            return self.loadFixation(files[0])
        return None

    def loadFixation(self, filename):
        """ Do nothin for the moment """
        if not os.path.exists(filename):
            return False

        print("loading Fixation: %s"% filename)

        
        self.fixation_filename = filename
        self.fixation_data  = [line.rstrip() for line in open(filename)]
        data = self.fixation_data
        self.duration = int(data[15])
        self.pix2deg_ratio = float(data[17])
        
        self.PRL_i = [float(data[26]),float(data[27])]
        self.PRL_f = [float(data[29]),float(data[30])]
        
        self.BCEA_63 = float(data[42][:-1])
        self.BCEA_95 = float(data[44][:-1])
        self.BCEA_angle = float(data[46])
        
        #find fixation point data start nad therdore reg points end
        i = data.index('Fixation points:')
        
        r_points = [
                np.array(line.split('\t')).astype(float) for line in data[51:i-1]
                ]
        
        r_p=[]
        for p in r_points:
            r_p.append({
                    "x_deg":p[0],
                    "y_deg":p[1],
                    "ttime":p[2]
                    })
        self.registration_points = pd.DataFrame.from_records(r_p)
        
        f_points = [
                np.array(line.split('\t')) for line in data[i+3:]
                ]
        f_p=[]
        for p in f_points:
            if(len(p)==3):
                p=np.append(p,'#-1')
            f_p.append({
                    "x_deg":float(p[0]),
                    "y_deg":float(p[1]),
                    "ttime":float(p[2]),
                    "stimID":float(p[3][1:])    #TODO finish loading od this annoying file!!!
                    })
        self.fixation_points = pd.DataFrame.from_records(f_p)
        
        return True
    
    def loadThreshold(self, filename):
        """
            load all the data.... MAIA s/n 0034
        """
        # print "loading Threshold: %s"% filename
        self.baseline_exam_id = '-1'
        self.rawdata  = [line.rstrip() for line in open(filename)]
        data = self.rawdata
        self.patient_id = int(data[3])
        self.exam_id    = int(data[5])    
        self.eye        = data[9]
        self.age        = int(data[11])
        self.exam_date  = datetime.strptime(data[13],'%Y-%m-%d %H:%M')
        self.date  = self.exam_date.strftime("%d/%m/%Y")
        self.reliability= int(data[1][:-1])
        self.avg = float(data[24])
        
        self.data = []
        for d in data[49:-1]:
            _d = d.split('\t')

            self.data.append({'ID': _d[0],
                              'Threshold':float(_d[3]),
                              'x_deg':float(_d[1]),
                              'y_deg':float(_d[2]),
                              'exam_id':self.exam_id,
                              'patient_id':self.patient_id
                              } )
        self.distanceToScotoma()
        self.dataframe = pd.DataFrame.from_records(self.data).sort_values(by="ID")
        self.length = len(self.data)
        #print self.avg, self.exam_id, len(self.dataframe)
        if self.loadXML():
            self.degToPixels()

    def loadThresholdXML(self,datadir=None):
        """
        Load the fixation data from the xml file,
        :param filename:
        :return:
        """        
        if datadir is None:
            datadir = self.datadir
            # home = os.path.expanduser('~')
            # datadir = os.path.join(home, "experiments/microperimetry/data/")

        dir = os.path.join(datadir,"%d/%d"%(self.patient_id,self.exam_id))
        xml = os.path.join(dir,'projection.xml')

        import xml.etree.ElementTree as ET
        self.tree = ET.parse(xml)
        root = self.tree.getroot()
        data = []

        for s in root.find("Stimuli").findall("Stimulus"):
            d = s.attrib
            d["history"] = s.find('history').text
            data.append(d)

        return pd.DataFrame.from_records(data)


    def loadXML(self,datadir=None):
        """
        Load all the xml data I want
        :return:
        """
        # print ("Loading XML")

        if datadir is None:
            datadir = self.datadir

        dir = os.path.join(datadir, "%d/%d" % (self.patient_id, self.exam_id))
        xml = os.path.join(dir, 'projection.xml')

        if not os.path.exists(xml):
            print("Not found: %s"%(xml))
            return False

        self.datadir = dir



        import xml.etree.ElementTree as ET
        self.tree = ET.parse(xml)
        self.root = self.tree.getroot()
        header = self.root.find("Header")
        self.baseline_exam_id = header.find('ExamBaselineID').text
        
        self.patient_id = int(header.find("PatientID").text)
        self.exam_id = int(header.find("ExamID").text)
        self.eye = header.find("Eye").text
        self.reliability = -1
        
        self.exam_date  = datetime.strptime(header.find("DateTime").text,'%Y-%m-%d %H:%M')
        self.date  = self.exam_date.strftime("%d/%m/%Y")

        avg = header.find("AnalysisResults").find("averageSensitivity")
        if avg is not None:
            avg = float(avg.text)
        self.avg = avg


        
        self.BCEA_63 = 1
        self.BCEA_95 = 1
        self.BCEA_angle =  1

        bceas = header.find("AnalysisResults").findall("BCEA")
        for bcea in bceas:
            if bcea.attrib['probability'] == '0.632':
                self.BCEA_63 = float(bcea.attrib['area_pix2'])
                self.BCEA_63_a_pix = float(bcea.attrib['a_pix'])
                self.BCEA_63_b_pix = float(bcea.attrib['b_pix'])
                self.BCEA_63_angle_rad = float(bcea.attrib['angle_rad'])
            else:
                self.BCEA_95 = float(bcea.attrib['area_pix2'])
                self.BCEA_95_a_pix = float(bcea.attrib['a_pix'])
                self.BCEA_95_b_pix = float(bcea.attrib['b_pix'])
                self.BCEA_95_angle_rad = float(bcea.attrib['angle_rad'])

        self.fov = float(header.find("fieldDegreesX").text)
        
        self.age = int(header.find("Age").text)
        self.reg_deg = float(header.find("baselineRotation_deg").text)
        self.reg_X = int(header.find("baselineShiftX_pix").text)
        self.reg_Y = int(header.find("baselineShiftY_pix").text)
        self.baselinePRLX = int(float(header.find("baselinePRLX_pix").text))
        self.baselinePRLY = int(float(header.find("baselinePRLY_pix").text))
        self.examPRLX = int(float(header.find("examPRLX_pix").text))
        self.examPRLY = int(float(header.find("examPRLY_pix").text))
        self.examDuration = int(header.find("examDuration_s").text)

        self.xml_df = self.loadThresholdXML(datadir=datadir)

        #merge
        self.xml_df['id'] = self.xml_df['id'].astype(int)

        self.data = []
        for index,row in self.xml_df.iterrows():
            t = int(row['final_intensity'])
            (x,y) = hf.pol2cart(float(row['ray']), np.radians(float(row['angle_deg'])))
            self.data.append({'ID': int(row['id']),
                              'Threshold':-t, 
                              'x_deg':float(x),
                              'y_deg':-float(y),
                              'exam_id':self.exam_id,
                              'patient_id':self.patient_id
                              } )
        
    
        self.dataframe = pd.DataFrame.from_records(self.data)
        self.dataframe = self.dataframe.set_index('ID').join(self.xml_df.set_index('id'))
        self.dataframe.reset_index(level=0, inplace=True)
        self.dataframe.sort_values(by="ID",inplace=True)
        #self.dataframe = self.dataframe.sort_values(by="ID")
        self.length = len(self.data)

        self.calcEyeMovements()
        self.loaded = True
        return True




    def calcEyeMovements(self):
        """
        Fixed verison
        :return:
        """
        if not hasattr(self,'fixation_points'):
            return False

        df = self.fixation_points
        ids = self.fixation_points['stimID'].unique()

        # loop through the whole file
        events = []
        last_id = None
        counter=0
        counters={}
        for i,row in df.iterrows():
            if row['stimID'] == -1:
                if last_id is not None:
                    events.append({
                        "stimID": last_id,
                        "index": i,
                        "counter": counters[last_id],
                        "position": np.array([df.iloc[i]['x_deg'], df.iloc[i]['y_deg']]),
                        "x_deg": df.iloc[i]['x_deg'],
                        "y_deg": df.iloc[i]['y_deg']
                    })
                    counter += 1
                last_id = None
                continue

            if last_id is None:
                #then first event
                last_id = int(row['stimID'])

                #have we seen this id before?
                if not last_id in counters.keys():
                    counters[last_id]=0
                else:
                    counters[last_id]+=1
                events.append({
                    "stimID":last_id,
                    "index":i-1,
                    "counter": counters[last_id],
                    "position":np.array([df.iloc[i-1]['x_deg'],df.iloc[i-1]['y_deg']]),
                    "x_deg": df.iloc[i-1]['x_deg'],
                    "y_deg": df.iloc[i-1]['y_deg']
                })

            # not first event
            events.append({
                "stimID": last_id,
                "index": i,
                "counter": counters[last_id],
                "position": np.array([df.iloc[i]['x_deg'], df.iloc[i]['y_deg']]),
                "x_deg":df.iloc[i]['x_deg'],
                "y_deg":df.iloc[i]['y_deg']
            })
        df_events = pd.DataFrame.from_records(events)


        #loop through events and create a list to mirror the history.
        output={}
        for id in ids:
            output[id]=[]
            stim = df_events[df_events.stimID==id]
            for i in range(len(stim['counter'].unique())):
                sum=[]
                distance = 0
                for i,row in stim[stim.counter==i].iterrows():
                    sum.append(np.array(row['position']))
                for i in range(len(sum)-1):
                    distance += np.linalg.norm(sum[i] - sum[i - 1])
                output[id].append(distance)


        # o_df = pd.DataFrame.from_records(output)

        #convert to a nice dataframe so it can be merged with the rest
        datas=[]
        for k,v in output.iteritems():
            datas.append({
                "ID":int(k),
                "movement":v,
                "mean_movement":np.mean(v)
            })

        m_df = pd.DataFrame.from_records(datas)
        if not "movement" in self.dataframe.columns:
            self.dataframe = self.dataframe.set_index("ID").join(m_df.set_index("ID"))
            self.dataframe.reset_index(inplace=True)
            self.dataframe.sort_values(by="ID", inplace=True)

        return m_df,output,df_events


    def __repr__(self):
        """ default display of an eye object """
        return "PID:%d EID:%d Rep:%d %s %d %s" %(
            self.patient_id,
            self.exam_id, 
            self.getReliability(),
            self.eye,
            self.length,
            self.date)

    
    def addManualPoints(self,points):
        """
        add three points for manual registration. Affine.
        
        """
    
        self.manual_points = np.array(points)
    
    def getReliability(self):
        history = self.dataframe[ self.dataframe['ID']==0]['history'].tolist()[0].split(',')[:-1]
        return 100*(float(history.count('-1'))/float(len(history)))

    def thresholds(self,clean=True):
        """ return all the measured points values, ignoring point 0"""
        
        return self.dataframe[self.dataframe["ID"]!=0].sort_values(by="ID")["Threshold"].tolist()
        
        if not clean:
            return self.dataframe['Threshold'].tolist()[1:]

        #replave all -1 value with 0
        _t = self.dataframe['Threshold'].tolist()[1:]
        for i in range(len(_t)):
            if _t[i] < 0:
                _t[i] = 0
        return _t

    def meanThreshold(self):
        return self.dataframe[self.dataframe["ID"] != 0].mean()
        # return self.dataframe['Threshold'].mean()
        
    def distanceToEye(self, eye, method="ssd"):
        """
        Return distance to another eye
        """
         
        """ Check eyes are the same (L/R)"""
        if self.eye != eye.eye:
            raise Exception("Eyes are not the same side!!!")
    
        if method == "ssd":
            return df.signedDiff(self.points(),eye.points())
        elif method == "average":
            return df.averageDiff(self.points(),eye.points())
        elif method == "hamming":
            return df.signedHammingDistance(self.points(),eye.points())

        
    def __sub__(self, rhs):
        """
            Overloading the subtact operator; subtract all of the point thresholds
        """
        return np.subtract(self.points(),rhs.points())
        
    def pointsAboveThreshold(self,threshold=0):
        """
            Return the number of points  at or above the thresold 
            Usefull for knowing how bad the vision is.
        """
        count =0
        for p in self.points():
            if p >= threshold:
                count+=1
        return count

    def plot(self,ax=None,onh=True):
        """
            Plot the eye: 
        """
                
        if not onh:
            df=self.dataframe[1:]
        else:  
            df = self.dataframe
            
        if ax is None:
            g = sns.lmplot('x_deg',
                       'y_deg',
                       data=df,
                       fit_reg=False,
                       hue='Threshold',
                       palette="Blues",
                       markers='o',
                       scatter_kws={'linewidths':1,'edgecolor':'k'})
            g.fig.suptitle("P:%s-%s %s %s"%(self.patient_id,self.eye,self.exam_date.date(),self.exam_id))
            return
        fig = ax.scatter(self.dataframe['x_deg'],
                   self.dataframe['x_deg'],
                   c=self.dataframe['Threshold'],
                   cm='BrBG')
        ax.set_title("%s %s %s"%(self.patient_id,self.eye,self.exam_id))
        return fig

    def getPoint(self,x,y):
        """
        return the tested point at the specific eccentricity
        :param x:
        :param y:
        :return:
        """

        value = self.dataframe.loc[ (self.dataframe['x_deg'] == x) & (self.dataframe['y_deg'] == y),'Threshold']
        if len(value) == 0:
            return None
        return value.tolist()[0]

    def plotMeridian(self,meridian):
        """
        Assuming a 10-2 grid, average points to generate a meridian.


        :param meridian:
        :return:
        """
        out = {}
        if meridian == 'temporal':
            for x in range(1,10,1):
                v1 = self.getPoint(x,1)
                v2 = self.getPoint(x,-1)
                if(v1 is not None) or (v2 is not None):
                    out[abs(x)] = (v1+v2)/2
        elif meridian == 'nasal':
            for x in range(-1,-10,-1):
                v1 = self.getPoint(x,1)
                v2 = self.getPoint(x,-1)
                if(v1 is not None) or (v2 is not None):
                    out[abs(x)] = (v1+v2)/2
        elif meridian == 'superior':
            for y in range(1,10,1):
                v1 = self.getPoint(1,y)
                v2 = self.getPoint(-1,y)
                if (v1 is not None) or (v2 is not None):
                    out[abs(y)] = (v1 + v2) / 2
        elif meridian == 'inferior':
            for y in range(-1, -10, -1):
                v1 = self.getPoint(1, y)
                v2 = self.getPoint(-1, y)
                if (v1 is not None) or (v2 is not None):
                    out[abs(y)] = (v1 + v2) / 2

        return out

    
    def distanceToScotoma(self):
        """
            step through the data, calculate each points minimum distance to a -1 point
            ignore ONH
        """
        data =self.data
        distances=[]
        for i in range(len(data)):
            l_dist=[]
            for j in range(1,len(data)):
                if j == i:
                    continue

                if data[i]['Threshold'] >-1:
                    if data[j]["Threshold"]>-1:
                        continue
                else:
                    if data[j]["Threshold"]<0:
                        continue
                x = data[i]['x_deg'] - data[j]['x_deg']
                y = data[i]['y_deg'] - data[j]['y_deg']
                l_dist.append(np.sqrt(x**2+y**2))
    
            if len(l_dist)==0:
                l_dist.append(-1)
            self.data[i]['dist2scotoma']=np.min(l_dist)
            distances.append(np.min(l_dist))
        return distances

    def compareRegistration(self,eye):
        """
        Using the raw fundus image, and gdb-icp (linux only)
        Calculate the affine transformation using the images 
        Then apply transformation to grid, and compare to the MAIA transofmred grid.
        
        return the diffference points.(in pixels)

        """

        """ register the images """
        success = self.register(eye)
        
        if not success:
            print("Failed to register")

            print("Trying again.... ")
            again = self.register(eye,complete=True)
            if not again:
                print("Failed to register again.")
                return None

        """ apply the result of the registration to the points in this eye """
        out = self.applyTransformationToPoints()
        
        """ convert destination points to pixels """
        eye.degToPixels()

        
        return np.array(out) - np.array(eye.points)



    def manualRegister(self,eye):
        """
        use manual points to register two eyes. used when auto fails
        """
        if not hasattr(self,'manual_points'):
            return None
        
        if not hasattr(eye,'manual_points'):
            return None
        
        if not hasattr(self,'points'):
            self.degToPixels()
            
        if not hasattr(eye,'points'):
            eye.degToPixels()
        
        affine = cv2.estimateAffine2D(self.manual_points,eye.manual_points)
        
        points = np.array(self.points)
        ones = np.zeros(shape=(len(points),1))
        points_ones = np.hstack([points,ones])
        t_points = affine[0].dot(points_ones.T).T
        
        return t_points - eye.points
        

    def register(self,eye,model=2,dryrun=False,complete=False,rotate=True):
        """ 
        Use GDB-ICP to calculate affine transformation
        
        -model 0 = affine
        -model 1 = homography
        -model 2 = Quadratic
        -model 3 = Homography with Radial Lens Distortion
        -model 4 = Similarity

        output should be in the file:
        mosaic_stillPicture_to_stillPicture.xform


        idea: copy files to tmp folder, rename as to easier identify

        """
        tmp = "/tmp/"
        src = os.path.join(tmp,"%s_%s.png"%(self.patient_id,self.exam_id))
        dst = os.path.join(tmp,"%s_%s.png"%(eye.patient_id,eye.exam_id))

        if rotate:
            src_img = cv2.imread(self.getStillImage(),-1)
            src_img = cv2.rotate(src_img,cv2.ROTATE_90_COUNTERCLOCKWISE)
            cv2.imwrite(src,src_img)
            dst_img = cv2.imread(eye.getStillImage(), -1)
            dst_img = cv2.rotate(dst_img, cv2.ROTATE_90_COUNTERCLOCKWISE)
            cv2.imwrite(dst, dst_img)
            print("Rotated images")
        else:
            shutil.copyfile(self.getStillImage(),src)
            shutil.copyfile(eye.getStillImage(),dst)
        
            print( "Copying %s to %s" %(self.getStillImage(),src))
            print("Copying %s to %s" %(eye.getStillImage(),dst))

        extra = ""
        if complete:
            extra = "-complete"
        gdbicp_cmd = "./gdbicp %s %s -model %d %s" %(src,dst,model,extra)
        wrd = "experiments/microperimetry/src/"
        wrd = os.path.join(os.path.expanduser("~"),wrd)
        if dryrun:
            return gdbicp_cmd
        else:
            subprocess.call(gdbicp_cmd,cwd=wrd,shell=True)
        
        out = "mosaic_%s_to_%s.xform" %(os.path.basename(src)[:-4],os.path.basename(dst)[:-4])
        
        

        return self.loadRegistrationOutput(out)

    
    def loadRegistrationOutput(self,filename):
        """
        Read in gdbicp output, depends on mode

        i think numpy is row major
        """
        
        if not os.path.exists(filename):
            print(" --- Registration Failed --- ")
            #try again
            return False

        data = [line.rstrip() for line in open(filename)]

        if data[5] == "QUADRATIC":

            self.B = np.zeros(shape=(2,3))
            row=0
            for r in data[7:9]:
                col=0
                for c in r.split(' '):
                    self.B[row][col] = float(c)
                    col+=1
                row+=1
    
            row=0
            self.A = np.zeros(shape=(2,2))
            for r in data[9:11]:
                col=0
                for c in r.split(' '):
                    self.A[row][col] = float(c)
                    col+=1
                row+=1

            self.T = np.zeros(shape=(2,1))
            self.C = np.zeros(shape=(2,1))
            t = data[11].split(' ')
            self.T[0][0] = float(t[0])
            self.T[1][0] = float(t[1])
            self.C[0][0] = float(t[2])
            self.C[1][0] = float(t[3])

            return True
        return False
    
    def applyTransformationToPoints(self,reg=None):
        """
        given the registration output, apply this to the points. 
        e.g. eye1 -> eye2 ;  eye1 x T = eye2
        apply eye1.points x T  = eye2.points


        QUADRATIC:
        P = [^p1,^p2].T = [p1,p2]-[c1,c2] 
        [qx,qy] = B * [^px**,^py**,^px^py].T + A*P+T
        
        """
        
        """ make sure points are converted"""
        self.degToPixels()

        out = []
        QUADRATIC = 2
        mode = 2
        if mode == QUADRATIC:
            for p in self.points:
                P = np.matrix([p[0],p[1]]).T - self.C
                dP = np.matrix([P[0,0]**2,P[1,0]**2,P[0,0]*P[1,0]]).T
                R = self.B * dP + self.A*P+self.T
                out.append( [R.item(0),R.item(1)]) 
        return np.array(out).astype(int)

    def transformPoints(self,reg):
        """ transform the points """

        self.degToPixels()

        out = []
        QUADRATIC = 2
        mode = 2
        if mode == QUADRATIC:
            for i,row in self.dataframe.iterrows():
                # for p in self.points:
                p = row['points']
                P = np.matrix([p[0], p[1]]).T - reg.C
                dP = np.matrix([P[0, 0] ** 2, P[1, 0] ** 2, P[0, 0] * P[1, 0]]).T
                R = reg.B * dP + reg.A * P + reg.T

                out.append({
                    'ID':row["ID"],
                    't_points':[int(round(R.item(0))), int(round(R.item(1)))]
                    })
        return pd.DataFrame.from_records(out)

    def transformPoint(self,point,reg):
        """ transform a single point """

        self.degToPixels()

        out = []
        QUADRATIC = 2
        mode = 2
        p = point
        if mode == QUADRATIC:
            P = np.matrix([p[0], p[1]]).T - reg.C
            dP = np.matrix([P[0, 0] ** 2, P[1, 0] ** 2, P[0, 0] * P[1, 0]]).T
            R = reg.B * dP + reg.A * P + reg.T
            out = [int(R.item(0)), int(R.item(1))]
        return out

    def degToPixels(self,points=None):
        """
        Fixation points are in deg, convert to pixels coordinates
        
        also, rotate by 90deg ccw

        image size=1024
        deg2pixel ratio = 0.0356

        P = (deg/ratio) + size/2 
        """
        ratio = 0.0356
        size = 1024/2

        points=[]
#        for d in self.data:
#            _x = d["x_deg"]/ratio + size
#            _y = d["y_deg"]/ratio + size
#
#            #x = size-(_y-size)
#            #y = _x
#
#            points.append([_x,_y])
#            
        for index,row in self.dataframe.iterrows():
            _x = row["x_deg"]/ratio +size
            _y = row["y_deg"]/ratio +size
            points.append([_x,_y])       
        self.points = np.array(points).astype(int)


        # #apply offset to all but the ONH
        # if self.baseline_exam_id == '-1':
        #     t_points = np.array(self.points[1:]) + np.array([self.examPRLX,self.examPRLY])
        # else:
        #     t_points = np.array(self.points[1:]) + np.array([self.baselinePRLX,self.baselinePRLY])
        t_points = self.points[1:]
        all_points = []
        all_points.append(self.points[0])
        self.points = all_points + t_points.tolist()
        self.dataframe['points'] = self.points

    def getStillImage(self):
        """
        get the raw fundus image associated with this experiment
        """
        filename = os.path.join(self.datadir,"stillPicture.png")
        
        if os.path.exists(filename):
            return filename
        return None

    def getStimLayer(self):
        """
        Get the png gertnerate with the stimulartion points
        :return:
        """
        filename = os.path.join(self.datadir, "stim_layer.png")

        if os.path.exists(filename):
            return filename
        return None

    def getFixationImage(self):
        """
        get the raw fixation overlay image.
        :return:
        """
        filename = os.path.join(self.datadir, "fix_layer.png")

        if os.path.exists(filename):
            return filename
        return None

    def getFixationChart(self):
        """
        get the raw fixation overlay image.
        :return:
        """
        filename = os.path.join(self.datadir, "fixation_chart.png")

        if os.path.exists(filename):
            return filename
        return None

    def calculateBlur(self,use_mask=False,use_special_mask=True):
        """
        using the variance of the laplacian, calclate an estimate for the
        blur / focus od the image
        also return the mean pixel value.
        """
        #print "Calcuating Blur %s"%(self.getStillImage())
        
        image = cv2.imread(self.getStillImage(), -1)
        if use_mask:
            #makesure we have all the points in pixel coordinates
            if use_special_mask:
                mask = self.createGridMask()
            else:
                self.degToPixels()
                (x,y),r = cv2.minEnclosingCircle(np.array(self.points).astype(int))
                mask = np.zeros(image.shape, dtype=np.uint16)
                cv2.circle(mask,(int(x),int(y)),int(r),65535,-1)
            image = cv2.bitwise_and(image, mask)

        return cv2.Laplacian(image,cv2.CV_64F).var(),image.mean()

    def overlayGrid(self,image=None):
        """
        load fundus image and measurement grid, overlay and return
        """
        if image is None:
            image = cv2.imread(self.getStillImage(),-1)
            image = cv2.transpose(image)
            image = cv2.flip(image, 0)

        grid = cv2.imread(os.path.join(os.path.dirname(self.getStillImage()),"stim_layer.png"),-1)
        grid_16 = np.array(grid,dtype=np.uint16)
        grid_16*=256
        mask = grid_16[:,:,3]
        mask = cv2.bitwise_not(mask)
        image = cv2.bitwise_and(image,mask)
        image = cv2.cvtColor(image,cv2.COLOR_GRAY2BGRA)
        out = cv2.addWeighted(image,1.0,grid_16,1.0,0)
        return (out/256).astype('uint8')

    def renderGrid(self,img=None,rotate=False):
        if img is None:
            img = cv2.imread(self.getStillImage())
            img = cv2.cvtColor(img,cv2.COLOR_BGR2BGRA)
            img = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)

#        home = os.path.expanduser('~')
#        datadir = os.path.join(home, "experiments/microperimetry/data")
#        filename = os.path.join(datadir, "%s/%s/stim_layer.png" % (self.patient_id, self.exam_id))
        filename = os.path.join(self.datadir, "stim_layer.png")
        
        layer = cv2.imread(filename,-1)

        #use alpha as mask
        mask = cv2.split(layer)[3]
        mask = cv2.cvtColor(mask,cv2.COLOR_GRAY2BGRA)
        mask = 255 - mask
        img = cv2.bitwise_and(img,mask)
        out =  cv2.addWeighted(img,1.0,layer,1.0,1.0)

        if rotate:
            out = cv2.rotate(out,cv2.ROTATE_90_CLOCKWISE)
        return cv2.cvtColor(out,cv2.COLOR_BGRA2BGR)
        #return img#cv2.bitwise_not(mask,img)


    def diffGrid(self):
        """
        As aI have found errors in the MAIA database,
        compare the two renders of the grids.

        :return:
        """
        radius = 5
        home = os.path.expanduser('~')
        datadir = os.path.join(home, "experiments/microperimetry/data")
        filename = os.path.join(datadir, "%s/%s/stim_layer.png" % (self.patient_id, self.exam_id))
        layer = cv2.imread(filename, -1)
        layer = cv2.rotate(layer, cv2.ROTATE_90_CLOCKWISE)
        # use alpha as mask
        mask = cv2.split(layer)[3]
        if not hasattr(self, 'points'):
            self.degToPixels()

        grid = np.zeros(mask.shape, dtype=mask.dtype)
        for i in range(len(self.points)):
            p = self.points[i]# - [6,7]
            grid = cv2.circle(grid,tuple(p),radius,255,thickness=-1,lineType=cv2.LINE_AA)

        max =  grid.sum() + mask.sum()
        return cv2.bitwise_xor(grid,mask),max

    def getLayerGrid(self):
        home = os.path.expanduser('~')
        datadir = os.path.join(home, "experiments/microperimetry/data")
        filename = os.path.join(datadir, "%s/%s/stim_layer.png" % (self.patient_id, self.exam_id))
        return cv2.imread(filename, -1)

    def getGridLocationsFromImage(self):

        img = self.getLayerGrid()
        gimg = cv2.split(img)[3]
        gimg = cv2.rotate(gimg,cv2.ROTATE_90_CLOCKWISE)

        circles = cv2.HoughCircles(gimg, cv2.HOUGH_GRADIENT, 1, 40,
                                   param1=10, param2=5, minRadius=5, maxRadius=8)
        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            # draw the outer circle
            cv2.circle(img, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv2.circle(img, (i[0], i[1]), 2, (0, 0, 255), 3)

        return img,circles


    def drawGrid2(self,image=None,registration=None):
        """
        Simpler version apply registration on the fly

        :param image:
        :param registration:
        :return:
        """

        radius = 5
        # colour = (255,0,0)
        alpha=1.0
        if image is None:
            image = cv2.imread(self.getStillImage())

        grid = np.zeros(image.shape, dtype=image.dtype)
        # thresholds = self.dataframe[['Threshold', 'x_deg', 'y_deg', 'ID']].to_dict('records')

        for i,row in self.dataframe.iterrows():
            p = row['points']
            if registration is not None:
                p = self.transformPoint(p,registration)

        t = row['Threshold']
        _c = 127
        max = 36.0
        if abs(t) == 0:
            colour = (180, 254, 255)
        else:
            if (t > 0):
                colour = (abs(_c * (float(t) / float(max))), 255, 255)

            if (t < 0):
                colour = (126 + abs(_c * (float(t) / float(max))), 255, 255)

            grid = cv2.circle(grid, tuple(p), radius, colour, thickness=-1, lineType=cv2.LINE_AA)
            grid = cv2.cvtColor(grid, cv2.COLOR_HSV2BGR)
            image = cv2.circle(image, tuple(p), radius, (0, 0, 0), thickness=1, lineType=cv2.LINE_AA)
            output = cv2.addWeighted(image, 1.0, grid, alpha, 1.0)
        return output

    def drawGrid(self,image=None,registration=None,points=None,radius=5,alpha=1.0,\
                 colour=None,thresholds=None,rotate=False,drawValues=False,drawIds=False,\
                 drawHistory=False,drawMovement=False,fontSize=1.0):
        """
        Draw points onto fundus image

        :return:
        """

        if not hasattr(self,'points'):
            self.degToPixels()

        if points is None:
            _points = self.points
        else:
            _points = points


        if image is None:
            image = cv2.imread(self.getStillImage())

        if image.shape[2] == 4:
            image = cv2.cvtColor(image,cv2.COLOR_BGRA2BGR)
            image = cv2.rotate(image,cv2.ROTATE_90_CLOCKWISE)

        if rotate:
            image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)
        c_image  = image#cv2.cvtColor(image,cv2.COLOR_GRAY2BGR)
        grid = np.zeros(c_image.shape,dtype=c_image.dtype)

        if thresholds is None:
            thresholds = self.dataframe[['Threshold', 'x_deg', 'y_deg', 'ID']].to_dict('records')

            #thresholds = self.dataframe['Threshold'].tolist()

        max=36.0
        #_c = (65535./2)/max

        #hsv 0-127
        _c = 127#63.5/max

        for i,row in self.dataframe.iterrows():
            p = row['points']
            if registration is not None:
                p = self.transformPoint(p,registration)
            if colour is None:
                t = row['Threshold']
                _colour = self.threshold_lookup[t]
            else:
                _colour = colour

            # lookup is in rgb, convert to bgr
            _colour = (_colour[2],_colour[1],_colour[0])

            grid = cv2.circle(c_image, tuple(p), radius, _colour, thickness=-1, lineType=cv2.LINE_AA)
            # if t==-1:
            #     c_image = cv2.circle(c_image, tuple(p), radius, (255, 255, 255), thickness=1, lineType=cv2.LINE_AA)

            fontSize = radius / 10.

            #p_t = (p_t[0], p_t[1])
            if drawValues:
                p_t = p + np.array([-5, -(radius*2)])
                # if t == -1:
                #     cv2.putText(grid, str(t), tuple(p_t), cv2.FONT_HERSHEY_PLAIN, fontSize+0.1, (255,255,255), 1, cv2.LINE_AA)
                # else:
                #     cv2.putText(grid, str(t), tuple(p_t), cv2.FONT_HERSHEY_PLAIN, fontSize+0.1, (0,0,0), 1, cv2.LINE_AA)

                cv2.putText(grid, str(t), tuple(p_t), cv2.FONT_HERSHEY_PLAIN, fontSize, _colour, 1, cv2.LINE_AA)
            if drawIds:
                p_i = p + np.array([5, 5])
                cv2.putText(grid, "#%s"%row['ID'], tuple(p_i), cv2.FONT_HERSHEY_PLAIN, fontSize-0.1, (0,0,0), 1, cv2.LINE_AA)
                p_i = p + np.array([6, 6])
                cv2.putText(grid, "#%s"%row['ID'], tuple(p_i), cv2.FONT_HERSHEY_PLAIN, fontSize-0.1, (255,255,255), 1, cv2.LINE_AA)


            #Draw History

            if drawHistory:
                offset = 5
                count = 0
                history = row['history'].split(',')[:-1]
                for h in history:
                    p1 = p + np.array([-5+(offset*count),15])
                    p2 = p + np.array([-10+(offset*count),20])
                    last = 0
                    if count+1 >= len(history):
                        #then last so use final value.
                        last = int(row['final_intensity'])
                    else:
                        last = int(history[count+1])


                    if(int(h) > last):
                        #negatvie change - seen
                        t_colour = (0, 255, 0)  # green did see
                    elif(int(h) < last):
                        t_colour = (0, 0, 255)  # red didnt see
                    else:
                        t_colour = (0, 255, 0)  # green did see

                    cv2.rectangle(grid,tuple(p1),tuple(p2),t_colour,-1)
                    cv2.rectangle(grid,tuple(p1),tuple(p2),(1,1,1),1)
                    count+=1

            if drawMovement:
                movement = row["movement"]
                if row['ID']==0:
                    continue
                max = 2.0
                min = 0.0
                offset = 5
                v_offset = 21
                count = 0
                for m in movement:
                    p1 = p + np.array([-5 + (offset * count), v_offset])
                    p2 = p + np.array([-10 + (offset * count), v_offset+5])

                    scale = 0.333 / max
                    h = m * scale
                    if h>0.333:
                        h=0.333
                    h = (0.333-h)
                    rgb = colorsys.hsv_to_rgb(h,1,1)
                    #BGR
                    m_colour = (round(rgb[2]*255),round(rgb[1]*255),round(rgb[0]*255))
                    cv2.rectangle(grid, tuple(p1), tuple(p2), m_colour, -1)
                    cv2.rectangle(grid, tuple(p1), tuple(p2), (1, 1, 1), 1)
                    count += 1

        c_grid = grid #cv2.cvtColor(grid,cv2.COLOR_HSV2BGR)
        output = cv2.addWeighted(c_image,1.0,c_grid,alpha,1.0)
        return c_image


    def showGrid(self,radius=5,alpha=1.0,drawText=False,image=None,rotate=True):
        image = self.drawGrid(radius=radius,alpha=alpha,drawText=drawText,image=image,rotate=rotate)
        cv2.namedWindow("%d"%self.patient_id, cv2.WINDOW_NORMAL)
        cv2.imshow("%d"%self.patient_id, image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def createGridMask(self,radius=10):
        """
        Crate a mask base don the simiulation points.
        :return: mask
        """
        if not hasattr(self,'points'):
            self.degToPixels()


        image = cv2.imread(self.getStillImage(),-1)

        mask = np.zeros(image.shape,dtype=image.dtype)

        for p in self.points:
            cv2.circle(mask,tuple(p),radius,65535,-1)

        return mask

    def gradient(self,img=None,trim=1,return_padded=False):
        """
            Calculate a gradient sensitiviy map, use the sobel operator.
        
        assumes a 10-2 grid. Fit it all into a 10x10 matrix.
        Use a 3x3 sobel kernel
        """
        if img is None:
            img  = self.asImg()
        else:
            #need to run self.asImg() to get x,y coords
            self.asImg()
        if trim is not None:
            img  = img[trim:-trim,trim:-trim]
            
                
        x_deg = cv2.Sobel(img,cv2.CV_16S,1,0,3)
        y_deg = cv2.Sobel(img,cv2.CV_16S,0,1,3)
        
        grad = np.linalg.norm((x_deg,y_deg),axis=0)
        
        
        #pad so shape is same as original img
        if trim is not None:
            g_p = np.zeros([img.shape[0]+trim*2,img.shape[1]+trim*2])
        
        g_p[trim:grad.shape[0]+trim,trim:grad.shape[1]+trim]=grad
        
        #loop through original data points and assign gradient
        for d in self.data[1:]:
            if 'y' in d:
                d['gradient']=g_p[d['y'],d['x']]
            else:
                d['gradient']=0
        
        #self.dataframe = pd.DataFrame.from_records(self.data)
        if return_padded:
            return g_p #(grad - np.mean(grad))/np.std(grad)
        return grad
    
    def newGradient(self):
        """
        Calculate gradient at a each point
        """
        
        return None
        
    
    def asImg(self,size=(10,10)):
        """
            Return gid as an image 
        """
        
        img = np.zeros(size)
        
        for d in self.data[1:]:
            x = (int(round(d['x_deg'])) +size[0])/2
            y = (size[1] - int(round(d['y_deg'])))/2
            
            if x+1>size[0] or y+1>size[0] or x<0 or y<0:
                continue
    
            img[y,x] = d['Threshold']
            d['x']=x
            d['y']=y
            
        return img
                  
class MyDict(dict):
    def __init__(self,*args,**kwargs):
        dict.__init__(self,*args,**kwargs)

    def __getitem__(self,key):
        val = dict.__getitem__(self,key)
        if not val.loaded:
            print("Loading",key)
            val.load()
        return val
    
    def __setitem__(self,key,val):
        dict.__setitem__(self, key, val)
        
class Eyes:
    """
    
    Class that manages both eyes, subject and timeseries data. 
    
    """

    def __init__(self, directory,preload=False,parse=True):
        """
            Given a directory, load all the eyes.
        """
        
        self.subjects = {}
        self.dir = directory
        if parse:
            self.loadEyes(directory,preload=preload)
      
    def __getitem__(self,key):
        #check if loded
        return self.subjects[key]
        
    def addEye(self,pid,eid,preload=False):
        """
        Manual add eyes (loaded from xml only)
        """
        #self.datadir = "/data/backup/MAIA/MAIA_backup_20180327/"
        e = Eye()
        e.init(pid,eid)
        e.loadXML(datadir=self.dir)
        e.dataframe.sort_values(by="ID",inplace=True)
        if not self.subjects.has_key(e.patient_id):
            self.subjects[e.patient_id] = MyDict()
        self.subjects[e.patient_id][e.exam_id] = e
        
    def loadEyes(self,directory,preload=True):
        """
            find all MP data files, 
            sort into subect, 
        """
        
        t_files=[]
        for root, dirnames, filenames in os.walk(directory):
            for filename in fnmatch.filter(filenames, '*threshold.txt'):
                t_files.append(os.path.join(root, filename))
    
        for file in t_files:
            e = Eye(threshold_filename=file,preload=preload)
            if not self.subjects.has_key(e.patient_id):
                self.subjects[e.patient_id] = MyDict()
            self.subjects[e.patient_id][e.exam_id] = e
        
        """ sort through each subject and order exams by date """
    
    def checkPreviousRegistration(self,pid,exam1,exam2):
        """
        As the outpurt of the registration is saved to file, check to see if it exists
        
        """
        home = os.path.expanduser('~')
        datadir = os.path.join(home,"experiments/microperimetry/registered/")
        
        filename = "mosaic_%d_%d_to_%d_%d.xform" %(pid,exam1,pid,exam2)
        filename = os.path.join(datadir,filename)
        #print "searching for : %s " %(filename)
        if os.path.exists(filename):
            return filename
        return None

    def pws_comp(self,pid,e1,e2,ttype=None):
        """
        Generate a dataframe for input into R for effects analyis.
        include all points, include which eye, and which test
        """
        
       
        
        #do the registration;
        
        filename = self.checkPreviousRegistration(pid,e1,e2)
        sub = self.subjects[pid]
        src = sub[e1]
        dest = sub[e2]
        
        """ check they are the same eyes """ 
        if src.eye != dest.eye:
            print("Eyes do not match ")
            return
        out = []
        
        for i in range(len(src.data)):
            out.append( {
                    "threshold": src.data[i]['Threshold'],
                    "test": 0,
                    "type":ttype
                    })
        for i in range(len(dest.data)):
            out.append( {
                    "threshold": dest.data[i]['Threshold'],
                    "test": 1,
                    "type":ttype
                    })
        
        return pd.DataFrame.from_records(out)
            

    def pwComp(self,pid,e1,e2,ttype=None):
        """
            Return a list of points, with all the comparisions generated
            Avg, regisstration  etc for Test Retest 
        """
        
        #do the registration;
        
        filename = self.checkPreviousRegistration(pid,e1,e2)
        sub = self.subjects[pid]
        src = sub[e1]
        dest = sub[e2]
        
        """ check they are the same eyes """ 
        if src.eye != dest.eye:
            print("Eyes do not match ")
            return
        if filename is None:
            regDiff = None
        else:
            #print "Found previous registrations"
            if src.loadRegistrationOutput(filename):
                out = src.applyTransformationToPoints()
                dest.degToPixels()
                regDiff = np.array(out) - np.array(dest.points)
            else:
                regDiff = None
                
        if regDiff is not None:
            for i in range(len(src.data)):
                src.data[i]['registration_error_x']=regDiff[i][0]
                src.data[i]['registration_error_y']=regDiff[i][1]
                src.data[i]['registration_error_magnitude']=np.linalg.norm(regDiff[i])

                src.data[i]['avg_threshold'] = (src.data[i]['Threshold'] + dest.data[i]['Threshold']) /2.
                src.data[i]['diff_threshold'] = abs(src.data[i]['Threshold'] - dest.data[i]['Threshold'])
                
                
                _t1 = src.data[i]['Threshold']
                _t2 = dest.data[i]['Threshold']
                
                if _t1 >0 and _t2 >0:                
                    thing  = abs(_t1 - _t2) / max(_t1, _t2)
                    src.data[i]['normalised_threshold_difference'] = thing
                src.data[i]['Threshold_2'] = dest.data[i]['Threshold']
                src.data[i]['eccentric_mag'] = np.linalg.norm( (src.data[i]['x_deg'],src.data[i]['y_deg']) )
                src.data[i]['condition'] = ttype
                src.data[i]['eye'] = src.eye
                src.data[i]['dst_exam_id'] = e2

        else:
            #print "No Registration found"
                
            for i in range(len(src.data)):
                #src.data[i]['reg_x']=regDiff[i][0]
                #src.data[i]['reg_y']=regDiff[i][1]
                #src.data[i]['reg_mag']=np.linalg.norm(regDiff[i])
                _t1 = src.data[i]['Threshold']
                _t2 = dest.data[i]['Threshold']
                
                if _t1 >0 and _t2 >0:                
                    thing  = abs(_t1 - _t2) / max(_t1, _t2)
                    src.data[i]['normalised_threshold_difference'] = thing
                
                src.data[i]['Threshold_2'] = dest.data[i]['Threshold']
                src.data[i]['avg_threshold'] = (src.data[i]['Threshold'] + dest.data[i]['Threshold']) /2.
                src.data[i]['diff_threshold'] = abs(src.data[i]['Threshold'] - dest.data[i]['Threshold'])
                src.data[i]['eccentric_mag'] = np.linalg.norm( (src.data[i]['x_deg'],src.data[i]['y_deg']) )
            
                src.data[i]['eye'] = src.eye
                src.data[i]['condition'] = ttype

                src.data[i]['dst_exam_id'] = e2
        return pd.DataFrame.from_records(src.data).sort_values(by="ID")
        

    def compare(self,pid,exam1,exam2,offline=False,calcblur=False,dryrun=False,register=False,thresh=4.5):
        """
        Compare two eyes, within patient - 
        genterate pointwise regestration differences
        PW sensitivity changes
        also, look at blur fidderence
        """
        
        #print "___ comparing PID: %d src: %d dst: %d ___" %(pid,exam1,exam2)
        
        
        filename = self.checkPreviousRegistration(pid,exam1,exam2)
        
        sub = self.subjects[pid]
        src = sub[exam1]
        dest = sub[exam2]
        
        """ check they are the same eyes """ 
        if src.eye != dest.eye:
            print("Eyes do not match ")
            return
        if src.length != dest.length:
            print("Grids are different lengths: %d %d : %d"%(pid,exam1,exam2))
        
        if filename is None:
            if not offline:
                if dryrun:
                    print("DRY")
                    return src.register(dest,dryrun=dryrun)
                else:
                    regDiff = src.compareRegistration(dest)
            else:
                regDiff = None

        else:
            #print "Found previous registrations"
            if dryrun:
                return None
            if src.loadRegistrationOutput(filename):
                out = src.applyTransformationToPoints()
                dest.degToPixels()
                regDiff = np.array(out) - np.array(dest.points)
            else:
                regDiff = None
        
        """ compare with TMS and mean diff """

        tms = df.thresholdDiff(src.thresholds(),dest.thresholds())
        tsad = df.tsad(src.thresholds(),dest.thresholds(),thresh=thresh)
        tsd = df.tsd(src.thresholds(),dest.thresholds(),thresh=thresh)
        sad = df.sad(src.thresholds(),dest.thresholds())
        ssd = df.ssd(src.thresholds(),dest.thresholds())
        sd = df.sd(src.thresholds(),dest.thresholds())
        
        stsad = df.stsad(src.thresholds(),dest.thresholds(),thresh=thresh)
        stsd  = df.stsd(src.thresholds(),dest.thresholds(),thresh=thresh)

       # rms_mean = math.sqrt(mean_squared_error(src.avg,dest.avg))
        
        mean_pws,std_pws = df.mpws(src.thresholds(),dest.thresholds())
        mean = src.avg - dest.avg
        m_avg = (src.avg + dest.avg) /2

        cor = df.cor(src.thresholds(),dest.thresholds())
        
        cor_mean = df.corMs(src.avg,dest.avg)
        
        if calcblur:
            blur_s,bright_s = src.calculateBlur(use_mask=False)
            blur_d, bright_b = dest.calculateBlur(use_mask=False)
            blur = abs(blur_s - blur_d)
            bright = abs(bright_s - bright_b)
        else:
            blur = None
            bright = None

        BCEA_63 = np.average((src.BCEA_63, dest.BCEA_63))
        BCEA_95 = np.average((src.BCEA_95, dest.BCEA_95))
        BCEA_63_log = 0
        if(BCEA_63 != 0):
            BCEA_63_log = math.log10(BCEA_63)

        BCEA_95_log = 0
        if (BCEA_95 != 0):
            BCEA_95_log = math.log10(BCEA_95)

        reg_mag=None
        if regDiff is not None:
           reg_mag = np.linalg.norm(np.mean(regDiff,axis=0))
        
        return {"stsd":stsd, 
                "stsad":stsad, 
                "tsad":tsad,
                 "tsd":tsd,
                 "sd":sd, #sum of difference (zero threshold)
                 "tms":tms,
                 "pws_mean":mean_pws,
                 "pws_std":std_pws,
                 "reg diff":regDiff,
                 "sad":sad,
                 "ssd":ssd,
                 "mean_mean":m_avg,
                 "blur":blur,
                 "bright":bright,
                 "cor":cor,
                 "cor_mean":cor_mean,
                 "pid":pid,
                 "eid1":exam1,
                 "eid2":exam2,
                 "date":dest.date,
                 "mean_diff":mean,
                 "reg_mag":reg_mag,
                 "BCEA_angle":np.average((src.BCEA_angle,dest.BCEA_angle)),
                 "BCEA_63":np.average((src.BCEA_63,dest.BCEA_63)),
                 "BCEA_95":np.average((src.BCEA_95,dest.BCEA_95)),
                 "BCEA_63_log10": BCEA_63_log,
                 "BCEA_95_log10": BCEA_95_log,
                 "age":src.age,
                 "eye":src.eye             
                 }


    def displayComparision(self,pid,exam1,exam2,offline=True,dryrun=False,registation=None):
        """
        Take n exams,
        get registration and display the results over the first.

        :return:
        """
        sub = self.subjects[pid]
        src = sub[exam1]
        dest = sub[exam2]
        if registation is None:

            filename = self.checkPreviousRegistration(pid, exam1, exam2)
            if filename is None:
                if not offline:
                    if dryrun:
                        print("DRY")
                        return src.register(dest,dryrun=dryrun)
                    else:
                        regDiff = src.compareRegistration(dest)
                else:
                    regDiff = None

            else:
                print("Found previous registrations")
                if dryrun:
                    return None
                if src.loadRegistrationOutput(filename):
                    out = src.applyTransformationToPoints()
                    dest.degToPixels()
                    regDiff = np.array(out) - np.array(dest.points)
                else:
                    regDiff = None


            if regDiff is None:
                print("registration failed")
                return

        else:
            src.loadRegistrationOutput(registation)
            out = src.applyTransformationToPoints()
            regDiff = np.array(out) - np.array(dest.points)

        img = dest.drawGrid(radius=5,colour=(120,255,255))
        out_image = dest.drawGrid(points=out,radius=1,image=img,rotate=False,colour=(258,255,255 ))
        cv2.imwrite('%d_%d_%d.tiff'%(pid,exam1,exam2),out_image)
        return regDiff

                    
if __name__ == "__main__":
        
    matplotlib.rcParams['lines.markeredgewidth'] = 1
    
    home = os.path.expanduser("~")
    dir = 'Dropbox/docs/ORG/Msc/project/experiments/microperimetry/test_data/'
    dir = os.path.join(home,dir)
    
    files = glob.glob(dir+"*threshold.txt")
    
    
        
        
