from .Scan import Scan
import os,cv2
import numpy as np
import xml.etree.ElementTree as ET
import datetime

class OCT(Scan):
    """
    Class to manage an OCT scan, is agnostic to filename format

    Not all OCT scan file format is the same, some requires cropping.
    Some have the roi on the IR image, some don't


    This class handles Mitals data format.

    for other formats, inherit and overload load() and extractSliceData()

    """

    mode = "OCT"

    class Slice():

        def __init__(self,fname=None,ir=None,slice=None,preload=False):
            self.fname = fname
            self.ir = ir
            self.slice = slice
            self.start = None
            self.end = None
            self.preload = preload


        def setSliceImage(self,fname):
            self.fname = fname
            if self.preload:
                self.slice = cv2.imread(fname)


        def getSliceImage(self):
            if self.slice is None:
                self.slice = cv2.imread(self.fname)
            return self.slice

    def __init__(self,date=None,pid=None,visit=None,eye=None,preload=False):
        Scan.__init__(self, date=date, pid=pid, visit=visit, eye=eye,preload=preload)
        self.xml_mode = False
        self.data = {}


    def __getitem__(self, item):
        return self.data[item].getSliceImage()



    def getIRImage(self,slice=0):
        """
        return the first slice ir image
        :return:
        """
        if self.xml_mode:
            return Scan.getIRImage(self)

        if self.data[slice].ir is None:
            return self.ir_image
        return self.data[slice].ir

    def getSliceImage(self,slice):
        return self.data[slice].getSliceImage()

    def getSlice(self,slice):
        return self.data[slice]

    def getSliceCount(self):
        return len(self.data)

    def load(self,fname,slice):
        """
        1536 , 768 - mital
        2032 , 596 - RP (line scan)
        1520 , 596 - Iain (slices)

        :param fname:
        :param slice:
        :return:
        """

        # """ check exists """
        if not os.path.exists(fname):
            raise Exception("[Scan::OCT::load()] file %s does not exit" % fname)

        # print "[Scan::OCT::Load()] Loading %s Slice %d" %(os.path.basename(fname),slice
        img = cv2.imread(fname,-1)
        if img.shape == (768,1536,3):
            ir_img = img[:, :int(img.shape[1] / 2), :]
            slice_img = img[:, int(img.shape[1] / 2):, :]
            self.data[slice] = self.Slice(fname,ir_img,slice_img)
        elif img.shape == (596,2032,3):
            ir_img = img[:489, :495,:]
            slice_img = img[:489, 495:,:]
            self.data[slice] = self.Slice(fname,ir_img,slice_img)


    def loadFromXml(self,xml):
        """
        Load OCT scan from an XML file.
        As exported from heidleberg.
        :param fname:
        :return:
        """
        self.xml_tree = ET.parse(xml)
        root = self.xml_tree.getroot()
        body = root.find("BODY")
        self.data_dir = os.path.dirname(xml)

        self.pid = body.find("Patient").find("LastName").text
        series = root.find("BODY").find("Patient").find("Study").find("Series")
        self.loadSeries(series)
        # self.visit = os.path.basename(xml)[:-4]

        self.xml_mode = True

    def loadSeries(self, series):
        """
        load an individual series from an xml element
        :param series:
        :return:
        """

        if not isinstance(series,ET.Element):
            raise Exception("Not an xml element")

        self.xml_series = series

        # if series is not a volumetric oct...quit.
        type = series.find("Type").text
        if type != "Volume":
            # print(" Skipping : %s"%type)
            return False

        self.eye = None

        self.visit = series.find("ID").text

        # print("\tLoading OCT: %s Series: %s"%(self.pid,self.visit))

        num_images = int(series.find("NumImages").text)
        images = series.findall("Image")

        oct_field_size_x = float(series.find("OCTFieldSize").find("Width").text)
        oct_field_size_y = float(series.find("OCTFieldSize").find("Height").text)
        self.oct_field_size = [oct_field_size_x, oct_field_size_y]

        for image in images:

            #laterality is found in the image, if a multiseries file
            if self.eye is None:
                self.eye = "OS"
                if image.find("Laterality").text == "R":
                    self.eye = "OD"


            type = image.find("ImageType").find("Type").text
            if type == "LOCALIZER":
                # save as IR image
                f = image.find("ImageData").find("ExamURL").text
                ir_fname = f[10:].split("\\")[-1]
                ir_fname = os.path.join(self.data_dir, ir_fname)
                if not os.path.exists(ir_fname):
                    raise Exception("[OCT::Scan::loadFromXML] %s file not found" % ir_fname)
                self.setIRImage(ir_fname)

                time = image.find("AcquisitionTime").find("Time")
                seconds = int(time.find("Second").text.split(".")[0])
                milliseconds = int(time.find("Second").text.split(".")[1])

                self.datetime = datetime.datetime(self.date.year,
                                                  self.date.month,
                                                  self.date.day,
                                                  int(time.find("Hour").text),
                                                  int(time.find("Minute").text),
                                                  seconds,
                                                  milliseconds
                                                  )

                # self.ir_image = cv2.imread(ir_fname,-1)
                self.scale_x = float(image.find("OphthalmicAcquisitionContext").find("ScaleX").text)
                self.scale_y = float(image.find("OphthalmicAcquisitionContext").find("ScaleY").text)
                self.localiser_width = float(image.find("OphthalmicAcquisitionContext").find("Width").text)
                self.localiser_height = float(image.find("OphthalmicAcquisitionContext").find("Height").text)
                self.localiser_focus = float(image.find("OphthalmicAcquisitionContext").find("Focus").text)
                self.localiser_angle = float(image.find("OphthalmicAcquisitionContext").find("Angle").text)
                self.localiser_autosensorgain = (image.find("OphthalmicAcquisitionContext").find(
                    "AutoSensorGain").text) == "true"
                self.localiser_sensor_gain = float(image.find("OphthalmicAcquisitionContext").find("SensorGain").text)
                self.localiser_num_ave = int(image.find("OphthalmicAcquisitionContext").find("NumAve").text)
            elif type == "OCT":
                f = image.find("ImageData").find("ExamURL").text
                fname = f[10:].split("\\")[-1]
                fname = os.path.join(self.data_dir, fname)
                if not os.path.exists(fname):
                    raise Exception("[OCT::Scan::loadFromXML] %s file not found" % ir_fname)

                slice_number = int(image.find("ID").text) - 1
                slice = self.Slice()
                slice.setSliceImage(fname)
                slice.scale_x = float(image.find("OphthalmicAcquisitionContext").find("ScaleX").text)
                slice.scale_y = float(image.find("OphthalmicAcquisitionContext").find("ScaleY").text)
                slice.start_x = float(
                    image.find("OphthalmicAcquisitionContext").find("Start").find("Coord").find("X").text)
                slice.start_y = float(
                    image.find("OphthalmicAcquisitionContext").find("Start").find("Coord").find("Y").text)
                slice.width = int(image.find("OphthalmicAcquisitionContext").find('Width').text)
                slice.height = int(image.find("OphthalmicAcquisitionContext").find('Height').text)

                slice.end_x = float(
                    image.find("OphthalmicAcquisitionContext").find("End").find("Coord").find("X").text)
                slice.end_y = float(
                    image.find("OphthalmicAcquisitionContext").find("End").find("Coord").find("Y").text)

                self.data[slice_number] = slice
        self.xml_mode = True
        return True


    def extractSliceData(self,slice):
        """
        The horizontal position of the OCT slice needs to be extracted.
        :return:
        """

        #if slice 0, then the botttom of the scan area is used.
        img = self.data[slice].ir
        green = img[:,int(round(img.shape[1]/2)),1] - img[:,int(round(img.shape[1]/2)),0]

        #get locations of the green pixels, should be 2 pixels high
        locations = np.where(green == 255)[0]
        if len(locations) == 4:
            """then we are either the first of last slice"""
            if slice==0:

                #return the lowest pixel
                return locations[2]
            else:
                return locations[0]

        else:
            """ then we have 3 lines, retrun the middle"""
            return locations[2]


    def sliceLine(self,image,slice,thickness=2,drawIndex=True):
        img = np.zeros(image.shape,dtype=np.uint8)
        if self.xml_mode:
            slice_0 = self.getSlice(slice)
            x1 = int(round(slice_0.start_x / self.scale_x))
            x2 = int(round(slice_0.end_x / self.scale_x))
            y1 = int(round(slice_0.start_y / self.scale_y))
            y2 = int(round(slice_0.end_y / self.scale_y))

            cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), thickness, cv2.LINE_AA)
        else:
            y_pos = self.extractSliceData(slice)
            img[y_pos-thickness:y_pos+thickness,:,1] = 255

        return img


    def drawSliceLine(self,image,slice,thickness=2,drawIndex=True):
        if self.xml_mode:
            slice_0 = self.getSlice(slice)
            x1 = int(round(slice_0.start_x / self.scale_x))
            x2 = int(round(slice_0.end_x / self.scale_x))
            y1 = int(round(slice_0.start_y / self.scale_y))
            y2 = int(round(slice_0.end_y / self.scale_y))

            cv2.line(image, (x1, y1), (x2, y2), (0, 255, 0), thickness, cv2.LINE_AA)
        else:
            y_pos = self.extractSliceData(slice)
            image[y_pos-thickness:y_pos+thickness,:,1] = 255


        if drawIndex:
            #draw slice number
            cv2.putText(image,
                        "[%d/%d]"%(slice+1,self.getSliceCount()),
                        tuple((10,50)),
                        cv2.FONT_HERSHEY_PLAIN,
                        3,
                        (0, 255, 0),
                        2,
                        cv2.LINE_AA)

    def overlayMPData(self,mp_scan,slice=0,reg=None,invert=False,alpha=0.3):
        """
        Overlay the MP IR and slice image with MP grid data.

        Search for the registartion internaly

        Goldman 3 is  0.43 deg.

        :param mp_scan:
        :return:
        """
        if reg is None:
            reg = self.person.getRegistration(mp_scan,self)
        if reg is None:
            raise Exception("Cannot find registration. Please re-run")

        # img = mp_scan.drawStimuli(self.data[slice].ir,reg=reg)
        slice_img = self.data[slice].slice.copy()

        # convert to bgra
        slice_img = cv2.cvtColor(slice_img,cv2.COLOR_BGR2BGRA)

        if self.xml_mode:
            slice_0 = self.getSlice(slice)
            y_loc = int(round(slice_0.start_y / self.scale_y))
        else:
            y_loc = self.extractSliceData(slice)

        # calculate thickness of mp stimuli.
        # (oct deg / oct pixel width) * goldman 3 deg.(0.43) = radius in pixels.

        thickness = (self.getSlice(slice).width / self.oct_field_size[0]) * 0.43
        offset = int(round(thickness / 2))

        # find any intersection of stimulation points.
        # should be round, but easier to do a square.
        width = self.data[slice].slice.shape[1]
        rect = [0,y_loc-offset,width,y_loc+offset]


        # transform points and store in the reg.
        # stored there we don't recalculate the transformation every time.
        if reg.t_points is None:
            reg.t_points = mp_scan.data.transformPoints(reg)

        #merge t_points with DF
        dataframe = mp_scan.data.dataframe.set_index('ID').join(reg.t_points.set_index('ID'))
        dataframe.reset_index(inplace=True)

        # helper function
        def contains(rect,y):
            return (rect[1] <= y <= rect[3])

        mp_scan.data.degToPixels()
        for i,row in dataframe.iterrows():
            if row['ID'] == 0:
                continue
            p = row['t_points']
            # p = mp_scan.data.transformPoint(p,reg)
            t = row['Threshold']
            if contains(rect,p[1]):
                #then draw onthe slice
                colour = mp_scan.data.threshold_lookup[t]
                if t==-1:
                    colour = (255,255,255,0)
                if self.xml_mode:
                    slice_0 = self.getSlice(slice)
                    x = int(round(p[0] - (slice_0.start_x/self.scale_x)))
                else:
                    x=p[0]
                # lookup is in rgb, convert to bgr
                colour = (colour[2], colour[1], colour[0],255)
                if invert:
                    slice_img = cv2.bitwise_not(slice_img)


                tmp_img = np.zeros(slice_img.shape,dtype=np.uint8)
                tmp_img[50:,x-offset:x+offset] = colour

                # # add history
                # history = row['history'].split(',')[:-1]
                # # offset = 5
                # count = 0
                # height = tmp_img.shape[0]
                # size = 20
                # for h in history:
                #     p1 = [int(round(x-size/2)), int(round(height - 5 - (size*count)))]  #+ np.array([-5 + (offset * count), 15])
                #     p2 = [int(round(x+size/2)), int(round(height - (5+size) - (size*count)))] # p + np.array([-10 + (offset * count), 20])
                #     last = 0
                #     if count + 1 >= len(history):
                #         # then last so use final value.
                #         last = int(row['final_intensity'])
                #     else:
                #         last = int(history[count + 1])
                #
                #     if (int(h) > last):
                #         # negatvie change - seen
                #         t_colour = (0, 255, 0)  # green did see
                #     elif (int(h) < last):
                #         t_colour = (0, 0, 255)  # red didnt see
                #     else:
                #         t_colour = (0, 255, 0)  # green did see
                #
                #     cv2.rectangle(tmp_img, tuple(p1), tuple(p2), t_colour, -1)
                #     cv2.rectangle(tmp_img, tuple(p1), tuple(p2), (1, 1, 1), 1)
                #     # cv2.putText(slice_img,
                #     #             str(h),
                #     #             tuple(int(round((x-size/2)), int(round(height - 10 - (size*count))))),
                #     #             cv2.FONT_HERSHEY_PLAIN, 0.6, (255, 255, 255), 1,
                #     #             cv2.LINE_AA)
                #     count += 1


                slice_img=cv2.addWeighted(slice_img,1.0,tmp_img,alpha,1.0)
                # slice_img[50:,x-30:x+30] = colour
                # draw  thresholds at the top of the image.

                cv2.putText(slice_img, str(t), tuple((x-5,40)), cv2.FONT_HERSHEY_PLAIN, 1, (255,255,255), 1, cv2.LINE_AA)
                # slice_img[:,p[0]-padd:p[0]+padd,1] += 50
        return slice_img


    def degToPixel(self):
        return round((self.localiser_width / self.localiser_angle))

    def drawMPGrid(self,mp_scan,reg=None,drawHistory=False,
                    drawIds=False,rotate=False,patient_data=False,
                    drawMovement=False,fontSize=1.0):
        """
        Overlay the MP grid onto the OCT IR localiser,
        scale the radius of the points to the goladman 3 stimulus diameter (0.43 deg) = 0.215 radius

        :param mp_scan:
        :param reg:
        :param drawHistory:
        :param drawIds:
        :param rotate:
        :param patient_data:
        :param drawMovement:
        :param fontSize:
        :return:
        """


        radius = int(self.degToPixel() * 0.215)

        out = mp_scan.data.drawGrid(image=self.getIRImage(), registration=reg, drawValues=True, \
                                 drawHistory=drawHistory, drawIds=drawIds, rotate=rotate, \
                                 drawMovement=drawMovement, radius=radius, fontSize=fontSize)

        if patient_data:
            cv2.putText(out, mp_scan.getID(), (40, 40), cv2.FONT_HERSHEY_PLAIN, 1.1, (255, 255, 255), 1, cv2.LINE_AA)

        return out