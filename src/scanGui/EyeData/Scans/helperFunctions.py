#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 11:33:34 2017

@author: iain

Helper functions for MP Processing

"""
import numpy as np
from . import distanceFunctions as df
import pandas as pd
import math

def createSingleScore(data,standard_eye,eyes):
    """
    Create a single score (for TSD) against standard Eye.
    """
    
    output = []
    
    for index,row in data.iterrows():
        #each row has two eyes, 
        pid = row['pid']
        eid1 = row['eid1']
        eid2 = row['eid2']
        
        eye = eyes[pid][eid1]        
        marker = "<"
        if row['type'] == "R":
            marker = ">"
        d = {
                "pid":int(pid),
                "eid":int(eid1),
                "mean_sensitivity":eye.avg,
                "tsd": df.tsd(standard_eye.thresholds(),eye.thresholds()),
                "sad": df.sad(standard_eye.thresholds(),eye.thresholds()),
                "ssd": df.ssd(standard_eye.thresholds(),eye.thresholds()),
                "sd": df.sd(standard_eye.thresholds(),eye.thresholds()),
                "type":row["type"],
                "marker":marker,
                "eye":row["eye"]
            }
        output.append(d)        
        
        eye = eyes[pid][eid2]
        d = {
                "pid":int(pid),
                "eid":int(eid1),
                "mean_sensitivity":eye.avg,
                "tsd": df.tsd(standard_eye.thresholds(),eye.thresholds()),
                "sad": df.sad(standard_eye.thresholds(),eye.thresholds()),
                "ssd": df.ssd(standard_eye.thresholds(),eye.thresholds()),
                "sd": df.sd(standard_eye.thresholds(),eye.thresholds()),
                "type":row["type"],
                "marker":marker,
                "eye":row["eye"]
            }
        output.append(d) 
        
    return pd.DataFrame.from_records(output)


def compare2Normal(data,eye,eyes):
    #global eyes
    #process all of the eyes and calculate tsd differnce to normal eye:
    new_data=[]
    for i in range(len(data)):
        pid = data.iloc[i]['pid']
        eid1 = data.iloc[i]['eid1']
        eid2 = data.iloc[i]['eid2']
        print("Comparing: %d %d-%d"%(pid,eid1,eid2))
        avg_t = np.mean((eyes[pid][eid1].thresholds(),eyes[pid][eid2].thresholds()),axis=0)
        diff_sad = df.sad(eye.thresholds(),avg_t)
        diff_ssd = df.ssd(eye.thresholds(),avg_t) 
        diff_tsd = df.tsd(eye.thresholds(),avg_t)
        diff_sd = df.sd(eye.thresholds(),avg_t)
        d = {"pid":int(pid),
             "eid1":int(eid1),
             "eid2":int(eid2),
             "diff_sad":diff_sad,
             "diff_ssd":diff_ssd,
             "diff_tsd":diff_tsd,
             "diff_sd":diff_sd}
        
        comp = eyes.compare(
                pid,
                eid1,
                eid2,
                offline=True,
                register=True,calcblur=True)
        #if type set
        ttype=data['type'].unique()[0]
        if ttype is not None:
            comp['type']=ttype
        d.update(comp)
        new_data.append(d)
    
    return pd.DataFrame.from_records(new_data)


def parse(t1,eye1='R T1',eye2='R T2',ttype=None,eyes=None):
    compared = []
    
    all_points = []#pd.DataFrame()
    

    for index, row in t1.iterrows():
        r_t1 = int(row[eye1])
        r_t2 = int(row[eye2])
        e1 = eyes[index][r_t1]
        e2 = eyes[index][r_t2]
        comp = eyes.compare(index,
                            r_t1,
                            r_t2,
                            offline=True,
                            register=True,calcblur=True)
        
        
        
        #applyting wyatt gradient comarisson, remembering to remove the trim.
        avg = np.mean((e1.asImg(),e2.asImg()),axis=0)   
        g = e1.gradient(avg,trim=2)
        diff =np.abs((e1.asImg() - e2.asImg()))[2:-2,2:-2]
        gradient_corr = compare(diff,g)
        
        
        #add gradient correlation to the comparison dict
        comp['gradient_corr']=gradient_corr
        
        #if type set
        if ttype is not None:
            comp['type']=ttype
        
        #flatten all Mats, so to extract and compare each point.
        avg = avg[2:-2,2:-2].flatten()
        g = g.flatten()
        diff = diff.flatten()
        t1 = e1.asImg()[2:-2,2:-2].flatten()
        t2 = e2.asImg()[2:-2,2:-2].flatten()
        
        if comp['reg diff'] is not None:
            #convert registration datat to a 2d Mat.
            regAsMat = reg2img(e1,comp['reg diff'][1:])
            reg = regAsMat[2:-2,2:-2].flatten()

            #for i in range(len(g)):
            #    grad_points.append({'diff':diff[i],'grad':g[i],'avg':avg[i],'reg':reg[i],'t1':t1[i],'t2':t2[i]})
        #else:
            #for i in range(len(g)):
            #    grad_points.append({'diff':diff[i],'grad':g[i],'avg':avg[i],'t1':t1[i],'t2':t2[i]})
        
        
        compared.append(comp)
        #compare PWS
        pws = eyes.pwComp(index,r_t1,r_t2,ttype=ttype)
        
        all_points.append(pws)
        #print all_points
                
    return pd.concat(all_points), pd.DataFrame.from_records(compared)



def compare(A,B):
    """
    array comparision. according to Wyatt et.al 2007
    
    Pab = Cov(A,B) / (stdA * stdB)

    Cov(A,B) = 1/n sum( (Aj - Amean)(Bj-Bmean) )
    
    """
     
    a = A - np.mean(A)
    b = B - np.mean(B)
    
    p = np.product( (a,b),axis=0)
    p /=p.size
    return np.sum(p) / (A.std() * B.std())



def reg2img(eye,reg):
    """
    convert registration data to 2d Matrix
    """
    size = (10,10)
    img=np.zeros(size)
    index=0
    for d in eye.data[1:]:
        x = (int(round(d['x_deg'])) +size[0])/2
        y = (size[1] - int(round(d['y_deg'])))/2
            
        if x+1>size[0] or y+1>size[0] or x<0 or y<0:
            continue
    
        img[y,x] = np.linalg.norm(reg[index])
        index+=1    
    return img



def createDifferenceMatrix(data, eyes,column='R T1'):
    """
    From the input data frame, create a difference matrix of the distances
    
    just normals so far
    
    """
    tmp = data[column].dropna()
    n = len(tmp)
    distMatrix = np.zeros((n,n))
    for i in range(n):
        e1 = eyes[tmp.keys()[i]][tmp.iloc[i]]
        for j in range(n):
            e2 = eyes[tmp.keys()[j]][tmp.iloc[j]]
            
            distMatrix[i][j] = df.ssd(e1.thresholds(),e2.thresholds())
        print("%d = %d:%d avg:%f BCEA_95:%f" %(i,tmp.keys()[i],tmp.iloc[i],e1.avg,e1.BCEA_95))
    return distMatrix

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)


