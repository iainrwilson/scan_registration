#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  3 11:55:37 2017

@author: iain

Distance caluclation functions

"""
import numpy as np

def cor(p1,p2):
    """
    Calculte coeficietn of reproducitibnliy:
        1.96 * sqrt(2) * Sd


    also, drop -1, and 0 values
    """
    
    diff=[]
    for i in range(len(p1)):
        if(p1[i]>0):
            diff.append( abs(p1[i]-p2[i]))

    return 1.96 * np.sqrt(2) * np.std(diff)

def corMs(p1,p2):
    """
    Calculte coeficietn of reproducitibnliy:
        1.96 * sqrt(2) * Sd


    also, drop -1, and 0 values
    """
    
    diff=[p1,p2]

    return 1.96 * np.sqrt(2) * np.std(diff)


def mpws(p1,p2):
    """
    mean point wise difference
    """
    diff=[]
    for i in range(len(p1)):
        diff.append( abs(p1[i]-p2[i]))

    return np.mean(diff),np.std(diff)
    


def tms(points):
    """
    Tri-Mean sensitivity
    
    tms = (Q1 + (2*Smedian) + Q3) /4
    
    Q1 = 25th percentile, 
    Q3 = 75th
    """
    
    s = np.median(points)
    q1 = np.percentile(points,25)
    q3 = np.percentile(points,75)   

    return (q1 + (2*s) + q3) /4

def tmsDiff(p1,p2):
    return tms(p1) - tms(p2)


def selectiveMean(p1,p2):
    """
        Ignore floor effet, remove points with <1
    """
    _p1=[]
    _p2=[]
    for i in range(len(p1)):
        if p1[i] >0:
            _p1.append(p1[i])
        if p2[i] >0:
            _p2.append(p2[i])
        
    return np.mean(_p1) - np.mean(_p2)

def thresholdDiff(p1,p2,thresh=4.1):
    """
    remove the differences below a certain threshold
    
    """
    sum=0
    for i in range(len(p1)):
        diff = (p1[i] - p2[i])
        if abs(diff) >= thresh:
             sum+= round(diff/thresh)
    return sum


def normalise(points):
    """
    In = (I-min) * ((newMax-newMin)/(max-min) + newMin
    
    newMax = 1.0
    newMin = 0.0
    """
    min = np.min(points)
    max = np.max(points)
    newMax = 1.0
    newMin = 0.0
    
    for i in range(len(points)):
        points[i] = (points[i]-min) * ((newMax-newMin)/(max-min))+newMin
    
    return points


def sad(p1,p2):
    sum = 0
    for i in range(len(p1)):
        sum += abs(p1[i] - p2[i])

    return sum
    
def tsad(p1,p2,thresh=4.1):
    sum=0
    for i in range(len(p1)):
        diff = abs(p1[i] - p2[i])
        if abs(diff) >= thresh:
             sum+= diff/thresh
    return sum

def stsad(p1,p2,thresh=4.1,scale=420):
    """ scaled thresholded sum of absolute differences """
    sum=0
    for i in range(len(p1)):
        diff = abs(p1[i] - p2[i])
        if abs(diff) >= thresh:
             sum+= diff/thresh
    return sum/scale

def tsd(p1,p2,thresh=4.1):
    sum=0
    for i in range(len(p1)):
        diff = p1[i] - p2[i]
        if abs(diff) >= thresh:
             sum+= diff/thresh
    return sum


def stsd(p1,p2,thresh=4.1,scale=460.93):
    """
    Scaled thresholded sum of differences
    """
    sum=0
    for i in range(len(p1)):
        diff = p1[i] - p2[i]
        if abs(diff) >= thresh:
             sum+= diff/thresh
    return sum/scale


def sd(p1,p2):
    sum=0
    for i in range(len(p1)):
        diff = p1[i] - p2[i]
        sum+= diff
    return sum

def averageDiff(p1,p2):
    """
        Difference of the two means
    """
    return np.average(p1) - np.average(p2)


def ssd(p1,p2):
    return sumofsquareDistances(p1,p2)

def sumofsquareDistances(p1,p2):
    """
        Sun of squared differences
    """
    sum = 0
    for i in range(len(p1)):
        sum += abs(p1[i] - p2[i])**2

    return sum

def hammingDistance(p1,p2):
    sum = 0
    for i in range(len(p1)):
        if p1[i] != p2[i]:
            sum+=1
    return sum


def signedHammingDistance(p1,p2):
    h = hammingDistance(p1,p2)
    s = simpleSum(p1,p2)
    if s < 0:
        h*=-1
    return h

def modifiedHamming(p1,p2):
    sum = 0
    thresh=50
    for i in range(len(p1)):
        tmp = abs(p1[i] - p2[i])
        if (tmp-thresh) > 0:
            sum+=1
    return sum


def simpleSum(p1,p2):
    s = 0
    for i in range(len(p1)):
        s += (p1[i]-p2[i])
    return s

def signedDiff(p1,p2):
    """
        Tries to keep a sign. Uses SSD, but the applies a sign.
    """
    ssd = sumofsquareDistances(p1,p2)
    s = simpleSum(p1,p2)
    if s < 0:
        ssd*=-1
    return ssd


def absoluteDiff(p1,p2):
    """
        assumes if 0 or < 0 set to 0 else set to 1
        then do hamming
    """
    
    thresh = 0.0
    sum=0
    for i in range(len(p1)):
        _p1 = p1[i]
        _p2 = p2[i]
        if _p1 > thresh:
            _p1 = 1
        else:
            _p1 = 0
                
        if _p2 > thresh:
            _p2 = 1
        else:
            _p2 = 0

        if _p1 != _p2:
            sum+=1
        

    return sum

