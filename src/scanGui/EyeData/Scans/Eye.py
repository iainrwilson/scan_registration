"""

Class that describes and eye.


"""
import os
import cv2
import numpy as np
from .rgc_density import retina

class Image:

    def _load(self,filename,mode=0):
        """ Load an image """
        if not os.path.exists(filename):
            print("%f not found" %(filename))
            return False

        img = cv2.imread(filename,mode)
        if img is None:
            print("Failed opening %f" % (filename))
            return None
        return img

    def addMask(self,mask):
        """ if not usign autoSegment, use this."""

        if len(mask.shape) == 3:
            #is 3 channel, convert to 1
            mask = cv2.cvtColor(mask,cv2.COLOR_BGR2GRAY)
        if mask.shape != self.image.shape:
            print("Error, mask is not the same shape as the image!")
        else:
            self.mask = mask

    def setFovea(self,loc):
        """set fundus location in pixels"""
        self.fovea = np.array(loc)

    def setOnh(self, loc):
        """set optic nerve head centre in pixels"""
        self.onh = np.array(loc)

    def setFoveaOnhDistance(self, distance):
        """  Set the distance in um from the fovea to ONH """
        self.fovea2onh = distance

    def scale(self,factor):
        """ scale image, fovea and onh lox. and the roi if set. """

        self.onh = self.onh * factor
        self.fovea = self.fovea * factor
        self.image = cv2.resize(self.image,None,
                              fx=factor,
                              fy=factor,
                              interpolation=cv2.INTER_AREA)
        self.fovea2onh *= factor

        if hasattr(self,'mask'):
            self.mask = cv2.resize(self.mask, None,
                                    fx=factor,
                                    fy=factor,
                                    interpolation=cv2.INTER_AREA)

        for i in range(len(self.roi)):
            _roi = self.roi[i] * float(factor)
            self.roi[i] = _roi.astype(np.uint)


class AF(Image):

    def load(self,filename):
        self.image = Image._load(self,filename)
        self.roi = []


    def addROI(self,box):
        """
        add reigion of interest for automatic segmentation

        0 = x, 1 = y, 2 = w, 3 = h
        """
        self.roi.append( np.array([box[0],
                           box[1],
                           box[2]-box[0],
                           box[3]-box[1]]))



    def autoSegment(self):
        """ run the auto segmentation """

        self.mask = np.zeros(self.image.shape,np.uint8)

        kernel = np.ones((5, 5), np.uint8)
        for roi in self.roi:
            roi_img = self.image[roi[1]:roi[1]+roi[3],roi[0]:roi[0]+roi[2]]
            #clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(5, 5))
            #roi_img = clahe.apply(roi_img)
            blur = cv2.GaussianBlur(roi_img,(5,5),0)
            ret, thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            thresh = cv2.dilate(thresh, kernel)
            thresh = cv2.erode(thresh, kernel)
            self.mask[roi[1]:roi[1]+roi[3],roi[0]:roi[0]+roi[2 ]] = thresh

    def um2px(self):
        """ return the pixel to um (using the default 4000um (after scaling.) """
        px_fovea2onh = np.linalg.norm(self.onh - self.fovea)
        return 4000./px_fovea2onh

class Eye:

    def __init__(self,side="right"):
        self.side = side

    def loadAF(self, image,fundus=None,onh=None):
        """ load autoflorescence image"""
        self.af = AF()
        self.af.load(image)
        self.af.setFovea([233,268])
        self.af.setOnh([383,246])


    def createRodDensityMap(self):
        """  Using the current eye AF image. """

        #First scale the AF image.
        #get af image fovea2onh
        modelFovea2Onh = 4000.

        scale = modelFovea2Onh / self.af.fovea2onh
        print("Scaling image: %f"%scale)
        self.af.scale(scale)

        self.rod_image = np.zeros(self.af.image.shape, np.uint16)

        ret = retina()


        # max rod value.
        max_val = 156649
        um2px = self.af.um2px()
        for x in range(self.af.image.shape[0]):
            for y in range(self.af.image.shape[1]):
                mm = (np.array([x, y]) - self.af.fovea) * (um2px / 1000.)
                # if mm[0] <0 or mm[1]<0:
                #    continue
                d = ret.rodDensity_mm(-mm[0], -mm[1])
                if np.isnan(d):
                    d = 0
                _d = d / max_val
                self.rod_image[y, x] = _d * ((2 ** 16) - 1)


if __name__ == "__main__":

    home = os.path.expanduser("~")
    data_dir = os.path.join(home, "experiments/microperimetry/af_data")

    eye = Eye()
    eye.loadAF(os.path.join(data_dir,"TW87.tif"))
    eye.af.setFoveaOnhDistance(5016)



    #eye.af.addROI([9,20,268,444])
    eye.af.addROI([75,125,264,361])
    eye.af.addROI([139,19,210,125])
    eye.af.addROI([7,286,40,320])


    eye.createRodDensityMap()
    eye.af.autoSegment()


    #model fovea 2 onh distance = 4mm
    mask_16 = np.array(eye.af.mask, dtype=np.uint16)
    mask_16 *= 256
    out = cv2.bitwise_and(eye.rod_image, mask_16)

    # out is the density, each pixel value is the rods per mm2
    #
    rod_count = np.sum(out * (eye.af.um2px()/1000.))

    print("Rod Count: %d" %rod_count)

    cv2.imwrite('mask.tif',eye.af.mask)
    cv2.imwrite('rods.tif',eye.rod_image)