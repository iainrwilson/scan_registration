from .Scan import Scan
import cv2,os

class Colour(Scan):

    mode = "COLOUR"

    def __init__(self,date=None,pid=None,visit=None,eye=None):
        Scan.__init__(self,date=date, pid=pid, visit=visit, eye=eye)

    def getID(self):
        """
         create a unique id for this scan
        mode_pid_eye_visit
        """
        return "%s_%s_%s_%s_%s" % (self.mode, self.pid, self.visit, self.eye, self.eid)

    def getIRImage(self):
        return self.image

    def load(self,fname):
        """ check exists """
        if not os.path.exists(fname):
            raise Exception("[Scan::MP::load()] file %s does not exit" % fname)

        # print "[Scan::MP::Load()] Loading %s" %(os.path.basename(fname))

        self.fname = fname
        self.image = cv2.imread(fname, -1)