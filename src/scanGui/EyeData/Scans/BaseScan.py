import numpy as np
import cv2

class BaseScan:

    """
    Baseclass for all modalities

    A scan is the whole data set.
    eg. OCT is all slices for one eye, at one time point

    """

    def __init__(self,mode,date=None,pid=None,visit=None,eye=None,person=None,preload=False):
        self.mode = mode
        self.date = date
        self.pid = pid
        self.visit = visit
        self.eye = eye
        self.data = {}
        self.ir_image = None
        self.person = person
        self.fov = None
        self.eid = None
        self.preload = preload
        self.ir_image_fname = None

        self.localiser_focus = 0
        self.localiser_sensor_gain = 0
        self.localiser_num_ave = 1


    def load(self):
        pass


    def __repr__(self):
        return self.getID()

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, BaseScan):
            return self.getID() == other.getID()

            # return self.mode == other.mode \
            #        and self.visit == other.visit \
            #        and self.eye == other.eye \
            #        and self.pid == other.pid \
            #        and self.date == other.date \
            #        and self.fov == other.fov \
            #        and self.eid == other.eid

        return False

    def getIRImage(self):
        if self.ir_image is None:
            self.ir_image =cv2.imread(self.ir_image_fname)
        return self.ir_image

    def setIRImage(self,fname):
        self.ir_image_fname = fname
        if self.preload:
            self.ir_image = cv2.imread(self.ir_image_fname)

    def getID(self):
        """
         create a unique id for this scan
        mode_pid_eye_visit
        """
        return "%s_%s_%s_%s" % (self.mode, self.pid, self.visit, self.eye)

    def registrations(self):
        """
        Return all registrations associated with this scan

        :return:
        """

        return self.person.registrations.getSources(self) + self.person.registrations.getTargets(self)


    def getRegistration(self, target):
        """
        Find and return the registration for the given target scan
        :param target:
        :return:
        """
        return self.person.registrations.get(self,target)


    def hasRegistration(self, target):
        """
        check if a registration exists for a given target scan
        :param target:
        :return:
        """
        return self.person.registrations.get(self, target) is not None

    def getTransformedIRImage(self,target):
        """
        Apply transformation from the registration to ir image.

        :param registration:
        :return:
        """

        reg = self.getRegistration(target)
        return cv2.imread(reg.src,-1)

    def transformImage(self,img,reg):
        """
        Testing the transformations

        :param img:
        :param reg:
        :return:
        """
        points = []
        for y in range(img.shape[0]):
            for x in range(img.shape[1]):
                points.append( self.applyTransformationToPoint((y,x),reg) )

        return points

    def applyTransformationToPoint(self,point,reg):
        """
        given the registration output, apply this to the points.
        e.g. eye1 -> eye2 ;  eye1 x T = eye2
        apply eye1.points x T  = eye2.points


        QUADRATIC:
        P = [^p1,^p2].T = [p1,p2]-[c1,c2]
        [qx,qy] = B * [^px**,^py**,^px^py].T + A*P+T

        """

        if reg.type == "QUADRATIC":
            P = np.matrix([point[0], point[1]]).T - reg.C
            dP = np.matrix([P[0, 0] ** 2, P[1, 0] ** 2, P[0, 0] * P[1, 0]]).T
            R = reg.B * dP + reg.A * P + reg.T
            return [R.item(0), R.item(1)]

        return None

    def degToPixel(self):
        return 1.0


#
# class ScanRegistrations:
#     """
#     Class to deal with the scan-registration many 2 many relationship.
#
#     """
#
#     def __init__(self):
#         self.data = []
#
#     def __repr__(self):
#         txt=""
#         for reg in self.data:
#             txt += reg.__repr__()
#             txt+="\n"
#         return txt
#
#     def __getitem__(self, item):
#         return self.data[item]
#
#     def add(self, registration):
#         """
#         Add a registration, skip is already exists
#         :param registration:
#         :return:
#         """
#         if registration in self.data:
#             return
#         self.data.append(registration)
#
#
#     def get(self, source, target):
#         """
#         return the registration associated with the scan pair
#
#         true if either the source or target match
#         :param other:
#         :return:
#         """
#         for reg in self.data:
#             if reg.matches(source,target):
#                 return reg
#         return None
#
#     def getTargets(self,source):
#         """
#         Get all registrations that have the same source,
#         will be a list of different targets
#         :param source:
#         :return:
#         """
#         targets = []
#         for reg in self.data:
#             if reg.source == source:
#                 targets.append(reg)
#         return targets
#
#     def getSources(self, target):
#         """
#         Get a list of all registrations with the same target
#         will be alist of different sources
#         :param target:
#         :return:
#         """
#         sources = []
#         for reg in self.data:
#             if reg.target == target:
#                 sources.append(reg)
#         return sources
