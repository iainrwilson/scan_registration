from .Scan import Scan
from .MPData import MPData
import os,cv2,glob
import pandas as pd
import numpy as np

class MP(Scan):

    mode = "MP"

    def __init__(self,date=None,pid=None,visit=None,eye=None):
        super(MP,self).__init__(date=date, pid=pid, visit=visit, eye=eye)

        """ Load the lookup table. """
        lookup_name = 'MAIA_lookup.csv'

        self.mp_data_dir = "/Users/iain/Documents/MAIA/"
        dir = os.path.join(os.path.dirname(__file__),'resources')
        self.lookup = pd.DataFrame.from_csv(os.path.join(dir,lookup_name))

        #used to override getID funcition
        self.getid = None

        self._data = None

        self.rotate_ir = True

    def getIRImage(self):
        if self.ir_image is None:
            if self.ir_image_fname is None:
                self.ir_image_fname = self.data.getStillImage()
        if self.rotate_ir:
            return cv2.rotate(Scan.getIRImage(self), cv2.ROTATE_90_COUNTERCLOCKWISE)
        return Scan.getIRImage(self)

    def getID(self):
        """
         create a unique id for this scan
        mode_pid_eye_visit
        """
        if self.getid is not None:
            return self.getid()

        if self.visit is None:
            return "%s_%s_%s_%s" % (self.mode, self.pid, self.eye, self.eid)

        return "%s_%s_%s_%s_%s" % (self.mode, self.visit, self.eye, self.pid, self.eid)

    def getOldID(self):
        return "%s_%s"%(self.pid,self.eid)

    def load(self, fname):
        """ check exists """
        if not os.path.exists(fname):
            raise Exception("[Scan::MP::load()] file %s does not exit" % fname)

        # print "[Scan::MP::Load()] Loading %s" %(os.path.basename(fname))

        self.setIRImage(fname)
        self.loadMPData()

    def getData(self):
        # print("Get Data")
        if not self._data.loaded:
            print("loading: ",self._data.patient_id,self._data.exam_id)
            self._data.loadXML()
            self.eye = "OS"
            if self._data.eye == "Right":
                self.eye = "OD"

            self.setIRImage(self._data.getStillImage())
        return self._data

    def setData(self,data):
        # print("set Data")
        self._data = data

    data = property(getData,setData)

    def loadFromID(self,pid,eid,mp_data_dir=None,preload=False):
        """ Load from XML using only pid add eid"""
        self._data = MPData(datadir=mp_data_dir)
        self._data.patient_id = pid
        self._data.exam_id = eid

        self.pid = pid
        self.eid = eid
        self.eye = "OD"


        if not self._data.loadXML():
            return False

        self.eye = "OS"
        if self._data.eye == "Right":
            self.eye="OD"

        image = self.data.getStillImage()
        self.setIRImage(self.data.getStillImage())

        self.datetime = self._data.exam_date

        self.localiser_width = 1024
        self.localiser_height = 1024
        self.localiser_angle = self._data.fov

        self.scale_x = self.localiser_angle / self.localiser_width
        self.scale_y = self.localiser_angle / self.localiser_height

        # img = cv2.imread(self.data.getStillImage())
        # img = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
        # self.ir_image = img
        return True

    def loadFromMPDataFile(self, fname,mp_data_dir=None):
        """
        Create an instance with only an MPData Object
        :param fname:
        :return:
        """
        if not os.path.exists(fname):
            raise Exception("[Scan::MP::loadFromMPData] File %s does not exist"%fname)

        if mp_data_dir is None:
            mp_data_dir=self.mp_data_dir

        self.loadFromMPData(MPData(threshold_filename=fname,datadir=mp_data_dir))
        if self._data.eye == "Right":
            self.eye = "OD"
        else:
            self.eye = "OS"
        self.date = self._data.exam_date
        img = cv2.imread(self._data.getStillImage())
        img = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
        self.ir_image = img

        # try loading fixation
        self.data.findFixation()

        self.data.calcEyeMovements()

    def loadFromMPData(self,data):
        self._data = data
        self.pid = self._data.patient_id
        self.eid = self._data.exam_id
        if self.data.eye == "Right":
            self.eye = "OD"
        else:
            self.eye = "OS"
        self.date = self._data.exam_date

        img = cv2.imread(self._data.getStillImage())
        img = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
        self.ir_image = img

        #try loading fixation
        self.data.findFixation()

        self.data.calcEyeMovements()

    def loadMPData(self):
        """
        Load the full MP data, using the
        :param fname:
        :return:
        """

        self.mp_pid = self.ir_image_fname[:-4].split('_')[-1]
        dir = os.path.dirname(self.ir_image_fname)

        files = glob.glob(os.path.join(dir,"*%s_threshold.txt"%(self.mp_pid)))
        if len(files)==0:
            print("[Scan::MP::loadMPData()] cannot find any MP data files")
            return

        # there should only be one match.
        t_fname = files[0]

        self._data = MPData(threshold_filename=t_fname,datadir=self.mp_data_dir)
        # pid,eid = self.fixIds(self.data.patient_id,self.data.exam_id,self.lookup)


    def fixIds(self,pid, eid,lookup=None):
        # convert old to new PID and EIDs
        if lookup is None:
            lookup = self.lookup
        l_df = lookup

        e = l_df[(l_df['old_pid'] == pid) & (l_df['old_eid'] == eid)].to_dict('records')[0]

        return e['new_pid'], e['new_eid']

    def fixEid(self, eid, lookup=None):
        # convert old to new PID and EIDs
        if lookup is None:
            lookup = self.lookup
        l_df = lookup

        e = l_df[(l_df['old_eid'] == eid)].to_dict('records')[0]

        return e['new_eid']

    def drawStimuli(self,image=None,reg=None,drawHistory=False,
                    drawIds=False,rotate=False,patient_data=False,
                    drawMovement=False,radius=5,fontSize=1.0,alpha=1.0):
        """
        Draw stimuli onto the given image or the MP IR image
        with optional registration

        :param image:
        :param reg:
        :param text:
        :return:
        """
        if image is None:
            image = self.getIRImage()

        print("draw Stimuli", reg)

        out = self.data.drawGrid(image=image, registration=reg, drawValues=True,\
                                 drawHistory=drawHistory,drawIds=drawIds,rotate=rotate,\
                                 drawMovement=drawMovement,radius=radius,fontSize=fontSize,alpha=alpha)

        if patient_data:
            cv2.putText(out, self.getID(),(40,40), cv2.FONT_HERSHEY_PLAIN, 1.1, (255,255,255), 1, cv2.LINE_AA)


        return out

    def __sub__(self, other):
        """
        Subtrace two scans - only use the MPData

        return a new scan onject
        :param other:
        :return:
        """

        # make suure IDs are aligned

        self.data.dataframe.sort_values(by="ID",inplace=True)
        other.data.dataframe.sort_values(by="ID",inplace=True)

        diff = self.data.dataframe['Threshold'] - other.data.dataframe['Threshold']

        diff_scan = Scan.create(self.mode)
        diff_scan.loadFromMPData(self.data)

        diff_scan.data.dataframe['Threshold'] = diff

        return diff_scan
        #create a new scan object from an MP object



    @staticmethod
    def compare(reg):
        """
        Do all of the comparisions. return a dataframe (created from the source)
        """
        if reg.source.mode != "MP" or reg.target.mode != "MP":
            raise Exception("[MP::Scan::compare()] must be a MP to MP registraton")

        reg.source.data.dataframe.sort_values(by="ID", inplace=True)
        reg.target.data.dataframe.sort_values(by="ID", inplace=True)
        source_df = reg.source.data.dataframe
        target_df = reg.target.data.dataframe
        columns = ['Threshold', 'history', 'ID','movement','mean_movement']

        df_1 = source_df[columns]
        df_2 = target_df[columns].rename(columns={"Threshold": "Threshold_2",
                                                    "history": "history_2",
                                                  "movement":"movement_2",
                                                  'mean_movement':'mean_movement_2'
                                                  })

        diff_df = df_1.set_index('ID').join(df_2.set_index("ID"))
        diff_df["Threshold_diff"] = diff_df["Threshold"] - diff_df["Threshold_2"]

        diff_df = diff_df.join(MP.calcRegistrationError(reg).set_index('ID'))
        diff_df.reset_index(inplace=True)

        #include eid and pid
        diff_df['pid'] = reg.source.data.patient_id
        diff_df['eid_1'] = reg.source.data.exam_id
        diff_df['eid_2'] = reg.target.data.exam_id


        return diff_df

    @staticmethod
    def calcRegistrationError(reg):
        """
        Compare scans, in a longitudinal way.
        TSD etc

        a registration file contains both source and dest

        :param scan:
        :return:
        """

        if reg.source.mode != "MP" or reg.target.mode != "MP":
            raise Exception("[MP::Scan::compare()] must be a MP to MP registraton")

        reg.source.data.dataframe.sort_values(by="ID", inplace=True)
        reg.target.data.dataframe.sort_values(by="ID", inplace=True)

        # make sure degToPixels has been run
        reg.source.data.degToPixels()
        reg.target.data.degToPixels()

        source_df = reg.source.data.dataframe
        target_df = reg.target.data.dataframe

        # ignore ONH
        source_df = source_df[source_df["ID"]!=0]
        target_df = target_df[target_df["ID"]!=0]

        points=[]
        # for i in range(2, len(source_df)+2):
        for i in source_df.ID.unique():
            s_p = source_df[source_df['ID'] == i]['points'].tolist()[0]
            t_p = target_df[target_df['ID'] == i]['points'].tolist()[0]

            s_p = reg.source.data.transformPoint(s_p, reg)
            d = { "ID":i,
                "registration_error": np.array(s_p) - np.array(t_p),
                "registration_error_magnitude":np.linalg.norm(np.array(s_p) - np.array(t_p))
                }
            points.append(d)
        return pd.DataFrame.from_records(points)

    def getHistory(self):
        """
        Generate a list of all stimulation points,
        including eyemovements and repsonse
        :return:
        """
        all_hist = []

        move, o, e = self.data.calcEyeMovements()
        for i, row in self.data.dataframe.iterrows():

            # for each loci, add the value and response,ignore 0
            if row.ID == 0:
                continue

            count = 0
            history = row['history'].split(',')[:-1]
            for h in history:
                if count + 1 >= len(history):
                    # then last so use final value.
                    last = int(row['final_intensity'])
                else:
                    last = int(history[count + 1])

                # sometimes the history doesnt match the fixation data.
                m_list = move[move.ID == row.ID].movement.tolist()[0]
                if count < len(m_list):
                    movement = m_list[count]
                else:
                    print("History fixation missmatch: [%d::%d] #%d" % (self.data.patient_id, self.data.exam_id, row.ID))
                    movement = None
                d = {
                    "value": int(h),
                    "ID": row.ID,
                    "pid": self.data.patient_id,
                    "scan": self.getID(),
                    "movement": movement
                }

                if (int(h) > last):
                    # negatvie change - seen
                    d['response'] = True
                elif (int(h) < last):
                    # did not see
                    d['response'] = False
                else:
                    # did see
                    d['response'] = True
                all_hist.append(d)
                count += 1

        return pd.DataFrame.from_records(all_hist)
