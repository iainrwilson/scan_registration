from .Scan import Scan
import xml.etree.ElementTree as ET
import os,cv2
import datetime

class SWAF(Scan):

    mode = "SWAF"

    def __init__(self,date=None,pid=None,visit=None,eye=None,preload=False):
        Scan.__init__(self, date=date, pid=pid, visit=visit, eye=eye,preload=preload)

    def getID(self):
        return "%s_%d_%s_%s_%s"%(self.mode,self.localiser_angle,self.pid,self.datetime.isoformat(),self.eye)

    def load(self,fname):
        """ check exists """
        if not os.path.exists(fname):
            raise Exception("[Scan::FAF::load()] file %s does not exit" % fname)

        # print "[Scan::SWAF::Load()] Loading %s" %(os.path.basename(fname))

        self.ir_image = cv2.imread(fname,-1)

    def loadFromXML(self,xml):
        self.xml_tree = ET.parse(xml)
        root = self.xml_tree.getroot()
        body = root.find("BODY")
        self.data_dir = os.path.dirname(xml)

        self.pid = body.find("Patient").find("LastName").text
        series = root.find("BODY").find("Patient").find("Study").find("Series")
        self.loadSeries(series)
        # self.visit = os.path.basename(xml)[:-4]

        self.xml_mode = True

    def loadImage(self,image):
        """
        Load an AF scan
        :param series:
        :return:
        """

        self.xml_image = image

        if not isinstance(image, ET.Element):
            raise Exception("Not an xml element")
        # print("\tLoading AF: %s " % (self.pid))
        self.eye = "OS"
        if image.find("Laterality").text == "R":
            self.eye = "OD"

        type = image.find("ImageType").find("Type").text

        if type == "ANGIO":
            # save as IR image
            f = image.find("ImageData").find("ExamURL").text
            ir_fname = f[10:].split("\\")[-1]
            ir_fname = os.path.join(self.data_dir, ir_fname)
            if not os.path.exists(ir_fname):
                raise Exception("[OCT::Scan::loadFromXML] %s file not found" % ir_fname)

            time = image.find("AcquisitionTime").find("Time")
            seconds = int(time.find("Second").text.split(".")[0])
            milliseconds = int(time.find("Second").text.split(".")[1])

            self.datetime = datetime.datetime(self.date.year,
                                              self.date.month,
                                              self.date.day,
                                              int(time.find("Hour").text),
                                              int(time.find("Minute").text),
                                              seconds,
                                              milliseconds
                                              )


            self.setIRImage(ir_fname)
            # self.ir_image = cv2.imread(ir_fname,-1)
            self.scale_x = float(image.find("OphthalmicAcquisitionContext").find("ScaleX").text)
            self.scale_y = float(image.find("OphthalmicAcquisitionContext").find("ScaleY").text)
            self.localiser_width = float(image.find("OphthalmicAcquisitionContext").find("Width").text)
            self.localiser_height = float(image.find("OphthalmicAcquisitionContext").find("Height").text)

            self.localiser_angle = float(image.find("OphthalmicAcquisitionContext").find("Angle").text)
            self.localiser_focus = float(image.find("OphthalmicAcquisitionContext").find("Focus").text)

            self.localiser_autosensorgain = (image.find("OphthalmicAcquisitionContext").find("AutoSensorGain").text) == "true"
            self.localiser_sensor_gain = float(image.find("OphthalmicAcquisitionContext").find("SensorGain").text)
            self.localiser_num_ave = int(image.find("OphthalmicAcquisitionContext").find("NumAve").text)
            # self.localiser_fixation_target = float(image.find("OphthalmicAcquisitionContext").find("FixationTarget").text)

        else:
            print("[SWAF] Type not matched:: %s"%type)
