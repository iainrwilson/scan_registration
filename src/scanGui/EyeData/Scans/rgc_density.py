"""

An implementation of the models from A. Watson (2014) paper;
"A formula for human retinal ganglion cell receptive field density as a function of visual field location"


This is a class that should generate densities for a given retinal location

"""
import math
import os
import pandas as pd
import numpy as np


class retina:

    def __init__(self):
        """
        Set the constants used
        """
        self.constants={}
        self.constants['temporal'] = {
            "k" :1,
            "a" :0.9851,
            "r2":1.058,
            "re":22.14,
            "rz":11
        }
        self.constants['superior'] = {
            "k" :2,
            "a" :0.9935,
            "r2":1.035,
            "re":16.35,
            "rz": 17
        }

        self.constants['nasal'] = {
            "k" :3,
            "a" :0.9729,
            "r2":1.084,
            "re":7.633,
            "rz": 17
        }
        self.constants['inferior'] = {
            "k" :4,
            "a" :0.996,
            "r2":0.9932,
            "re":12.13,
            "rz": 17
        }

        self.dc0 = 14804.6
        self.rm = 41.03
        self.loadDensityData()

        self.interpolate_rods()

    def loadDensityData(self, cones=None, rods=None):
        """
        Load data from csv files.
        :return:
        """

        home = os.path.expanduser("~")
        self.data_dir = os.path.join(home, "experiments/microperimetry/density_data")

        if cones is None:
            self.cone_filename = os.path.join(self.data_dir,"cones_per_sq_mm.csv")
        else:
            self.cone_filename = cones

        if rods is None:
            self.rod_filename = os.path.join(self.data_dir, "rods_per_sq_mm.csv")
        else:
            self.rod_filename = rods


        self.cones = pd.read_csv(self.cone_filename)
        self.rods = pd.read_csv(self.rod_filename)

    @staticmethod
    def mm2deg(mm):
        """
        Appendix 6, conversion of eccentricitied in mm to deg
        :param mm:
        :return:
        """
        return (3.556 * mm) + (0.05993 * (mm**2)) - (0.007358 *(mm**3)) + (0.0003027*(mm**4))

    @staticmethod
    def deg2mm(deg):
        """
        Appendix 6, conversion of eccentricities in deg to mm
        :param deg:
        :return:
        """
        return (0.268*deg) + (0.0003427*(deg**2)) - ((8.3309e-6)*(deg**3))

    @staticmethod
    def mm2Todeg2(mm2,deg):
        """
        Appendix 6 mm^2 to deg^2

        slightly odd,  the sub-function ration gives the ratio of mm2 to deg2 at a given
        eccentricity in deg.
        :return:
        """

        def ratio(r):
            return 0.0752 + 5.846e-5*r - 1.064e-5*r**2 + 4.116e-8*r**3


        return ratio(deg) * mm2

    def rgcDensity(self,r,meridian):
        """
        Retinal Ganglion cell density

        :param r:
        :param meridian:
        :return:
        """
        ak = self.constants[meridian]['a']
        r2 = self.constants[meridian]['r2']
        re = self.constants[meridian]['re']

        return ak * ((1 + (r / r2))**-2) + (1 - ak) * math.exp(-(r / re))



    def mRGCfDensity(self,r, meridian):
        """
        midget retinal ganglion cell receptive field density as a function of eccentricity

        equation 8

        590.2395168149003


        :param r: eccentricity in degrees
        :return:
        """

        return (((2.0 * self.dc0) * (1+(r/self.rm))**-1)) * self.rgcDensity(r=r,meridian=meridian)


    def mRGCf(self,x,y):
        """
        midget retinal ganglion cell receptive field density at arbritaty points
        :param x:
        :param y:
        :return:
        """
        r_xy = math.sqrt(x ** 2 + y ** 2)

        # check the meridian, if positive x = temporal, -ve nasal
        x_meridian = 'temporal'
        if (x > 0):
            x_meridian = 'temporal'
        else:
            x_meridian = 'nasal'
        if (y > 0):
            y_meridian = 'superior'
        else:
            y_meridian = 'inferior'

        s_1 = self.mRGCfDensity(r_xy, x_meridian)
        s_2 = self.mRGCfDensity(r_xy, y_meridian)

        if r_xy == 0:
            return s_1
        return (1/r_xy) * math.sqrt( (x**2 * s_1**2) + (y**2*s_2**2))

    def coneDensity(self,x,y):
        """
        Get the cone density at a certin x,y pos
        :return:  square mm
        """
        from scipy.interpolate import interp1d

        #firt convert from mm2 to deg2

        deg = self.cones['deg'].tolist()
        _temporal = self.cones['temporal'].tolist()
        _nasal = self.cones['nasal'].tolist()
        _superior = self.cones['superior'].tolist()
        _inferior = self.cones['inferior'].tolist()

        _nan  = np.zeros(len(deg))
        _nan[:] = np.nan
        temporal = _nan
        nasal = _nan
        superior = _nan
        inferior = _nan


        for i in range(len(deg)):
            temporal[i] = self.mm2Todeg2(_temporal[i],deg[i])
            nasal[i] = self.mm2Todeg2(_nasal[i], deg[i])
            superior[i] = self.mm2Todeg2(_superior[i], deg[i])
            inferior[i] = self.mm2Todeg2(_inferior[i], deg[i])

        temporal = interp1d(deg,temporal)
        nasal = interp1d(deg, nasal)
        superior = interp1d(deg, superior)
        inferior = interp1d(deg, inferior)


        r_xy = math.sqrt(x**2 + y**2)

        #check the meridian, if positive x = temporal, -ve nasal
        x_meridian = 'temporal'
        if(x>0):
            x_meridian = temporal
        else:
            x_meridian = nasal
        if(y>0):
            y_meridian = superior
        else:
            y_meridian = inferior

        s_1 = x_meridian(r_xy)
        s_2 = y_meridian(r_xy)

        if r_xy == 0:
            return s_1.item()
        return (1/r_xy) * math.sqrt( (x**2 * s_1**2) + (y**2*s_2**2))

    def rodDensity(self, x, y):
        """
        Get the rod density at a certain x,y pos
        :return:
        """
        from scipy.interpolate import interp1d

        deg = self.rods['deg'].tolist()
        temporal = interp1d(deg, self.rods['temporal'].tolist())
        nasal = interp1d(deg, self.rods['nasal'].tolist())
        superior = interp1d(deg, self.rods['superior'].tolist())
        inferior = interp1d(deg, self.rods['inferior'].tolist())

        r_xy = math.sqrt(x ** 2 + y ** 2)

        # check the meridian, if positive x = temporal, -ve nasal
        x_meridian = None
        if (x > 0):
            x_meridian = temporal
        else:
            x_meridian = nasal
        if (y > 0):
            y_meridian = superior
        else:
            y_meridian = inferior

        s_1 = x_meridian(r_xy)
        s_2 = y_meridian(r_xy)

        if r_xy == 0:
            return s_1.item()

        return (1 / r_xy) * math.sqrt((x ** 2 * s_1 ** 2) + (y ** 2 * s_2 ** 2))


    def interpolate_rods(self):
        from scipy.interpolate import interp1d

        deg = self.rods['mm'].tolist()
        self.r_temporal = interp1d(deg, self.rods['temporal'].tolist())
        self.r_nasal = interp1d(deg, self.rods['nasal'].tolist())
        self.r_superior = interp1d(deg, self.rods['superior'].tolist())
        self.r_inferior = interp1d(deg, self.rods['inferior'].tolist())


    def rodDensity_mm(self, x, y):
        """
        Get the rod density at a certain x,y pos (mm)
        returns rods / mm^2
        :return:
        """

        r_xy = math.sqrt(x ** 2 + y ** 2)

        # check the meridian, if positive x = temporal, -ve nasal
        x_meridian = 'temporal'
        if (x >= 0):
            x_meridian = self.r_temporal
        else:
            x_meridian = self.r_nasal
        if (y > 0):
            y_meridian = self.r_superior
        else:
            y_meridian = self.r_inferior

        s_1 = x_meridian(r_xy)
        s_2 = y_meridian(r_xy)

        if r_xy == 0:
            return s_1.item()

       # if np.isnan(s_1.item()):
       #     return s_2.item()

        return (1 / r_xy) * math.sqrt((x ** 2 * s_1 ** 2) + (y ** 2 * s_2 ** 2))


if __name__ == "__main__":
    from matplotlib import pyplot as plt
    import seaborn as sns

    e = retina()
    e.loadDensityData()




