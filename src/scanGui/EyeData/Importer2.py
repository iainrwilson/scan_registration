import os
from .Importer import Importer
from . import Scans
import xml.etree.ElementTree as ET
import datetime

class Importer2(Importer):
    """

    Another importer, used for the MP / OCT data from the FHS project

    """

    def __init__(self, mp_data_folder=None, mp_fixation_dir=None):
        Importer.__init__(self)
        self.mp_data_folder = mp_data_folder
        self.mp_fixation_dir = mp_fixation_dir


    def load(self,dir):
        """
        find xml files and load oct.
        :param dir:
        :return:
        """

        import glob
        files = glob.glob(dir+"/**/*.xml",recursive=True)

        for file in files:
            self.loadXML(file)

    def loadXML(self,file,preload=False):
        """
        Heidleberg xml files contain modaility details.

        :param file:
        :return:
        """
        if not os.path.exists(file):
            raise Exception("[Importer2::load] %s file does not exist"%file)


        self.xml_tree = ET.parse(file)
        root = self.xml_tree.getroot()
        body = root.find("BODY")

        # sometimes there are multiple series per xml file
        series = body.find("Patient").find("Study").findall("Series")
        pid = body.find("Patient").find("LastName").text

        # image files should be in the same folder
        data_dir = os.path.dirname(file)

        # get the subject (or create new)
        person = self.people[pid]

        # get the study date
        study_date = body.find("Patient").find("Study").find("StudyDate").find("Date")
        date = datetime.date( int(study_date.find("Year").text),
                              int(study_date.find("Month").text),
                              int(study_date.find("Day").text)
                              )

        # print("Loading %s"%pid)

        for s in series:
            modality = s.find("Modality").text
            if modality == "OCT":
                # print "\t OCT"
                oct = Scans.OCT(preload=preload)
                oct.pid = pid
                oct.date = date
                oct.data_dir = data_dir
                if oct.loadSeries(s):
                    person.addScan(oct)

            if modality == "OP": #
                # print("LOADING AF")
                images = s.findall("Image")
                # print("\tFound %d images"%len(images))
                for image in images:
                    af = Scans.SWAF(preload=preload)
                    af.pid = pid
                    af.data_dir = data_dir
                    af.date = date
                    af.mode = s.find("ModalityProcedure").text
                    af.loadImage(image)
                    person.addScan(af)


    def loadOct(self,file):
        """
        XML file loader for OCT

        :param file:
        :return:
        """
        if not os.path.exists(file):
            raise Exception("[Importer2::load] %s file does not exist"%file)


        # print ("[Importer2::loadOct] %s"%file)
        oct = Scans.OCT()
        oct.loadFromXml(file)

        # get the subject (or create new)
        person = self.people[oct.pid]
        person.addScan(oct)



    def loadMP(self,pid,mp_pid,mp_eid,load_fixation=False):
        """
        Load MP data with pid and eid only
        :param pid:
        :param eid:
        :return:
        """

        person = self.people[pid]
        mp = Scans.MP(pid=pid)
        ret = mp.loadFromID(mp_pid,mp_eid,mp_data_dir=self.mp_data_folder,preload=False)
        if not ret:
            return
        if load_fixation:
            mp.data.findFixation(search_dir=self.mp_fixation_dir)
        person.addScan(mp)


    def loadAllMP(self,pid,m_pid):
        """
        Load all MP data associated to a pid

        :param pid:
        :return:
        """
        p_dirs = [os.path.join(self.mp_data_folder, name) for name in os.listdir(self.mp_data_folder) if os.path.isdir(os.path.join(self.mp_data_folder, name))]

        eids = None
        for dir in p_dirs:
            if os.path.basename(dir) == "config_backup":
                continue
            if int(os.path.basename(dir)) == m_pid:
                eids = [name for name in os.listdir(dir) if os.path.isdir(os.path.join(dir,name))]

        if eids is None:
            return

        for eid in eids:
            self.loadMP(pid,m_pid,int(eid))


