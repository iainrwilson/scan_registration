import os,glob
import datetime
from .People import People
from .EyeBall import EyeBall


class Importer:
    """
    Class that parses folders and populates the EyeBall data structures

    Uses Mitals filename structure.

    """

    def __init__(self,mp_data_folder=None):
        self.people = People()

        if mp_data_folder is None:
            self.mp_data_folder = "/Users/iain/Documents/MAIA/"
        else:
            self.mp_data_folder = mp_data_folder

    def load(self,fname):
        """
        Load an individual file, assumed 3 charachter  file extension

        :param fname:
        :return:
        """

        data = os.path.basename(fname)[:-4].split('_')
        mode = data[0]

        """ check modality """
        if mode == "OCT":
            pid = data[1]
            eye = data[2]
            visit = data[3]
            _date = data[-4:-1]
            date = datetime.date(int(_date[0]),int(_date[1]),int(_date[2]))
            patient = self.people[pid]
            scan = patient.eye(eye).getScan(mode="OCT", visit=visit, date=date)
            slice = int(data[7])
            scan.load(fname,slice)

        elif mode == "SWAF":
            pid = data[2]
            eye = data[3]
            visit = data[4]
            _date = data[-4:-1]
            fov = data[1]
            count = int(data[7])
            date = datetime.date(int(_date[0]),int(_date[1]),int(_date[2]))
            patient = self.people[pid]
            # scan = patient.eye(eye).getScan(mode="SWAF", visit=visit, date=date,fov=fov)
            scan = EyeBall.createScan(mode)
            scan.pid = pid
            scan.date = date
            scan.visit = visit
            scan.fov = fov
            scan.person = self.people[pid]
            scan.load(fname)

        elif mode == "MP":
            if len(data) > 9:
                """ Mitals naming convention, only pick the empty ir image"""
                return
            pid = data[2]
            eye = data[3]
            visit = data[4]
            eid = int(data[8])
            _date = data[-4:-1]
            patient = self.people[pid]
            date = datetime.date(int(_date[0]),int(_date[1]),int(_date[2]))

            scan = EyeBall.createScan(mode)
            scan.mp_data_dir = self.mp_data_folder
            scan.pid = pid
            scan.date = date
            scan.visit = visit
            scan.person = patient
            scan.eid = eid
            # scan = patient.eye(eye).getScan(mode="MP", visit=visit, date=date)
            scan.load(fname)

        # print scan
        patient.eye(eye).addScan(scan)
        return self.people




if __name__ == "__main__":

    """ Test this new importer thing """

    home = os.path.expanduser("~")
    baseDir = os.path.join(home, "experiments/vision classification/data/mital/")
    files = glob.glob(baseDir+"/*/*/*/*.tif")
    files += glob.glob(baseDir+"/*/*/*/*.png")
    di = Importer()

    for file in files:
        di.load(file)
