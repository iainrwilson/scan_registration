
from .EyeBall import EyeBall
from . import Scans

class Person:
    """
    Contains all data for an individual
    """

    def __init__(self,pid=None):
        self.OD = EyeBall(eye="OD",pid=pid,person=self)
        self.OS = EyeBall(eye="OS",pid=pid,person=self)

        self.pid = pid

        self.registrations = Scans.ScanRegistrations()

        # self.scans=[]

    def __repr__(self):
        txt = ""
        txt+= self.OD.__repr__()
        txt += self.OS.__repr__()
        return txt

    def __getitem__(self, key):
        return self.getScanByID(key)


    def scans(self):
        """
        return a list of scans
        :return:
        """
        return self.OD.scans+self.OS.scans

    def mode(self, mode):
        """
        Return all scans of a certain mode

        :param mode:
        :return:
        """
        return self.OD.mode(mode) + self.OS.mode(mode)

    def eye(self,eye):
        if eye=="OD":
           return self.OD

        if eye=="OS":
           return self.OS


    def addScan(self,scan):
        """
        ID which eye, then add to correspondeing eyeball.

        :param scan:
        :return:
        """

        # print scan.eye

        if scan.eye=="OD":
            self.OD.addScan(scan)

        if scan.eye=="OS":
            self.OS.addScan(scan)


    def getScan(self,mode,visit,eye):
        """
        return a specifc scan for this patient,
        """

        if eye=="OD":
            return self.OD.getScan(mode=mode, visit=visit)

        if eye=="OS":
            return self.OS.getScan(mode=mode, visit=visit)

    def getScanByID(self,scan_id):
        for s in self.scans():
            if s.getID() == scan_id:
                return s
        return None


    def addRegistration(self,reg):
        self.registrations.add(reg)


    def getRegistration(self,source,target):
        return self.registrations.get(source,target)

