from .Person import Person

class People:
    """
    Simple container class for all the people / subjects
    """

    def __init__(self):
        self.people = {}

    def __contains__(self, key):
        return key in self.people.keys()

    def __getitem__(self, key):
        if not self.__contains__(key):
            self.people[key] = Person(key)
        return self.people[key]

    def __repr__(self):
        pids = ""
        for pid in self.people.keys():
            pids += " %s"%pid
        return pids

    def __len__(self):
        return len(self.people)

    def add(self,pid):
        if not self.__contains__(pid):
            self.people[pid] = Person(pid)

    def list(self):
        return self.people.keys()


    def scans(self):
        scans=[]
        for k,v in self.people.items():
            scans+=v.scans()
        return scans

    def iteritems(self):
        return self.people.items()

    def mode(self,mode):
        """
        get all scans with certain mode.
        :param mode:
        :return:
        """
        scans = []
        for k, v in self.people.items():
            scans += v.mode(mode)
        return scans
