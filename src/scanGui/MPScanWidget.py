"""

PyQT5 scan Widgert


Base class for the scan display widget

just do oct to start.

"""

from .ScanWidget import ScanWidget
from .MPScanImage import MPScanImage
from PyQt5 import QtWidgets


class MPScanWidget(ScanWidget):

    def __init__(self,parent=None, scan=None):
        ScanWidget.__init__(self,parent)

        # self.frame.hide()
        self.sliceLabel.hide()
        self.sliceInfo.hide()

        self.verticalLayout.removeWidget(self.localiserLabel)
        self.localiserLabel.close()
        self.localiserLabel = MPScanImage()
        self.verticalLayout.insertWidget(0,self.localiserLabel)


        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.localiserLabel.sizePolicy().hasHeightForWidth())
        self.localiserLabel.setSizePolicy(sizePolicy)

        self.mpInfoBox.gridEnabled.connect(self.localiserLabel.enableGridOverlay)
        self.mpInfoBox.fixationEnabled.connect(self.localiserLabel.enableFixationOverlay)

    def drawMPOverlay(self,image=None,reg=None):
        """
        Overlay grid data
        :return:
        """

        if image is None:
            image = self.localiser.copy()

        self.scan.drawStimuli(image=image,reg=reg,drawHistory=False)
        return image




    def loadScan(self,scan):
        super().loadScan(scan)
        self.mpInfoBox.scan = scan
        self.mpInfoBox.init()

    def wheelEvent(self, event):
        pass