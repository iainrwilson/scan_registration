# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'octslicewidget.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_OCTSliceWidget(object):
    def setupUi(self, OCTSliceWidget):
        OCTSliceWidget.setObjectName("OCTSliceWidget")
        OCTSliceWidget.resize(1106, 678)
        self.widget = QtWidgets.QWidget(OCTSliceWidget)
        self.widget.setGeometry(QtCore.QRect(190, 50, 741, 551))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.spinBox = QtWidgets.QSpinBox(self.widget)
        self.spinBox.setObjectName("spinBox")
        self.horizontalLayout.addWidget(self.spinBox)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.sliceLabel = OCTSliceImage(self.widget)
        self.sliceLabel.setObjectName("sliceLabel")
        self.verticalLayout.addWidget(self.sliceLabel)
        self.groupBox = QtWidgets.QGroupBox(self.widget)
        self.groupBox.setStyleSheet("QGroupBox {\n"
"    border: 2px solid grey;\n"
"    border-radius: 10px;\n"
"}")
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout.addWidget(self.groupBox)

        self.retranslateUi(OCTSliceWidget)
        QtCore.QMetaObject.connectSlotsByName(OCTSliceWidget)

    def retranslateUi(self, OCTSliceWidget):
        _translate = QtCore.QCoreApplication.translate
        OCTSliceWidget.setWindowTitle(_translate("OCTSliceWidget", "Form"))
        self.label.setText(_translate("OCTSliceWidget", "Slice Number"))
        self.sliceLabel.setText(_translate("OCTSliceWidget", "Slice Image"))
        self.groupBox.setTitle(_translate("OCTSliceWidget", "Slice Information"))

from .OCTSliceImage import OCTSliceImage

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    OCTSliceWidget = QtWidgets.QWidget()
    ui = Ui_OCTSliceWidget()
    ui.setupUi(OCTSliceWidget)
    OCTSliceWidget.show()
    sys.exit(app.exec_())

