"""

Custom class / QLable for displaying the localiser image

"""

from PyQt5.QtWidgets import QLabel,QSizePolicy
from PyQt5.QtCore import Qt,pyqtSignal,QSize
from PyQt5.QtGui import QImage, QPixmap,QPainter,QPen,QColor,QMouseEvent
import cv2
import numpy as np

from .Markers import Marker


class ScanImage(QLabel):
    """
    Base class for displaying scan images
    """


    # add custom signals
    addMarker = pyqtSignal(object)
    removeMarker = pyqtSignal(object)

    def on_add_marker(self,value):
        print("LocaliserImage::on_add_marker")
        self.addMarker.emit(value)

    def on_remove_marker(self,value):
        print("LocaliserImage::on_remove_marker")
        self.addMarker.emit(value)


    def __init__(self,parent=None,scan=None,markers=None):
        super().__init__(parent)
        self.scan = scan
        self.markers = markers

        self.enableMarkers = True
        self.highlight = Marker(-1,-1,-1)

        # self.width = 496
        # self.height = 496

        self.scale = [1.0,1.0]
        self.overlay = None
        self.reg = None
        self.enable_mp_overlay = True
        self.reg_cache = None
        self.mp_cache = None
        self.alt_localiser = None
        self.alt_localiser_image = None
        self.alt_localiser_fname = None

        self.enable_alt_localiser = True
        self.enable_etdrs_overlay = False
        # self.setScaledContents(True)

        # self.initUI()

    def load(self,scan,markers,reg=None,alt_localiser=None):
        self.scan = scan
        self.markers = markers
        self.setMinimumSize(496, 496)
        self.reg = reg
        self.alt_localiser = alt_localiser
        self.markers.markersChanged.connect(self.update)
        self.scale = (np.array((self.width(),self.height()) / np.array(self.scan.getIRImage().shape[:2])))

    def initUI(self):
        self.setMouseTracking(True)
        # self.setFixedSize(self.width,self.height)
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        sizePolicy.setHeightForWidth(True)
        self.setSizePolicy(sizePolicy)
        # self.setMinimumSize(QSize(400,400))
        # self.setGeometry(0,0,self.width,self.height)

    # def hasHeightForWidth(self):
    #     return self.scan is not None
    #
    # def heightForWidth(self, w):
    #     if self.scan is not None:
    #         _w = self.scan.localiser_width
    #         _h = self.scan.localiser_height
    #         return int(w * (_h/_w))

    def paintEvent(self, event):
        if self.scan is None:
            return

        painter = QPainter(self)
        pixmap = self.cv2pixmap(self.scan.getIRImage().copy())

        painter.drawPixmap(self.rect(), pixmap)



    def overlayETDRS(self,painter,centre_point=None):

        pass




    def mouseMoveEvent(self, event):
        # print("LocaliserImage: Mouse move ",event.pos())
        x = event.pos().x() / self.scale[0]
        y = event.pos().y() / self.scale[1]

        # print("Rx: %d, Ry: %d, x:%d, y:%d"%(event.pos().x(),event.pos().y(),x,y))

        # find overlap with any markers
        if self.markers is None:
            return
        for m in self.markers:
            _x = m.pos_x
            _y = int(round(self.scan.getSlice(m.slice).start_y / self.scan.scale_y))
            dist = np.linalg.norm(np.array((x,y))-np.array((_x,_y)))
            # print("M == x:%d y:%d Dist: %f"%(_x,_y,dist))
            if dist < 20.0:
                self.highlight = m
                # print("found overlap")
                self.update()
                return
        self.highlight = Marker(-1,-1,-1)
        self.update()

    # def mousePressEvent(self, event):
    #     super().mousePressEvent(event)

    def cv2pixmap(self,image):
        """
        convert opencv image to pixmap
        :return:
        """
        height, width, bytesPerComponent = image.shape
        bytesPerLine = 3 * width
        cv2.cvtColor(image, cv2.COLOR_BGR2RGB, image)
        qimg = QImage(image.tobytes(), width, height, bytesPerLine, QImage.Format_RGB888)
        return QPixmap.fromImage(qimg)