"""

PyQT5 scan Widgert


Base class for the scan display widget

just do oct to start.

"""

from .ScanWidget import ScanWidget

class AFScanWidget(ScanWidget):

    def __init__(self,parent=None, scan=None):
        super().__init__(parent)

        self.sliceLabel.hide()
        self.sliceInfo.hide()
        self.mpInfoBox.hide()

    def wheelEvent(self, event):
        pass