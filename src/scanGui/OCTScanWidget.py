"""

PyQT5 scan Widgert


Base class for the scan display widget

just do oct to start.

"""
from PyQt5.QtWidgets import QWidget,QLabel,QVBoxLayout,QSplitter,QHBoxLayout,QSpinBox,QPushButton
from PyQt5.QtCore import *
from PyQt5.QtCore import Qt
import cv2
import numpy as np
from .ScanWidget import ScanWidget
from .Markers import Markers
from .OCTSliceImage import OCTSliceImage
from .OCTLocaliserImage import OCTLocaliserImage

class OCTScanWidget(ScanWidget):

    def __init__(self,parent=None, scan=None):
        super().__init__(parent,scan)
        self.mpInfoBox.hide()
        self.minSize = QSize(400,400)

        # self.splitter = QSplitter(Qt.Vertical)
        # self.localiserLabel = OCTLocaliserImage(self,scan=scan)
        # self.sliceLabel = OCTSliceImage()
        # self.sliceLabel.mousePressEvent = self.sliceMousePress
        # self.hbox.addWidget(self.sliceLabel)
        self.sliceNumber = 0


        # self.overlayButton = QPushButton("Hide MP Grid")
        self.overlayButton.clicked.connect(self.overlayButtonClicked)
        # self.vbox.addWidget(self.overlayButton)
        # self.vbox.addStretch(1)

        # self.vbox.addWidget(QLabel("OCT Slice:"))
        # self.spinbox = QSpinBox(self)
        # self.vbox.addWidget(self.spinbox)


        # self.setFixedSize(1024,496)

        if scan is not None:
            self.loadScan(scan)

        self.enable_mp_overlay=True

    def overlayButtonClicked(self):
        if self.localiserLabel.enable_mp_overlay:
            self.localiserLabel.enable_mp_overlay = False
            self.sliceLabel.enable_mp_overlay = False
            self.overlayButton.setText("Show MP Grid")
        else:
            self.localiserLabel.enable_mp_overlay = True
            self.sliceLabel.enable_mp_overlay = True
            self.overlayButton.setText("Hide MP Grid")
        # print("Overlay button pressed")
        # self.draw()
        # self.localiserLabel.update()
        # self.sliceLabel.update()
        # self.update()
        # self.parent().update()

    def sliceNumberChanged(self,value):
        self.sliceNumber = value-1
        self.draw()

    def draw(self):
        self.localiserLabel.update()
        self.loadSlice()


    def setReg(self, reg):
        """
        Overlay new scan onto localiser
        :param reg:
        :return:
        """
        if reg.source.mode == "MP":
            self.reg = reg
        else:
            self.alt_localiser = reg

        # must retrigger localiser
        self.localiserLabel.load(self.scan, self.markers, self.reg,self.alt_localiser)
        self.sliceLabel.load(self.scan,self.markers,self.reg)
        self.draw()

    def loadScan(self,scan):
        self.scan = scan
        self.loadLocaliser()
        self.markers = self.findMarkers()
        self.localiserLabel.load(self.scan,self.markers,self.reg,self.alt_localiser)
        self.sliceLabel.load(self.scan,self.markers,self.reg)
        self.loadSlice()
        self.spinbox.setRange(1,self.scan.getSliceCount())
        self.spinbox.valueChanged.connect(self.sliceNumberChanged)

    def loadLocaliser(self):
        # self.localiserLabel.load(self.scan, self.markers)

        self.scanModeLabel.setText(self.scan.mode)
        text = "%dx%d" % (self.scan.localiser_width, self.scan.localiser_height)
        self.scanResolutionLabel.setText(text)
        text = "%.4fx%.4f" % (self.scan.scale_x, self.scan.scale_y)
        self.scanScaleLabel.setText(text)

        self.scanAngleLabel.setText("%.2f" % self.scan.localiser_angle)
        self.dateLabel.setText(self.scan.datetime.strftime("%H:%M %d/%m/%Y"))
        self.scanFocusLabel.setText("%.2f" % self.scan.localiser_focus)
        self.scanGainLabel.setText("%.2f" % self.scan.localiser_sensor_gain)
        self.scanNumAvgLabel.setText("%d" % self.scan.localiser_num_ave)

    def loadSlice(self):
        self.slice = self.scan[self.sliceNumber]
        self.sliceLabel.update()
        self.sliceNumberLabel.setText("%d"%self.scan.getSliceCount())

        text = "%dx%d" % (self.scan.getSlice(0).width, self.scan.getSlice(0).height)
        self.sliceResolutionLabel.setText(text)

        text = "%dx%d" % (self.scan.getSlice(0).scale_x, self.scan.getSlice(0).scale_y)
        self.sliceScaleLabel.setText(text)



    def wheelEvent(self, event):
        p = event.angleDelta() / 120

        self.sliceNumber += p.y()
        if self.sliceNumber >= self.scan.getSliceCount():
            self.sliceNumber = self.scan.getSliceCount()-1
        elif self.sliceNumber <0:
            self.sliceNumber = 0

        # update spinbox
        self.spinbox.setValue(self.sliceNumber+1)
        self.spinbox.update()
        self.localiserLabel.update()
        self.draw()