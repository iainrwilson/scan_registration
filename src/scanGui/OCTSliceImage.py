"""

Custom class / QLable for displaying the localiser image

"""

from PyQt5.QtWidgets import QLabel
from PyQt5.QtCore import Qt,pyqtSignal,QSize
from PyQt5.QtGui import QImage, QPixmap,QPainter,QPen,QColor
import cv2
import numpy as np

from .Markers import Marker
from .ScanImage import ScanImage


class OCTSliceImage(ScanImage):
    """ OCT Specific at the moment"""

    def __init__(self,parent=None,scan=None,markers=None):
        super().__init__(parent)
        # ScanImage.__init__(self,parent)
        # Ui_OCTSliceWidget.__init__(self)
        # self.setupUi(self)


        #
        # self.width = 1024
        # self.height = 496

        self.initUI()


    def load(self,scan,markers,reg=None,alt_localiser=None):
        self.scan = scan

        self.setMinimumSize(self.scan.getSlice(0).width,self.scan.getSlice(0).height)

        self.markers = markers
        self.reg = reg
        self.alt_localiser = alt_localiser
        self.markers.markersChanged.connect(self.update)
        self.scale = (np.array((self.width(),self.height()) / np.array(self.scan.getIRImage().shape[:2])))



        # self.setGeometry(0,0,self.width,self.height)


    def overlayMPGridSlice(self, slice=None):
        if slice is None:
            slice = self.scan[self.parent().sliceNumber].copy()

        if not self.enable_mp_overlay:
            return slice

        if self.reg is None:
            return slice

        if self.reg.source.mode != "MP":
            return slice

        slice = self.scan.overlayMPData(self.reg.source, slice=self.parent().sliceNumber, reg=self.reg, alpha=0.3)
        # print("Slice Data: ",slice.shape)
        slice = cv2.cvtColor(slice, cv2.COLOR_BGRA2BGR)
        return slice



    def paintEvent(self, event):
        if self.scan is None:
            return
        sliceImage = self.overlayMPGridSlice()
        painter = QPainter(self)
        pixmap = self.cv2pixmap(sliceImage)
        painter.drawPixmap(self.rect(), pixmap)

        # goldman 3 thickness
        thickness = 10
        offset = int(round(thickness / 2))

        if self.enableMarkers and (self.markers is not None):
            # pen = QPen(Qt.green,2)
            painter.setBrush(Qt.green)
            painter.setPen(Qt.NoPen)
            for m in self.markers:
                if not m.slice == self.parent().sliceNumber:
                    continue

                if m == self.highlight:
                    painter.setBrush(QColor(251, 155, 152, 128))
                else:
                    painter.setBrush(QColor(155, 251, 152, 128))

                slice_0 = self.scan.getSlice(self.parent().sliceNumber)

                x1 = int(round((slice_0.start_x / self.scan.scale_x)))
                x2 = int(round((slice_0.end_x / self.scan.scale_x)))
                s = float(x2-x1) / float(self.width())
                x = (m.pos_x - x1)

                # print("Marker stored x=%d  rendered x=%d"%(m.pos_x,x))
                painter.drawRect(x-offset,0,thickness,self.height())
                pen = QPen(QColor(0,0,0,60))

                painter.setPen(pen)
                painter.drawLine(x,0,x,self.height())
                painter.setPen(Qt.NoPen)
                # y = int(round((self.scan.getSlice(m.slice).start_y / self.scan.scale_y) * self.scale[1]))
                # print("drawing %d, %d" % (x, y))
                # painter.drawEllipse(x - 5, y - 5, 10, 10)

    def mousePressEvent(self, event):
        x = event.pos().x()
        if self.scan is None:
            return
        # check x within slice limit
        slice_0 = self.scan.getSlice(self.parent().sliceNumber)
        x1 = int(round((slice_0.start_x / self.scan.scale_x)))
        x2 = int(round((slice_0.end_x / self.scan.scale_x)))

        # add padding as we store the markers in localiser image coordinates
        x+=x1

        if event.button() == Qt.RightButton:
            if self.highlight != Marker(-1, -1, -1):
                self.markers.remove(self.highlight)
        else:
            self.markers.add(self.parent().sliceNumber, x)
        self.update()


    def mouseMoveEvent(self, event):
        # print("OCTSliceImage: Mouse move ",event.pos())
        x = event.pos().x()
        y = event.pos().y()
        if self.scan is None:
            return
        slice_0 = self.scan.getSlice(self.parent().sliceNumber)
        x1 = int(round((slice_0.start_x / self.scan.scale_x)))

        # add padding as we store the markers in localiser image coordinates
        x += x1

        #
        # print("Rx: %d, Ry: %d, x:%d, y:%d"%(event.pos().x(),event.pos().y(),x,y))

        # find overlap with any markers
        if self.markers is None:
            return
        for m in self.markers:
            _x = m.pos_x
            # _y = int(round(self.scan.getSlice(m.slice).start_y / self.scan.scale_y))
            # dist = np.linalg.norm(np.array((x,y))-np.array((_x,_y)))
            # print("M == x:%d y:%d Dist: %f"%(_x,_y,dist))
            dist = abs(_x-x)
            if dist < 10:
                self.highlight = m
                # print("found overlap")
                self.update()
                return

        self.highlight = Marker(-1,-1,-1)
        self.update()

