from .MPInforGroupBoxGui import Ui_MPInfoGroupBox
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtCore import Qt,pyqtSignal
import cv2

class MPInfoBox(QGroupBox,Ui_MPInfoGroupBox):
    def __init__(self,parent=None,scan=None):
        QGroupBox.__init__(self,parent)
        Ui_MPInfoGroupBox.__init__(self)
        self.setupUi(self)
        self.scan = scan

    fixationEnabled = pyqtSignal(bool)
    def on_fixation_changed(self,state):
        if self.fixationCheckBox.isChecked():
            self.fixationEnabled.emit(True)
        else:
            self.fixationEnabled.emit(False)


    gridEnabled = pyqtSignal(bool)
    def on_grid_changed(self, state):
        if self.gridCheckBox.isChecked():
            self.gridEnabled.emit(True)
        else:
            self.gridEnabled.emit(False)

    def init(self):
        if self.scan is None:
            return
        data = self.scan.data
        self.pidLabel.setText("%d"%data.patient_id)
        self.eidLabel.setText("%d" % data.exam_id)
        self.eyeLabel.setText(data.eye)
        self.dateLabel.setText(data.exam_date.strftime("%H:%M %d/%m/%Y"))
        self.fovLabel.setText("%f"%data.fov)
        self.examDurationLabel.setText("%d"%data.examDuration)
        self.reliabilityLabel.setText("%.2f"%data.getReliability())

        image = self.cv2pixmap(cv2.imread(data.getFixationChart()))

        image_rs = image.scaled(self.fixationChart.width(),self.fixationChart.height(),transformMode=Qt.SmoothTransformation)
        # self.fixationChart.resize(image.shape[1],image.shape[0])
        self.fixationChart.setPixmap(image_rs)

        self.fixationCheckBox.stateChanged.connect(self.on_fixation_changed)
        self.gridCheckBox.stateChanged.connect(self.on_grid_changed)


    def cv2pixmap(self,image):
        """
        convert opencv image to pixmap
        :return:
        """
        height, width, bytesPerComponent = image.shape
        bytesPerLine = 3 * width
        cv2.cvtColor(image, cv2.COLOR_BGR2RGB, image)
        qimg = QImage(image.tobytes(), width, height, bytesPerLine, QImage.Format_RGB888)
        return QPixmap.fromImage(qimg)